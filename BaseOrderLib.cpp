#include "BaseOrderLib.h"

BaseOrderLib::BaseOrderLib()
{

}

//GetNodes   用于扫描所有节点,支持广播/组播/单播
//客户端->设备
QString BaseOrderLib::getNodes_CToD(QString ip,quint32 port)
{
    QString getNodes = "GetNodes";
    if(ip.isEmpty() == false || port != 0)
    {
        getNodes.append(QString(";%1,%2").arg(ip).arg(QString::number(port)));
    }
    getNodes = addBraceForString(getNodes);
    return getNodes;
}

//SetLockFrame 用于设置锁屏基数,实际锁屏时间为 锁屏基数+同步系数
//客户端->设备
QString BaseOrderLib::setLockFrame_CToD(int panel_sn,int baseLockFrame)
{
    QString setLockFrame = QString("SetLockFrame;%1;%2").arg(QString::number(panel_sn)).arg(QString::number(baseLockFrame));
    setLockFrame = addBraceForString(setLockFrame);
    return  setLockFrame;
}

//GetTransmit 用于或缺指定信号源的输出组播转发,支持单播
//客户端->设备
QString BaseOrderLib::getTransmit_CToD(int panel_sn,QString ip,quint32 port)
{
    QString getTransmit = QString("GetTransmit;%1").arg(QString::number(panel_sn));
    if(ip.isEmpty() == false && port != 0)
    {
        getTransmit.append(QString(";%1,%2").arg(ip).arg(QString::number(port)));
    }
    getTransmit = addBraceForString(getTransmit);
    return getTransmit;
}

//SetTransmit 用于指定信号源的输出组播转发,支持单播
//客户端->设备
QString BaseOrderLib::setTransmit_CToD(int panel_sn,QList<SetTransmitParam*> SetTransmitParamList)
{
    QString setTransmit = QString("SetTransmit;%1").arg(QString::number(panel_sn));
    for(int i=0;i<SetTransmitParamList.count();i++)
    {
        setTransmit.append(QString(";%1,%2")
                           .arg(SetTransmitParamList[i]->src_url)
                           .arg(SetTransmitParamList[i]->transmit_url));
    }
    setTransmit = addBraceForString(setTransmit);
    return setTransmit;
}

//GetWindows 用于从某个输出节点获取当前节点属于的视频墙的实时窗口信息，只能单播
//客户端->设备
QString BaseOrderLib::getWindows_CToD(int panel_sn,QString ip,quint32 port)
{
    QString getWindows = QString("GetWidnwos;%1").arg(QString::number(panel_sn));
    if(ip.isEmpty() == false && port != 0)
    {
        getWindows.append(QString(";%1,%2").arg(ip).arg(QString::number(port)));
    }
    getWindows = addBraceForString(getWindows);
    return getWindows;
}

//SetWindows 用于设定指定视频墙的实时窗口信息,支持广播/组播/单播
//客户端->设备
QString BaseOrderLib::setWidnows_CToD(int panel_sn,QList<SetWindowsParam*> SetWindowsParamList)
{
    QString setWindows = QString("SetWindows;%1").arg(QString::number(panel_sn));
    for(int i=0;i<SetWindowsParamList.count();i++)
    {
        setWindows.append(QString(";%1,%2,%3,%4,%5,%6,%7")
                          .arg(SetWindowsParamList[i]->src_id)
                          .arg(SetWindowsParamList[i]->url)
                          .arg(SetWindowsParamList[i]->x)
                          .arg(SetWindowsParamList[i]->y)
                          .arg(SetWindowsParamList[i]->w)
                          .arg(SetWindowsParamList[i]->h)
                          .arg(SetWindowsParamList[i]->group_id));
    }
    setWindows = addBraceForString(setWindows);
    return setWindows;
}

//为QString 变量添加大括号(示例   GetNodes;128.0.0.1,3220 -> {GetNodes;128.0.0.1,3220})
QString BaseOrderLib::addBraceForString(QString str)
{
    str.push_front("{");
    str.push_back("}");
    return str;
}

//转发节点配置命令 默认值
QJsonObject BaseOrderLib::defJson_SetRelayList()
{
    QJsonObject setRelayNode;
    QJsonObject params,snap;
    setRelayNode.insert("Type","Video");
    setRelayNode.insert("Function","setRelayList");
    snap.insert("oneshot",false);
    params.insert("snap",snap);
    setRelayNode.insert("Params",params);
    QJsonArray value;
    setRelayNode.insert("Value",value);
    return setRelayNode;
}

//回显控制Value数组对象
QJsonObject BaseOrderLib::setRelayList_valueObj()
{
   QJsonObject valueObj;
   valueObj.insert("id",0);
   valueObj.insert("protocol","rtsp");
   QJsonObject url,transmit,main,sub,snap;
   url.insert("main","");
   url.insert("sub","");
   main.insert("type","bypass");
   main.insert("protocol","udp");
   main.insert("ip","224.168.111.1");
   main.insert("port",45678);
   main.insert("length",1300);
   sub.insert("type","bypass");
   sub.insert("protocol","udp");
   sub.insert("ip","224.168.111.1");
   sub.insert("port",45678);
   sub.insert("length",1300);
   snap.insert("snap_w",320);
   snap.insert("snap_h",180);
   snap.insert("snap_len",1200);
   snap.insert("snap_delay",1000);
   snap.insert("mcast_enable",true);
   snap.insert("mcast_ip","224.168.2.13");
   snap.insert("mcast_port",23456);
   snap.insert("mcast_freq",20);
   transmit.insert("main",main);
   transmit.insert("sub",sub);
   transmit.insert("snap",snap);
   valueObj.insert("url",url);
   valueObj.insert("transmit",transmit);
   return valueObj;
}

//抓图回显控制SNAP默认值
QJsonObject BaseOrderLib::defJson_snap()
{
    QJsonObject snapObj,valueObj;
    snapObj.insert("Type","Snap");                                      //参数:Type 必选:是 类型:string 说明:命令类型
    snapObj.insert("Function","setSnap");                               //参数:Function 必选:是 类型:string 说明:功能名
    valueObj.insert("enable",true);                                     //参数:Value-enable 必选:是 类型:bool 说明:抓图使能
    valueObj.insert("type","MJPEG");                                    //参数:Value-type 必选:否 类型:string 说明:抓图编码类型 "H264"/"H265"/"JPEG"/"MJPEG"
    valueObj.insert("gop",10);                                          //参数:Value-gop 必选:否 类型:string 说明:h26x编码时,设定gop关键帧间隔
    valueObj.insert("pause",false);                                     //参数:Value-pause 必选:否 类型:bool 说明:暂停/开始组播发送,默认缺省值,值为true
    valueObj.insert("server_ip","192.168.1.60");                        //参数:Value-server_ip 必选:否 类型:string 说明:服务器IP地址
    valueObj.insert("server_port",54321);                               //参数:Value-server_port 必选:否 类型:int 说明:服务器端口
    valueObj.insert("frame_rate",30);                                   //参数:Value-frame_rate 必选:否 类型:int 说明:主动抓图帧率
    valueObj.insert("snap_pic_width",960);                              //参数:Value-snap_pic_width 必选:是 类型:int 说明:抓图宽度
    valueObj.insert("snap_pic_height",540);                             //参数:Value-snap_pic_height 必选:是 类型:int 说明:抓图高度
    valueObj.insert("snap_crop_width",0);                               //参数:Value-snap_crop_width 必选:是 类型:int 说明:抓图截图宽度
    valueObj.insert("snap_crop_height",0);                              //参数:Value-snap_crop_height 必选:是 类型:int 说明:抓图截图高度
    valueObj.insert("snap_pic_quality",50);                             //参数:Value-snap_pic_quality 必选:是 类型:int 说明:抓图品质
    valueObj.insert("snap_packet_interval",800);                        //参数:Value-snap_packet_interval 必选:是 类型:int 说明:抓图包间隔(ms)
    valueObj.insert("user_info","null");                                //参数:Value-snap_packet_interval 必选:是 类型:int 说明:抓图包间隔(ms)
    snapObj.insert("Value",valueObj);                                   //参数:Value 必选:是 类型:Object 说明:网络参数
    return snapObj;
}

//setWindows回复到客户端的 默认值
QJsonObject BaseOrderLib::defJson_setWindowsToC()
{
    QJsonObject setWindowsToCObj;
    setWindowsToCObj.insert("code",0);
    setWindowsToCObj.insert("msg","ok");
    setWindowsToCObj.insert("response","SetWindows");
    return setWindowsToCObj;
}

//清空命令
QJsonObject BaseOrderLib::closeRelayList()
{
    QJsonObject closeRelayListObj;
    closeRelayListObj.insert("Type","Video");
    closeRelayListObj.insert("Function","closeRelayList");
    closeRelayListObj.insert("Value","all");
    return closeRelayListObj;
}

//{
// "Type":"Video",
// "Function": "closeRelayList",
// "Value":"all"
//}

