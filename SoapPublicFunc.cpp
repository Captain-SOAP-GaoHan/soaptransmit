﻿/***************************************************
 * @Title : SoapPublicFunc.cpp
 * @brief :
 * @author: Captain_SOAP
 * @lastestUpdata: 2021.11.11
 **************************************************/

/*
 * 声明:
 * 该类提供自定义的公用函数,基本上都是静态成员函数
 * 所有界面相关的函数(例 弹窗提示)都不能放在主线程以外的其他线程中
 * STATEMENT:
 * "SoapPublicFunc" Class is desinged and created by Captain_SOAP
 * This class contains popular public functions and so be specified by keyword "static"
 * u can just invoke (or call) functions below without creating a "SoapPublicFunc" object
 * well then , u can also add some functions to this class if u like
 * good luck and have fun
*/

#include "SoapPublicFunc.h"

//构造
SoapPublicFunc::SoapPublicFunc()
{

}

//析构
SoapPublicFunc::~SoapPublicFunc()
{

}

//消息弹窗
void SoapPublicFunc::msgBoxRemind(QString remind)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,QString("提示"),remind);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    //模态窗口延时800ms关闭
    QTimer::singleShot(800,box,SLOT(accept()));
    box->exec();
}

//消息弹窗(重载1)
void SoapPublicFunc::msgBoxRemind(QString remind,quint32 ms)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,QString("提示"),remind);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->exec();
}

//消息弹窗(重载2)
void SoapPublicFunc::msgBoxRemind(QString remind,quint32 ms, QString label)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,label,remind);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->exec();
}

//消息弹窗(重载3)
void SoapPublicFunc::msgBoxRemind(QString remind, quint32 ms, QString label, QFont font)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,label,remind);
    box->setFont(font);
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->exec();
}

//警告弹窗
void SoapPublicFunc::warnBoxRemind(QString remind)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Warning,"警告",remind,QMessageBox::Ok);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->exec();
}

//警告弹窗(重载1)
void SoapPublicFunc::warnBoxRemind(QString remind, QFont font)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Warning,"警告",remind,QMessageBox::Ok);
    box->setFont(font);
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->exec();
}

//非模态弹窗提示
void SoapPublicFunc::modelessBoxRemind(QString remind)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",remind,QMessageBox::Ok);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->show();
}

//非模态弹窗提示(重载1)
void SoapPublicFunc::modelessBoxRemind(QString remind,quint32 ms)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",remind,QMessageBox::Ok);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->show();
}

//非模态弹窗提示(重载2)
void SoapPublicFunc::modelessBoxRemind(QString remind,quint32 ms,QWidget *parent)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",remind,QMessageBox::Ok,parent);
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->show();
}

//分支弹窗(继续或退出 (确定或取消))
int SoapPublicFunc::switchBoxRemind(QString remind)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Warning,"注意",remind,QMessageBox::Yes | QMessageBox::No);
    box->setFont(QFont("微软雅黑",12));
    box->setButtonText(QMessageBox::Yes,"确定");
    box->setButtonText(QMessageBox::No,"取消");
    box->setDefaultButton(QMessageBox::Yes);

    //额外内容 (该段代码仅在公用类的基础上自行添加，因为包含外部依赖项)
    //box->setWindowIcon(QIcon(":/Transmit/leave_Black.png"));
    //额外内容

    box->setAttribute(Qt::WA_DeleteOnClose);
    return box->exec();
}

//Soap自定义弹窗
void SoapPublicFunc::soapBoxRemind(QString remind,quint32 ms,QString label,QFont font,bool isModel)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,label,remind,QMessageBox::Ok);
    box->setFont(font);
    box->setAttribute(Qt::WA_DeleteOnClose);
    if(ms != 0)
    {
        QTimer::singleShot(ms,box,SLOT(accept()));
    }
    if(isModel)
    {
        box->exec();
    }
    else
    {
        box->show();
    }
}

//检查QByteArray对象是否符合Json格式
bool SoapPublicFunc::checkIsJson(QByteArray json)
{
    bool isJson = false;
    QJsonParseError jsonErr;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(json,&jsonErr);
    (jsonErr.error == QJsonParseError::NoError) ? isJson = true : isJson = false;
    return isJson;
}

//检查是否为合法的IP地址(示例: 128.0.113.103)
bool SoapPublicFunc::checkIsIp(QString ip)
{
    QStringList list = ip.split(".");
    //判断分割后的数量
    if(list.count() != 4)
    {
        return false;
    }
    //检查范围
    for(int i = 0 ;i<list.count();i++)
    {
        if(list[i].toInt() < 0 || list[i].toInt() > 255)
        {
            return false;
        }
    }
    return true;
}

//检查是否为合法的端口号(0~65536,0除外)
bool SoapPublicFunc::checkIsPort(quint32 port)
{
    bool isPort;
    (port<=0 || port>65536) ? isPort = false : isPort = true;
    return isPort;
}

//检查QString是否为合法的资源显示区域(示例:0,0,1920,1080)
bool SoapPublicFunc::checkIsArea(QString area)
{
    //拆分该Area字符串
    if(area.contains(",") == false)
    {
        return false;
    }
    QStringList areaList = area.split(",");
    if(areaList.count() != 4)
    {
        return false;
    }
    for(int i = 0 ; i<4 ; i++)
    {
        switch(i)
        {
        case 0:
        case 1:
        {
            if(areaList[i]<0)
                return false;
        }
            break;
        case 2:
        case 3:
        {
            if(areaList[i]<=1)
                return false;
        }
        }
    }
    if((areaList[0].toUInt()+areaList[2].toUInt()>4096) || (areaList[1].toUInt()+areaList[3].toUInt()>2160))
    {
        return false;
    }
    return true;
}

//检查QString是否为合法的节点实际内容(示例：1.jpg)(判定标准同window合法文件名判定标准)
bool SoapPublicFunc::checkIsContent(QString content)
{
    if(content.length() >224 || content.isEmpty())
    {
        return false;
    }
    bool isContent = false;
    //正则表达式判断特殊字符
    QString pattern("[\\\\/:*?\"<>|]");
    QRegExp rx(pattern);
    (content.indexOf(rx)>0) ? isContent = false : isContent = true;
    return isContent;
}

//检查QString是否是合法的网址(Url)
bool SoapPublicFunc::checkIsWebUrl(QString url)
{
    bool isWebUrl = false;
    (url.contains("http") && url.contains("://") && url.contains(".")) ? isWebUrl = true : isWebUrl = false;
    return isWebUrl;
}

//检查QString是否为合法的windows文件名
bool SoapPublicFunc::checkIsFileName(QString fileName)
{
    if(fileName.length() > 224 || fileName.isEmpty())
    {
        return false;
    }
    bool isFileName = false;
    //正则表达式判断特殊字符
    QString pattern("[\\\\/:*?\"<>|]");
    QRegExp rx(pattern);
    (fileName.indexOf(rx) >0 ) ? isFileName = false : isFileName = true;
    return isFileName;
}

//检查QString是否为合法的dev(设备)名
bool SoapPublicFunc::checkIsDevName(QString devName)
{
    if(devName.isEmpty())
    {
        return false;
    }
    bool isDevName = false;
    //正则表达式判断特殊字符
    QString pattern("[\\\\/:*?\"<>|]");
    QRegExp rx(pattern);
    (devName.indexOf(rx) >0 ) ? isDevName = false : isDevName = true;
    return isDevName;
}

//检查QString的位数是否为奇数
bool SoapPublicFunc::checkStrSizeIsOdd(QString str)
{
    bool isOdd;
    (str.size() % 2 != 0) ? isOdd = true : isOdd = false ;
    return isOdd;
}

//检查QString的位数是否为偶数
bool SoapPublicFunc::checkStrSizeIsEven(QString str)
{
    bool isEven;
    (str.size() % 2 == 0) ? isEven = true : isEven = false ;
    return isEven;
}

//检查是否带有中文字符
bool SoapPublicFunc::checkStrContainCN(QString str)
{
    bool isContainCN = str.contains(QRegExp("[\\x4e00-\\x9fa5]+"));
    return isContainCN;
}

//返回一个不带有后缀的文件名
QString SoapPublicFunc::fileNameWithoutSuff(QString fileName)
{
    //若文件名不合法，返回空字符
    if(checkIsFileName(fileName) == false)
    {
        return "";
    }
    int index = fileName.lastIndexOf(".");
    fileName.truncate(index);
    return fileName;
}

//QString->QjsonObject(将QString转换为QJsonObject)
QJsonObject SoapPublicFunc::QStringToJsonObj(const QString string)
{
    //防止中文乱码
    QTextCodec::codecForName("UTF-8");
    QJsonDocument jsonDoc = QJsonDocument::fromJson(string.toUtf8().data());
    QJsonObject jsonObj;
    if(jsonDoc.isNull())
    {
        //转换失败,jsonString不符合json格式
        qDebug()<<"QString转为QJsonObject失败！请检查QString : "<<string;
        //若QString参数不符合json格式,只会返回空的QJsonObject对象
        return jsonObj;
    }
    jsonObj = jsonDoc.object();
    return  jsonObj;
}

//QJsonObject->QString(将QJsonObject转换为QString)
QString SoapPublicFunc::jsonObjToQstring(const QJsonObject &json)
{
    return QString(QJsonDocument(json).toJson());
}

//QJsonObject->QString(精简版)
QString SoapPublicFunc::jsonObjToQstring_lite(const QJsonObject &jsonObject)
{
    return QString(QJsonDocument(jsonObject).toJson(QJsonDocument::JsonFormat::Compact));
}

//QByteArray -> QString
QString SoapPublicFunc::QByteArrayToQString(QByteArray byteArray)
{
    return QString(byteArray);
}

//返回当前时间
QString SoapPublicFunc::currentTime()
{
    //返回一个QString,格式为：年/月/日  时：分
    return QDateTime::currentDateTime().toString("yyyy/MM/dd-HH:mm");
}

//返回当前时间(年/月/日)
QString SoapPublicFunc::currentTime_date()
{
    return QDateTime::currentDateTime().toString("yyyy/MM/dd");
}

//返回当前时间(时:分)
QString SoapPublicFunc::currentTime_hour()
{
    return QDateTime::currentDateTime().toString("HH:mm");
}

//判断质数函数(一个老生常谈的函数了，不过我结合网上的方法自己优化了一下哈哈)
//虽然该算法效率高，但在频繁调用的情况下依然需要放入子线程中执行,否则可能导致主界面假死
//该算法可能是目前为止除筛法外判断质数 代码量与效率很平衡的算法(你也可以加入更多的判断条件以提升效率)
//优化思路：若一个数不能奇数i整除则，在接下来的判断中，你不需要判断prime是否能整除i的倍数(因为若一个数无法被i整除，则肯定不能被i的公倍数整除，当然，这可能需要额外的内存去暂时存放i)
bool SoapPublicFunc::checkIsPrime(quint64 prime)
{
    switch(prime)
    {
    case 0:
    case 1:return false;break;
    case 2:
    case 3:return true;break;
    default:
    {
        //偶数一定不是质数(2除外)
        if(prime % 2 == 0)
        {
            return false;
        }
        //不在6的倍数两侧的数一定不是质数(6倍定理)
        if(prime %6 != 1 && prime %6 != 5)
        {
            return false;
        }
        for(unsigned int i = 5 ; (i*i) <= prime ; i = i+6)
        {
            if(prime % i == 0 || prime %(i+2) == 0)
            {
                return false;
            }
        }
        return true;
    }
    }
}

//该函数与checkIsPrime效率不相上下
bool SoapPublicFunc::checkIsPrime2(quint64 prime)
{
    long long stop = prime / 6 + 1, Tstop = sqrt(prime) + 5;
    if (prime == 2 || prime == 3 || prime == 5 || prime == 7 || prime == 11)
    {
        return true;
    }
    if (prime % 2 == 0 || prime % 3 == 0 || prime % 5 == 0)
    {
        return false;
    }
    for (int i = 1; i <= stop; i++)
    {
        if (i * 6 >= Tstop)
        {
            break;
        }
        if ((prime % (i*6+1) == 0) || (prime % (i*6+5) == 0))
        {
            return false;
        }
    }
    return true;
}

//计时程序
quint64 SoapPublicFunc::countTime()
{
    QElapsedTimer timer1;
    timer1.start();

    //将要测试的程序放在以下

    //将要测试的程序放在以上

    return timer1.elapsed();
}

//返回一个(0,max-1)区间内的随机数(短时间内相同的随机数)
quint32 SoapPublicFunc::generateRamNum(quint32 max)
{
    if(max == 0)
    {
        return 0;
    }
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    quint32 randomNum = qrand() % max;
    return randomNum;
}

//返回一个(0,max-1)区间内的随机数列表(生成随机数列表同样需要一些时间,在num过大时同样需要放入子线程中处理)
QList<quint32> SoapPublicFunc::generateRamNumList(quint32 max ,quint32 num)
{
    if(num < 1)
    {
        num = 1;
    }
    QList<quint32> randomNumList;
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    for(quint32 i = 0 ; i != num ;i++)
    {
        randomNumList.append(qrand()%max);
    }
    return randomNumList;
}

//返回一个(0,max-1)区间内的随机数(不重复)
quint32 SoapPublicFunc::generateRamNumPro(quint32 max)
{
    if(max == 0)
    {
        return 0;
    }
    QTime time = QTime::currentTime();
    qsrand(time.msec() + time.second()*1000);
    quint32 randomNum = qrand() % max;
    return randomNum;
}

//获取(指定路径下的)文件名列表
QStringList SoapPublicFunc::getFileNames(QString parentPath)
{
    QDir dir(parentPath);
    //设置文件类型过滤器
    QStringList filter = QStringList()<<"*.jpg"<<"*.bmp"<<"*.png";
    return dir.entryList(filter,QDir::Files|QDir::Readable,QDir::Name);
}

//获取(指定路径下的)文件名列表(重载1)
QStringList SoapPublicFunc::getFileNames(QString parentPath,QStringList filter)
{
    QDir dir(parentPath);
    return dir.entryList(filter,QDir::Files|QDir::Readable,QDir::Name);
}

//获取(指定路径下的)文件信息
QFileInfoList SoapPublicFunc::getFileInfo(const QString &path)
{
    QDir dir(path);
    //设置文件类型过滤器
    QStringList filter = QStringList()<<"*.jpg"<<"*.bmp"<<"png";
    return dir.entryInfoList(filter);
}

//获取(指定路径下的)文件信息(重载1)
QFileInfoList SoapPublicFunc::getFileInfo(const QString &parentPath,QStringList filter)
{
    QDir dir(parentPath);
    return dir.entryInfoList(filter);
}

//判断一个绝对路径下的文件是否存在
bool SoapPublicFunc::isFileNameExist(QString absoluteFileName)
{
    bool isExist = false;
    QFile file(absoluteFileName);
    (file.exists()) ? isExist = true : isExist = false;
    return isExist;
}

//判断文件夹是否存在
bool SoapPublicFunc::isFilePathExist(QString absolutePath)
{
    bool isExist = false;
    QDir dir(absolutePath);
    (dir.exists()) ? isExist = true : isExist = false;
    return isExist;
}

//根据指定的分隔符将QString->QStringList
QStringList SoapPublicFunc::stringToStringList(const QString str,QChar separtor)
{
    QStringList list ;
    return list = str.split(separtor);
}

//根据指定的分隔符将QStringList->QString(组合)
QString SoapPublicFunc::stringListToString(const QStringList strList,QChar separator)
{
    QString str;
    return str = strList.join(separator);
}

//生成问候语
QString SoapPublicFunc::soapGreeting()
{
    //通过随机数和当前时间生成问候语
    int currHour = currentTime_hour().section(":",0,0).toUInt();
    QStringList greetList = QStringList()<<"欢迎回来"<<"你好"<<"好久不见"<<"欢迎"<<"很高兴遇见你"<<"等你很久了"<<"你好啊";
    if(currHour >6 && currHour<=12)
    {
        greetList += QStringList()<<"早上好"<<"又是新的一天"<<"赞美太阳";
    }
    else if(currHour>12 && currHour<=13)
    {
        greetList += QStringList()<<"中午好"<<"午休得怎样";
    }
    else if(currHour>13 && currHour<=17)
    {
        greetList += QStringList()<<"下午好"<<"来杯下午茶吧";
    }
    else if(currHour>17 && currHour<=23)
    {
        greetList += QStringList()<<"晚上好"<<"来杯咖啡吧";
    }
    else
    {
        greetList += QStringList()<<"夜深了"<<"注意别太晚啦";
    }
    quint32 index = greetList.count();
    QString greeting = greetList[generateRamNumPro(index)];
    return  greeting;
}

//生成离别语
QString SoapPublicFunc::soapFarewell()
{
    //通过随机数和当前时间生成离别语
    int currHour = currentTime_hour().section(":",0,0).toUInt();
    QStringList byeList = QStringList()<<"再见"<<"我会想你的"<<"要再来啊";
    if(currHour >6 && currHour <=12)
    {
        byeList += QStringList()<<"回见喽"<<"出去活动活动吧"<<"祝你好运";
    }
    else if(currHour >12 && currHour <= 18)
    {
        byeList += QStringList()<<"又是愉快的一天"<<"又要走了吗";
    }
    else if(currHour >18 && currHour <24)
    {
        byeList += QStringList()<<"祝好梦"<<"好好睡觉 精神棒棒";
    }
    else
    {
        byeList += QStringList()<<"快去睡吧"<<"可别再玩手机了"<<"当心熬出黑眼圈哦";
    }
    quint32 index = byeList.count();
    QString farewell = byeList[generateRamNumPro(index)];
    return farewell;
}

//自定义非阻塞延时函数(毫秒)
void SoapPublicFunc::soapSleep(quint32 ms)
{
    //currentTime返回当前时间,用当前时间加上我们要延时的时间得到一个新的时刻
    QTime reachTime = QTime::currentTime().addMSecs(ms);
    while(QTime::currentTime() < reachTime)
    {
        //当当前时间未达到设置的时间,允许线程继续处理下面的事件
        QCoreApplication::processEvents(QEventLoop::AllEvents,100);
    }
}

//自定义非阻塞延时函数(秒)
void SoapPublicFunc::soapSleep_S(quint32 s)
{
    QTime reachTime = QTime::currentTime().addSecs(s);
    while(QTime::currentTime() < reachTime)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents,100);
    }
}

//将一个数字转为16进制的QbyteArray类型
QByteArray SoapPublicFunc::toHex(int num,unsigned int place)
{
    QByteArray data;
    while(place--)
    {
        char hexNum = num & 0xff;
        data.append(hexNum);
        num = num >> 8;
    }
    return data;
}

//将一个QByteArray类型转为uint64
quint64 SoapPublicFunc::toUint64(QByteArray data)
{
    quint64 sum = 0;
    for(int i=0; i < data.size(); i++)
    {
        unsigned char tmp_num = (unsigned char)data[i];
        sum += (quint64)(tmp_num<<(i*8));
    }
    return sum;
}

//返回一个合法(windows)文件名的后缀
QString SoapPublicFunc::fileSuff(QString fileName)
{
    fileName = fileName.trimmed();
    //非法的文件名返回空字符
    if(checkIsFileName(fileName) == false)
    {
        return "";
    }
    QStringList list = fileName.split(".");
    QString fileSuff = "." + list[list.size()-1];
    return  fileSuff;
}

//返回一个合法(windows)文件名的后缀长度
int SoapPublicFunc::fileSuffLen(QString fileName)
{
    fileName = fileName.trimmed();
    //非法的文件名返回长度为0
    if(checkIsFileName(fileName) == false)
    {
        return 0;
    }
    QStringList list = fileName.split(".");
    QString fileSuff = "." + list[list.size()-1];
    return fileSuff.length();
}

/******************************REFERENCE******************************/
//以下为元老级别的算法,大多数做法看不懂，反正用就完事了
//交换函数
void SoapPublicFunc::swap(int *x , int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

//交换double变量
void SoapPublicFunc::swap(double *x,double *y)
{
    double temp = *x;
    *x = *y;
    *y = temp;
}

//冒泡排序(稳定但是效率感人)
void SoapPublicFunc::bubbleSort(int *arra, int len)
{
    for(int i = 0 ;i<len;i++)
    {
        for(int j=1;j<len-i;j++)
        {
            if(arra[j-1]>arra[j])
            {
                swap(&arra[j-1],&arra[j]);
            }
        }
    }
}

//选择排序
void SoapPublicFunc::selectionSort(int *arra, int len)
{
    int temp,index;
    for(int i = 0;i<len;i++)
    {
        temp = arra[i];
        index = i;
        for(int j=i+1;j<len;j++)
        {
            if(arra[j]<temp)
            {
                temp = arra[j];
                index = j;
            }
        }
        if(index == i)
        {
            continue;
        }
        swap(&arra[i],&arra[index]);
    }
}

//插入排序
void SoapPublicFunc::insertSort(int *arra,int len)
{
    for(int i = 0 ;i<len ;i++)
    {
        for(int j=i;j>0;j--)
        {
            if(arra[j]<arra[j-1])
            {
                swap(&arra[j],&arra[j-1]);
            }
            else
            {
                break;
            }
        }
    }
}

//希尔排序
void SoapPublicFunc::shellSort(int *arra,int len)
{
    int step = len;
    do
    {
        step = step/3 + 1;
        for(int i = 0 ;i<step;i++)
        {
            for(int j = i;j<len;j+=step)
            {
                for(int k = j;k>i;k-=step)
                {
                    if(arra[k]<arra[k-step])
                    {
                        swap(&arra[k],&arra[k-step]);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }while(step>1);
}

//快速排序
void SoapPublicFunc::quickSort(int *arra,int begin ,int end)
{
    if(begin < end)
    {
        //将区间的第一个数作为基准数
        int temp = arra[begin];
        //从左到右进行查找时的“指针”，指示当前左位置
        int i = begin;
        //从右到左进行查找时的“指针”，指示当前右位置
        int j = end;
        //不重复遍历
        while(i<j)
        {
            //当右边的数大于基准数时，略过，继续向左查找.不满足条件时跳出循环，此时的j对应的元素是小于基准元素的
            while(i<j && arra[j]>temp)
            {
                j--;
            }
            //将右边小于等于基准元素的数填入右边相应位置
            //(重复的基准元素集合到左区间)
            //不满足条件时跳出循环，此时的i对应的元素是大于等于基准元素的
            arra[i] = arra[j];
            while(i<j && arra[i]<=temp)
            {
                i++;
            }
            //将左边大于基准元素的数填入左边相应位置
            arra[j] = arra[i];
        }

        //将基准元素填入相应位置
        arra[i] = temp;
        //此时的i即为基准元素的位置
        //对基准元素的左边子区间进行相似的快速排序
        quickSort(arra,begin,i-1);
        //对基准元素的右边子区间进行相似的快速排序
        quickSort(arra,i+1,end);
    }
    else
    {
        return;
    }
}

//归并排序
void SoapPublicFunc::mergeSort(int *arra, int start, int end)
{
    if(start >= end)
    {
        return;
    }
    int i = start;
    int mid = (start+end)/2;
    int j = mid + 1;
    //递归
    mergeSort(arra,i,mid);
    mergeSort(arra,j,end);
    int *temp = (int*)malloc((end-start+1)*sizeof(int));
    int index = 0;
    while (i<=mid && j<=end)
    {
        (arra[i] <= arra[j]) ? temp[index++] = arra[i++] : temp[index++] = arra[j++];
    }
    while(i <= mid)
    {
        temp[index++] = arra[i++];
    }
    while(j <= end)
    {
        temp[index++] = arra[j++];
    }
    for(int k = start;k<=end;k++)
    {
        arra[k] = temp[k-start];
    }
    free(temp);
}


/******************************REFERENCE******************************/

//返回一个与resnName相同(text)的QStandardItem指针
QStandardItem* SoapPublicFunc::findSameItem(QString resName, QStandardItem *parentItem)
{
    QStandardItem *item = nullptr;
    for(int i = 0 ;i<parentItem->rowCount() ; i++)
    {
        if(resName == parentItem->child(i,0)->text())
        {
            item = parentItem->child(i,0);
            break;
        }
        else if(parentItem->hasChildren())
        {
            //递归,只要item不为空则意味找到了,直接返回
            item = findSameItem(resName,parentItem->child(i,0));
            if(item != nullptr)
            {
                break;
            }
        }
    }
    return item;
}

//抛硬币函数(单次)
int SoapPublicFunc::coinFlip()
{
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    return (qrand() % 2);
}

//抛硬币(多次)(times 过大的情况下依然需要放入子线程中处理)
QList<int> SoapPublicFunc::coinFlip(quint32 times)
{
    if(times==0)
    {
        times = 1;
    }
    QList<int> coinList;
    for(unsigned int i = 0 ;i<times;i++)
    {
        coinList.append(coinFlip());
    }
    return coinList;
}

//获取本机主机名
QString SoapPublicFunc::getLocalHostName()
{
    return QHostInfo::localHostName();
}

//获取本地主机相关的IP地址
QList<QHostAddress> SoapPublicFunc::getLocalHostIpList()
{
    QString hostName = QHostInfo::localHostName();
    QHostInfo hostInfo = QHostInfo::fromName(hostName);
    QList<QHostAddress> hostIpList = hostInfo.addresses();
    return hostIpList;
}

//返回一个x!(x的阶乘) (警告,该函数的参数不能过大！)
quint64 SoapPublicFunc::factorial(quint8 x)
{
    if(x == 0 || x == 1)
    {
        return 1;
    }
    else
    {
        return factorial(x-1)*x;
    }
}

/*
 * 递归的优缺点
 * 优点：
 * 1.代码量小 , 函数更加简洁， 在某些场合下比循环更好 , 遍历一个组织树的节点 , 树的前序，中序，后序遍历
 * 缺点：
 * 1.递归由于是函数调用自身，而函数调用是有时间和空间的消耗的：每一次函数调用，都需要在内存栈中分配空间以保存参数、返回地址以及临时变量，而往栈中压入数据和弹出数据都需要时间
 * 2.递归中很多计算都是重复的，由于其本质是把一个问题分解成两个或者多个小问题，多个小问题存在相互重叠的部分，则存在重复计算，如fibonacci斐波那契数列的递归实现
 * 3.调用栈可能会溢出，每一次函数调用会在内存栈中分配空间，而每个进程的栈的容量是有限的，当调用的层次太多时，就会超出栈的容量，从而导致栈溢出
*/

//QStringList 去重函数
int SoapPublicFunc::stringListRemoveDuplicates(QStringList *myStringList)
{
    int n = myStringList->size();
    int j = 0;
    QSet<QString> seen;
    seen.reserve(n);
    int setSize = 0;
    for(int i=0;i<n;i++)
    {
        const QString &s = myStringList->at(i);
        seen.insert(s);
        if(setSize == seen.size())
        {
            continue;
        }
        ++setSize;
        if(j!=i)
        {
            myStringList->swapItemsAt(i,j);
        }
        ++j;
    }
    if(n!=j)
    {
        myStringList->erase(myStringList->begin() + j,myStringList->end());
    }
    return n-j;
}

//将时间(毫秒)转换为 分钟:秒 的格式QString 返回
QString SoapPublicFunc::msTo_MinSec(quint64 ms)
{
    quint64 sec = ms/1000;
    const quint64 min = sec/60;
    sec = sec - min*60;
    return QStringLiteral("%1:%2").arg(min,2,10,QLatin1Char('0'))
                                  .arg(sec,2,10,QLatin1Char('0'));
}

//为QString变量添加{}  (示例: 123 -> {123}) (注意:该函数的形参为引用,会改变代入的原参数)
void SoapPublicFunc::addBraceForString(QString &str)
{
    //若str已经以"{"开始和"}"结束,跳出
    if(str.startsWith("{") && str.endsWith("}"))
    {
        return;
    }
    str.push_front("{");
    str.push_back("}");
}

//为QByteArray变量添加{}  (示例: 123 -> {123})
void SoapPublicFunc::addBraceForByteArray(QByteArray &byteArra)
{
    if(byteArra.startsWith("{") && byteArra.endsWith("}"))
    {
        return;
    }
    byteArra.push_front("{");
    byteArra.push_back("}");
}

//为QString变量取出最外层的{}  (示例:{123} -> 123)
void SoapPublicFunc::delBraceForString(QString &str)
{
    //只在str以"{"开始和"}"结束的情况下执行
    if(str.startsWith("{") && str.endsWith("}"))
    {
        str.remove(0,1);
        str.remove(str.length()-1,1);
    }
}

//取出一个绝对路径下的文件数据(若文件不存在或打开失败,取出的数据都为空)
QByteArray SoapPublicFunc::getAbsoluteFileData(QString absoluteFilePath)
{
    //若文件不存在返回 空
    if(isFileNameExist(absoluteFilePath) == false)
    {
        return "";
    }
    QFile file(absoluteFilePath);
    //若文件打开失败返回 空
    if(file.open(QFile::ReadOnly) == false)
    {
        return "";
    }
    QByteArray fileData = file.readAll();
    //关闭文件
    if(file.isOpen())
    {
        file.close();
    }
    //返回文件数据
    return fileData;
}

//将数据写入一个绝对路径下的文件
bool SoapPublicFunc::setAbsoluteFileData(QString absoluteFilePath,QByteArray fileData)
{
    bool isDone;
    //若指定的文件不存在,则会创建一个
    QFile file(absoluteFilePath);
    file.open(QFile::ReadWrite);
    file.resize(0);
    qint64 len = file.write(fileData);
    file.close();
    //若给定的数据为空,则文件同样会清空(但在该情况下会返回false)
    (len>0) ? isDone = true : isDone = false;
    return isDone;
}

//将数据加入一个绝对路径下的文件(在文件尾添加)
void SoapPublicFunc::appendAbsoluteFileData(QString absoluteFilePath,QByteArray fileData)
{
    QFile file(absoluteFilePath);
    file.open(QFile::Append);
    file.write(fileData);
    file.close();
}

//在一个指定路径下新建指定名称的文件夹(若给定的路径不存在或给定路径下已存在同名的文件夹,跳出)
bool SoapPublicFunc::createFolderInPath(QString absolutePath,QString folderName)
{
    bool isCreated = false;
    //若给定的路径不存在或路径下已存在同名文件
    if(isFilePathExist(absolutePath) == false || isFileNameExist(absolutePath + "/" + folderName))
    {
        return false;
    }
    QDir dir;
    isCreated = dir.mkdir(absolutePath + "/" + folderName);
    return isCreated;
}

//新建一条路径(若路径已存在,跳出)
bool SoapPublicFunc::createPath(QString absolutePath)
{
    bool isCreated = false;
    //若路径不存在,创建一个路径
    if(isFilePathExist(absolutePath) == false)
    {
        QDir dir;
        isCreated = dir.mkpath(absolutePath);
    }
    return isCreated;
}

//打开给定的路径
bool SoapPublicFunc::openGivenUrl(QUrl url)
{
    return QDesktopServices::openUrl(url);
}

//更改一个ip的段落值(QString变量的某一段值) (例如 alterIpSectionAt("128.0.1.2",0,224)   则会变成 "224.0.1.2")
QString SoapPublicFunc::alterIpSection(QString ip,quint16 alterSection,quint16 alterContent)
{
    QStringList ipList = ip.split(".");
    ipList[alterSection] = QString::number(alterContent);
    return ipList.join(".");
}

//更改一个IP的段落值,重载
//void SoapPublicFunc::alterIpSection(QString &ip, quint16 alterSection,quint16 alterContent)
//{
//    QStringList ipList = ip.split(".");
//    ipList[alterSection] = QString::number(alterContent);
//    ip = ipList.join(".");
//}

//取出一个IP的段落值
int SoapPublicFunc::getIpSection(QString ip,quint16 getSection)
{
    int port;
    QStringList ipList = ip.split(".");
    (getSection <=3 && getSection >=0) ? port = ipList.at(getSection).toInt()
                                       : port = 0;
    return port;
}

//返回指定长度的随机字符串(密码)
QByteArray SoapPublicFunc::genRandomString(quint32 strLen,bool isContainLowercase,bool isContainCapital,bool isContainSymbol)
{
    QByteArray randomString;
    //根据要求设置基础字符库
    QByteArray baseStr = "0123456789";
    if(isContainLowercase)
    {
        baseStr.append("abcdefghijklmnopqrstuvwxyz");
    }
    if(isContainCapital)
    {
        baseStr.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }
    if(isContainSymbol)
    {
        baseStr.append("`~!@#$%^&*()_+-=[]{};:|<>,.?");
    }
    //设置随机数种子
    qsrand(QDateTime::currentMSecsSinceEpoch());
    //生成指定长度的字符串
    for(quint32 i=0;i<strLen;i++)
    {
        randomString.append(baseStr[rand()%(baseStr.length()-1)]);
    }
    return randomString;
}

//QByteArray 转指定编码格式
QString SoapPublicFunc::fromTextCondec(const QByteArray &array,const char *name)
{
    if(array.isNull())
    {
        return QString("");
    }
    else
    {
        const char *data = array.data();
        int size = qstrnlen(array.constData(),array.size());
        if(size == 0 || (!*data && size < 0))
        {
            QStringDataPtr empty = {QStringData::allocate(0)};
            return QString(empty);
        }
#if QT_CONFIG(textcodec)
        if(size<0)
        {
            size = qstrlen(data);
        }
        QTextCodec *codec = QTextCodec::codecForName(name);
        if(codec)
        {
            return codec->toUnicode(data,size);
        }
#endif //textcodec
        return QString::fromLatin1(data,size);
    }
}

//根据加密方式加密明文
QByteArray SoapPublicFunc::soapEncrypt(QCryptographicHash::Algorithm encryptWay,QByteArray originText)
{
    QByteArray cipherText;
    switch (encryptWay)
    {
    case QCryptographicHash::Md4:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Md4).toHex();break;
    case QCryptographicHash::Md5:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Md5).toHex();break;
    case QCryptographicHash::Sha1:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Sha1).toHex();break;
    case QCryptographicHash::Sha224:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Sha224).toHex();break;
    case QCryptographicHash::Sha256:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Sha256).toHex();break;
    case QCryptographicHash::Sha384:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Sha384).toHex();break;
    case QCryptographicHash::Sha512:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Sha512).toHex();break;
    case QCryptographicHash::Keccak_224:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Keccak_224).toHex();break;
    case QCryptographicHash::Keccak_256:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Keccak_256).toHex();break;
    case QCryptographicHash::Keccak_384:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Keccak_384).toHex();break;
    case QCryptographicHash::Keccak_512:cipherText = QCryptographicHash::hash(originText,QCryptographicHash::Keccak_512).toHex();break;
    default:cipherText = "";break;
    }
    return cipherText;
}

//将字符数组逆序
void SoapPublicFunc::reverseArray(char *s)
{
    int len = 0;
    char t;
    while(s[len]!= '\0')
    {
        len++;
    }
    len--;
    for(int i=0;i<len/2;i++)
    {
        t = s[i];
        s[i] = s[len-i];
        s[len-i] = t;
    }
}

//将字符数组逆序(重载1)
void SoapPublicFunc::reverseArray(QByteArray &s)
{
    int len = s.count()-1;
    char t;
    for(int i=0;i<len/2;i++)
    {
        t = s[i];
        s[i] = s[len-i];
        s[len-i] = t;
    }
}

//比较2数大小
int SoapPublicFunc::findBig_2Num(int x,int y)
{
    return (x>y? x:y);
}

//比较2数大小(重载)
int SoapPublicFunc::findBig_2Num(double x,double y)
{
    return (x>y? x:y);
}

/******************************SOAP LYRICS LIB******************************/
//Song of the lonely mountain歌词
QByteArray SoapPublicFunc::lyrics_songOfTheLonelyMountain()
{
    QByteArray lyrics;
    lyrics.append("Far over the misty mountain rise\n");
    lyrics.append("Leave us standing upon the height\n");
    lyrics.append("What was before we see once more\n");
    lyrics.append("Is our kingdom a distant light\n");
    lyrics.append("Firey mountain beneth the moon\n");
    lyrics.append("Words unspoken we'll be there soon\n");
    lyrics.append("For home a song that echos on\n");
    lyrics.append("All who find us will know the tune\n");
    lyrics.append("Some folks we never forget\n");
    lyrics.append("Some kinds we never forgive\n");
    lyrics.append("Haven't seen the back of us yet\n");
    lyrics.append("We will fight as long as we live\n");
    lyrics.append("All eyes on the hidden door\n");
    lyrics.append("To the lonely mountain borne\n");
    lyrics.append("We'll ride in the gathering storm\n");
    lyrics.append("Untill we get our long forgotten gold\n");
    lyrics.append("We lay under the misty mountain's cold\n");
    lyrics.append("In slumber's deep and dream's gold\n");
    lyrics.append("We must away our lives to make\n");
    lyrics.append("And in the darkness a torch we hold\n");
    lyrics.append("From long ago when lentern burned\n");
    lyrics.append("Untill these days our heart we yearned\n");
    lyrics.append("Her fate unknown the arken-stone\n");
    lyrics.append("What was stolen mustbe returned\n");
    lyrics.append("~We must away and make the day~\n");
    lyrics.append("~To find a song for heart and soul~\n");
    lyrics.append("Some folks we never forget\n");
    lyrics.append("Some kinds we never forgive\n");
    lyrics.append("Haven't seen the back of us yet\n");
    lyrics.append("We will fight as long as we live\n");
    lyrics.append("All eyes on the hidden door\n");
    lyrics.append("To the lonely mountain borne\n");
    lyrics.append("We'll ride in the gathering storm\n");
    lyrics.append("Untill we get our long forgotten gold\n");
    lyrics.append("Far away from misty mountains cold\n");
    return lyrics;
}

//The lion's roar 歌词
QByteArray SoapPublicFunc::lyrics_theLionsRoar()
{
    QByteArray lyrics;
    lyrics.append("Now the pale morning sings of forgotten things\n");
    lyrics.append("She plays a tune of those who wish to overlook\n");
    lyrics.append("The fact that they've been blindy deceived\n");
    lyrics.append("By those who preach and pray and teach\n");
    lyrics.append("But she falls short and the night explodes in laughter\n");
    lyrics.append("Don't you come here and say i didn't warn you\n");
    lyrics.append("Back way your world can alter\n");
    lyrics.append("And oh how you try to command it all still\n");
    lyrics.append("Every single time it all shifts one way or another\n");
    lyrics.append("And i am a goddamn coward but then again so are you\n");
    lyrics.append("And the lion's roar the lion's roar\n");
    lyrics.append("Has me evading and hollering for you\n");
    lyrics.append("And i never really knew what to do\n");
    lyrics.append("Now i guess sometimes i wish you were a little more predictable\n");
    lyrics.append("That i could read you just like a book\n");
    lyrics.append("For now i can only guess what's coming next\n");
    lyrics.append("By examining your timid smile and ways of the old old winds blow you back round\n");
    lyrics.append("And i am a goddamn fool but then again so are you\n");
    lyrics.append("And the lion's roar the lion's roar\n");
    lyrics.append("Has me seeking out and searching for you\n");
    lyrics.append("And i never really knew what to do\n");
    lyrics.append("Sometimes i wish i could find my Rosemary hill\n");
    lyrics.append("I'd sit there and look at deserted lakes and i'd sing\n");
    lyrics.append("And every once in a while i'd sing a song for you\n");
    lyrics.append("That would rise above the mountains and the stars and the sea\n");
    lyrics.append("And if i wanted it to it would lead you back to me\n");
    lyrics.append("And the lion's roar the lion'd roar\n");
    lyrics.append("Is something that i have heard before\n");
    lyrics.append("A children's tale the lonesome wail of a lion's roar");
    return lyrics;
}

//Long road to hell 歌词
QByteArray SoapPublicFunc::lyrics_longRoadToHell()
{
    QByteArray lyrics;
    lyrics.append("Yeah~ Story goes\n");
    lyrics.append("Here we down to the cross-roads\n");
    lyrics.append("He sat down and kept his eyes closed\n");
    lyrics.append("Everyone has a price child,and what it cost him\n");
    lyrics.append("Was his soul\n");
    lyrics.append("You think you're getting away\n");
    lyrics.append("No escaping the deal you made\n");
    lyrics.append("You walked into his plan\n");
    lyrics.append("Run as fast as you think you can\n");
    lyrics.append("Back from wherever you came\n");
    lyrics.append("Call devil by any name\n");
    lyrics.append("oh no!Now who do you blame\n");
    lyrics.append("Should're listened to what i say.What i say,what i say ,Yeah!");
    lyrics.append("I don't know if i'll be coming home again,home again\n");
    lyrics.append("No,I don't know if i'll be coming home\n");
    lyrics.append("It's a long road to hell without no soul\n");
    lyrics.append("Just when do you think you're getting away\n");
    lyrics.append("No escaping the deal you made\n");
    lyrics.append("You walked into his plan\n");
    lyrics.append("Run as fast as you think you can\n");
    lyrics.append("Back from wherever you came\n");
    lyrics.append("Call devil by any name\n");
    lyrics.append("oh No!Now who do you blame\n");
    lyrics.append("Should're listened to what i say.What i say,what i say , Yeah!");
    lyrics.append("Let me tell you it's a long road to hell without no soul\n");
    return lyrics;
}

//No time to die 歌词
QByteArray SoapPublicFunc::lyrics_noTimeToDie()
{
    QByteArray lyrics;
    lyrics.append("I should have known\n");
    lyrics.append("I'll leave alone\n");
    lyrics.append("Just goes to show\n");
    lyrics.append("That blood you bleed is just the blood you own\n");
    lyrics.append("We were a pair\n");
    lyrics.append("But i saw you there\n");
    lyrics.append("Too much to bear\n");
    lyrics.append("You were my life but life is far away from fair\n");
    lyrics.append("Was i stupid to love you\n");
    lyrics.append("Was i reckless to help\n");
    lyrics.append("Was it obvious to everybody else\n");
    lyrics.append("That i'd fallen for a lie\n");
    lyrics.append("You were never on my side\n");
    lyrics.append("Fool me once ,fool me twice\n");
    lyrics.append("Are you death or paradise\n");
    lyrics.append("Now you'll never see me cry\n");
    lyrics.append("There's just no time to die\n");
    lyrics.append("I let it burn\n");
    lyrics.append("You're no longer my concern\n");
    lyrics.append("Faces from my past return\n");
    lyrics.append("Another lesson yet to learn\n");
    lyrics.append("That i'd fallen for a lie\n");
    lyrics.append("You were never on my side\n");
    lyrics.append("Fool me once,fool me twice\n");
    lyrics.append("Are you death or paradise\n");
    lyrics.append("Now you'll never see me cry\n");
    lyrics.append("There's just no time to die\n");
    lyrics.append("No time to die\n");
    lyrics.append("No time to die\n");
    lyrics.append("Fool me once,fool me twice\n");
    lyrics.append("Are you death or paradise\n");
    lyrics.append("Now you'll never see me cry\n");
    lyrics.append("There's just no time to die\n");
    return lyrics;
}

//Guns for hire 歌词
QByteArray SoapPublicFunc::lyrics_gunsForHire()
{
    QByteArray lyrics;
    lyrics.append("Hold the die your turn to roll\n");
    lyrics.append("Before they fall through your fingers\n");
    lyrics.append("Not a good night to lose control\n");
    lyrics.append("Right as the earth is unraveling\n");
    lyrics.append("You play with blocks until they break\n");
    lyrics.append("And these walls come tumbling down\n");
    lyrics.append("Oh they are tumbling down\n");
    lyrics.append("You're out of time make your move\n");
    lyrics.append("Live or die while the fuse is lit and there's no turning back\n");
    lyrics.append("Kiss your perfect day goodbye\n");
    lyrics.append("Because the world is on fire\n");
    lyrics.append("Tuck your innocence goodnight\n");
    lyrics.append("You sold your firends like guns for hire\n");
    lyrics.append("Go play with your blocks\n");
    lyrics.append("And now you'll pay when these walls come tumbling down\n");
    lyrics.append("Oh they are tumbling down\n");
    lyrics.append("Resting on a knife you heavy souls\n");
    lyrics.append("With all this weight buckling down on you now\n");
    lyrics.append("Don't you drown and float away\n");
    lyrics.append("Not a good time to lose control\n");
    lyrics.append("Right as your marionettes cut their strings and run away\n");
    lyrics.append("You're out of time make your move\n");
    lyrics.append("Live or die while the fuse is lit and these's no turning back\n");
    lyrics.append("Kiss your perfect day goodbye\n");
    lyrics.append("Because the world is on fire\n");
    lyrics.append("Tuck your innocence goodnight\n");
    lyrics.append("You sold your friends like guns for hires\n");
    lyrics.append("Go play with your blocks\n");
    lyrics.append("And now you'll pay when these walls come tumbling down\n");
    lyrics.append("Oh they are tumbling down\n");
    return lyrics;
}
