#ifndef SOAPTRANSMIT_H
#define SOAPTRANSMIT_H

#include <QMainWindow>
#include <QDebug>
#include <QThread>
#include <QFile>
#include <QDir>
#include <QUdpSocket>
#include <QSystemTrayIcon>
#include <QListWidgetItem>
#include <QComboBox>
#include <QLineEdit>
#include <QCloseEvent>
#include <QPushButton>
#include <QShortcut>
#include <QJsonArray>
#include <QMetaType>
#include <QLabel>
#include <QTreeWidget>

//自定义头文件
#include "SoapPublicFunc.h"
#include "BaseOrderLib.h"
#include "ScenesPollingThread.h"
#include "DownloadFileThread.h"
#include "QSliderClick.h"
#include "FileReadThread.h"
#include "FileSaveThread.h"
#include "SoundThread.h"
#include "FileListDisplay.h"
#include "LogDialog.h"
#include "SnapShot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SoapTransmit; }
QT_END_NAMESPACE

class SoapTransmit : public QMainWindow , public BaseOrderLib
{
    Q_OBJECT

public:
    //文件读取错误错误类型
    enum FileReadError
    {
        NoError = 0,                                                //无错误
        SpecifiedPathNotFoundFile,                                  //指定的路径不存在文件
        SpecifiedPathFileOpenFail,                                  //指定路径下的文件打开失败
        SpecifiedPathFileEmpty                                      //指定的路径下的文件信息为空
    };

    //运行分页文件类型
    enum FileType
    {
        IdType = 0,                                                 //信号源格式(信号源ID:名称:主码流:辅码流)
        NodeType,                                                   //转发节点格式(转发节点IP:该节点最大可分配的信号数量:该节点转发的信号IP列表)
        WindowsType,                                                //视频墙信息格式(已开启的视频墙号:该视频墙最近转发的信号IP列表)
        ScenesPollingType,                                          //场景轮询信息格式(正在轮询的视频墙号:场景轮询的切换场景时间间隔)
        OthersType                                                  //其他
    };

    //命令类型
    enum FileOrderType
    {
        GetNodes = 0,                                               //扫描所有节点
        SetLockFrame,                                               //设置锁屏基数
        GetTransmit,                                                //获取信号源的输出组播转发
        SetTransmit,                                                //设定信号源的输出组播转发
        GetWindows,                                                 //从某输出节点获取当前输出节点属于视频墙的实时窗口信息
        SetWindows,                                                 //设定指定视频墙的实时窗口信息
        GetAudios,                                                  //从某个输出节点获取当前该节点的音频信号配置文件信息(单播)
        SetAudios,                                                  //用于设定指定视频墙的音频输出信息(广播/组播/单播)
        Json_GetSignalResourcesTree,                                //获取信号源组织树(Json格式的消息)
        GetSceneState,                                              //从某输出节点获取当前输出节点属于的视频墙的场景编辑状态信息(只能单播)
        SetSceneState,                                              //设定指定视频墙的场景编辑状态信息(支持广播/组播/单播)
        GetScene,                                                   //从某个输出节点获取当前输出节点属于的视频墙的实时场景信息(单播)
        GetScenes,                                                  //从某个输出节点获取当前输出节点属于该视频墙的所有场景信息(单播)
        GetSceneNames,                                              //从某个输出节点获取当前输出节点属于的视频墙的实时场景信息(单播)
        SaveScene,                                                  //保存指定视频墙的场景(广播/组播/单播)
        DeleteScene,                                                //删除指定视频墙的场景(广播/组播/单播)
        ClearScenes,                                                //清空指定视频墙的场景列表(广播/组播/单播)
        CallScene,                                                  //用于调取指定视频墙的场景(广播/组播/单播)
        SetScenesPolling,                                           //同于设置场景轮询(单播) 同一组视频墙,要且近要设定一个节点
        PauseScenesPolling,                                         //场景轮询暂停与继续
        DownloadFile,                                               //用于下载文件,支持单播
        GetClientInfo,                                              //用于获取客户端希望存储于节点上面信息(单播)
        SaveClientInfo,                                             //用于存储客户端希望存储与节点上面信息(广播,组播,单播)
        SubtitleOsd,                                                //用于设置输出节点跑马灯的显示(广播/组播/单播)
        Unknown                                                     //未知类型(不在协议范围内的数据格式)
    };

    //命令发送方式
    enum TransmitWay
    {
        Unicast = 0,                                                //单播
        Multicast,                                                  //组播(多播)
        Broadcast                                                   //广播
    };

    //文件筛选方式
    enum FileSortWay
    {
        ScodeDataFolder = 0,                                        //scodeData文件夹
        ScodeListFolder,                                            //scodeList文件夹
        ScodeLogFolder,                                             //scodeLog文件夹
        ScodeResFolder,                                             //scodeRes文件夹
        OthersFolder                                                //其他(预留用)
    };

    //转发节点状态
    enum NodeState
    {
        InitState = 0,                                              //转发节点状态:初始的(不包含任何转发信号)
        ContainSigState,                                            //从不含任何信号转变为包含信号的状态
        RemoveSigState                                              //从包含信号转变为不包含任何信号状态(在该状态下会先向转发节点发送重置的SetTransmit命令然后将该节点状态置为初始状态)
    };

    //通用时间单位
    enum TimeUnit
    {
        Second = 0,                                                 //秒
        Minute,                                                     //分
        Hour,                                                       //时
        Day,                                                        //天
        Month,                                                      //月
        Year,                                                       //年
        Eternal,                                                    //永远
        Never,                                                      //永不
        OthersTimeUnit                                              //其他单位
    };

    //接收消息时保存来自客户端的信息结构体
    struct ClientIpPortInfo
    {
        QHostAddress ip;                                            //对方IP
        quint16 port;                                               //对方端口
    };
    ClientIpPortInfo clientInfo;                                    //用于保存客户端信息(IP和端口号)

    //接收可能需要回复的IP和端口结构体
    struct RespIpPortInfo
    {
        QHostAddress ip;                                            //回复的IP
        quint16 port;                                               //回复的端口
    };
    RespIpPortInfo respInfo;                                        //保存可能要回复的IP和端口

    //信号源表参数结构体
    struct idListInfo
    {
        QString idListIp;                                           //信号源IP
        QString idListName;                                         //名称
        QString idListMajorStream;                                  //信号源主流URL
        QString idListMinorStream;                                  //信号源辅流URL
    };
    QList<idListInfo*> idListInfoList;                              //信号源表参数链表

    //转发表参数结构体
    struct nodeListInfo
    {
        QString nodeListIp;                                         //转发节点ip
        int nodeListTransmitSigLim;                                 //该转发节点可分配的信号上限
        QStringList transmitedIpList;                               //该转发节点已被转发的IP列表(数量只可能小于等于该转发节点可分配信号上限)
        NodeState currNodeState;                                    //该转发节点当前的状态
    };
    QList<nodeListInfo*> nodeListInfoList;                          //转发表参数链表

    //视频墙及对应信号列表
    struct windowsInfo
    {
        int windowsNum;                                             //视频墙号
        QStringList transmitedIpList;                               //该视频墙历史转发的信号列表
        QByteArray lastSetWindowsCont;                              //该视频墙最近收到的setWindows参数
    };
    QList<windowsInfo*> windowsInfoList;                            //视频墙参数链表

    //场景轮询结构体
    struct scenesPollingInfo
    {
        int scenesPollingPanel;                                     //场景轮询对应的视频墙号
        int scenesPollingTime;                                      //场景轮询的场景间时间间隔
        QStringList scenesPollingList;                              //场景轮询的setWindows命令列表
        bool scenesIsPolling;                                       //该场景是否正在轮询
        QThread *scenesSubThread;                                   //场景轮询子线程
        ScenesPollingThread *scenesPollingThread;                   //场景轮询自定义线程对象
        bool closeAfterPause;                                       //标识场景轮询是否在暂停后关闭子线程
    };
    QList<scenesPollingInfo*> scenesPollingInfoList;                //场景轮询参数链表
    quint16 maxScenesPollingThread = 5;                             //可分配最大的场景轮询子线程数量

    //传输文件结构体
    struct downloadFileInfo
    {
        int downloadFilePanel;                                      //传输文件对应的视频墙号
        QString downloadFileName;                                   //传输文件的文件名称
        quint16 downloadFilePort;                                   //传输文件的发送端口号
        QString downloadFileIp;                                     //传输文件的发送IP
        quint32 downloadFilePackLen;                                //传输文件的每包长度
        quint32 downloadFileInterval;                               //传输文件的每包之间的时间间隔(单位:微秒us)
        QList<quint32> downloadFilePack_sn;                         //传输文件的指定的包号(可能省略)
        bool downloadFileAbort;                                     //标识传输文件是否要强制终止
        DownloadFileThread::DownloadFileState downloadFileState;    //传输文件线程的当前状态
        QThread *downloadFileSubThread;                             //传输文件子线程
        DownloadFileThread *downloadFileThread;                     //传输文件自定义线程对象
    };
    QList<downloadFileInfo*> downloadFileInfoList;                  //传输文件参数链表
    quint16 maxDownloadFileThread = 5;                              //传输文件最大子线程数量

    //接收日志结构体
    struct recvLogInfo
    {
        bool recvLogSave = true;                                    //标志是否开启接收日志保存
        QByteArray recvLog = "";                                    //接收日志变量
        QByteArray recvLogErr = "";                                 //保存当前接收命令的错误细节
        int recvLogUseLife = 1;                                     //接收日志的有效期
        int recvLogUseLifeUnit = TimeUnit::Month;                   //接收日志有效期单位
    };
    recvLogInfo myRecvLogInfo;

    //发送日志结构体
    struct sendLogInfo
    {
        bool sendLogSave = true;                                    //标志是否开启发送日志保存
        QByteArray sendLog = "";                                    //发送日志变量
        int sendLogUseLife = 1;                                     //发送日志的有效期
        int sendLogUseLifeUnit = TimeUnit::Month;                   //发送日志的有效期单位
    };
    sendLogInfo mySendLogInfo;

    //场景状态结构体(setSceneState)
    struct setSceneStateInfo
    {
        int panel_sn;                                               //视频墙号
        int time;                                                   //0:允许编辑, >0:被占用,不允许编辑,设定时间后自动解除锁定
    };
    QList<setSceneStateInfo*> setSceneStateInfoList;                //场景状态链表

    //抓图控制结构体
    SnapParam snapParam;

    //保存数据的文件夹名
    const QString scodeDataFolderName = "scodeData";
    //软件各类参数设置文件
    const QString transmitDataFileName = "transmitData.json";

    //保存各类列表文件夹
    const QString scodeListFolderName = "scodeList";
    //转发表名
    const QString nodeListFileName = "nodeList.txt";
    //信号源表名
    const QString idListFileName = "idList.txt";

    //保存资源用文件夹名
    const QString scodeResourceFolderName = "scodeRes";

    //保存日志用文件夹(根据日期区分)
    const QString scodeLogFolderName = "scodeLog";

    //节点转发程序关键文件名称列表(包括后缀)
    const QStringList transmitKeyFileList = QStringList()<<transmitDataFileName<<nodeListFileName<<idListFileName;

    //主体字形
    const QFont bodyFont = QFont("微软雅黑",11);
    //菜单字形
    const QFont menuFont = QFont("微软雅黑",12);

    //txt图标
    const QIcon txtIcon = QIcon(":/Transmit/txt_Blue.png");
    //筛选(分类)图标
    const QIcon sortIcon = QIcon(":/Transmit/sort_Blue.png");

    //常用量
    const quint32 msgBoxWindowShowTime = 5000;                  //提示窗口持续时间
    const quint32 statusBarShowTime = 3000;                     //状态栏信息持续时间
    const quint32 statusBarShowTime_error = 5000;               //状态栏错误持续时间


    //运行信息名称列表
    const QStringList runInfoNameList = QStringList()<<"信号源信息"<<"转发节点信息"<<"视频墙信息"<<"场景轮询信息";

public:
    //构造
    SoapTransmit(QWidget *parent = nullptr);

    //析构
    ~SoapTransmit();

    //初始化创建文件夹和txt文件,用于保存可能接收的scodeOrders
    void initCreateFoler();

    //初始化运行信息分页
    void initRunInfoTab();

    //将数据写入(覆盖)指定的文件(需要文件的绝对路径)
    bool setAbsoluteFileData(QString absoluteFilePath,QByteArray fileData);

    //取出一个绝对路径下的文件数据
    QByteArray getAbsoluteFileData(QString absoluteFilePath);

    //系统托盘图标单击(双击)槽
    void trayIconActivatedSlot(QSystemTrayIcon::ActivationReason reason);

    //筛选文件夹(scodeList)
    void sort_scodeList();

    //筛选文件夹(scodeData)
    void sort_scodeData();

    //筛选文件夹(scodeLog)
    void sort_scodeLog();

    //筛选文件夹(scodeRes)
    void sort_scodeRes();

    //文件列表右键菜单 打开文件所在位置
    void openOrderFileDir();

    //文件列表右键菜单 查看内容
    void examineOrderFile();

    //文件列表右键菜单 删除文件
    void deleteFile();

    //将命令头转换为枚举值
    FileOrderType getOrderType(QString orderKey);

    //设置命令文件筛选方式
    void setFileSortWay(FileSortWay way);

    //取出命令文件筛选方式
    FileSortWay getFileSortWay();

    //设置当前UDP接收的数据
    void setCurrUdpRecvData(QByteArray currData);

    //取出当前UDP接收的数据
    QByteArray getCurrUdpRecvData();

    //设置客户端信息(IP和端口)
    void setClientInfo(ClientIpPortInfo clientInfo);

    //取出客户端信息(IP和端口)
    ClientIpPortInfo getClientInfo();

    //设置需要回复端的信息(IP和端口)
    void setRespInfo(RespIpPortInfo respInfo);

    //取出当前回复端的信息(IP和端口)
    RespIpPortInfo getRespInfo();

    //设置信号源表和转发表是否正确
    void setIsIdAndNodeListCorrect(bool isCorrect);

    //取出信号源表和转发表标识
    bool getIsIdAndNodeListCorrect();

    //取出命令头
    QString getOrderKey(QByteArray order);

    //UDP发送套接字接收消息槽
    void udpSockRecvData_toSendSock();

    //UDP接收套接字接收消息槽
    void udpSockRecvData();

    //UDP收到消息时的处理函数(总函数)
    void udpRecvOrder(QByteArray orderData);

    //UDP收到消息时的处理函数(GetNodes)
    void udpRecvOrder_GetNodes(QByteArray getNodes);

    //UDP收到消息时的处理函数(SetLockFrame)
    void udpRecvOrder_SetLockFrame(QByteArray setLockFrame);

    //UDP收到消息时的处理函数(GetTransmit)
    void udpRecvOrder_GetTransmit(QByteArray getTransmit);

    //UDP收到消息时的处理函数(SetTransmit)
    void udpRecvOrder_SetTransmit(QByteArray setTransmit);

    //UDP收到消息时的处理函数(GetWindows)
    void udpRecvOrder_GetWindows(QByteArray getWindows);

    //UDP收到消息时的处理函数(SetWindows)
    void udpRecvOrder_SetWindows(QByteArray setWindows);

    //UDP收到消息时的处理函数(JSON格式的(GetSignalResourcesTree))
    void udpRecvOrder_Json_GetSignalResourcesTree();

    //UDP收到消息时的处理函数(GetSceneState)
    void udpRecvOrder_GetSceneState(QByteArray getSceneState);

    //UDP收到消息时的处理函数(SetSceneState)
    void udpRecvOrder_SetSceneState(QByteArray setSceneState);

    //UDP收到消息时的处理函数(GetScene)
    void udpRecvOrder_GetScene(QByteArray getScene);

    //UDP收到消息时的处理函数(GetScenes)
    void udpRecvOrder_GetScenes(QByteArray getScenes);

    //UDP收到消息时的处理函数(GetSceneNames)
    void udpRecvOrder_GetSceneNames(QByteArray getSceneNames);

    //UDP收到消息时的处理函数(SaveScene)
    void udpRecvOrder_SaveScene(QByteArray saveScene);

    //UDP收到消息时的处理函数(DeleteScene)
    void udpRecvOrder_DeleteScene(QByteArray deleteScene);

    //UDP收到消息时的处理函数(ClearScenes)
    void udpRecvOrder_ClearScenes(QByteArray clearScenes);

    //UDP收到消息时的处理函数(CallScene)
    void udpRecvOrder_CallScene(QByteArray callScene);

    //UDP收到消息时的处理函数(SetScenesPolling)
    void udpRecvOrder_SetScenesPolling(QByteArray setScenesPolling);

    //UDP收到消息时的处理函数(PauseScenesPolling)
    void udpRecvOrder_PauseScenesPolling(QByteArray pauseScenesPolling);

    //UDP收到消息时的处理函数(DownloadFile)
    void udpRecvOrder_DownloadFile(QByteArray downloadFile);

    //UDP收到消息时的处理函数(GetClientInfo)
    void udpRecvOrder_GetClientInfo(QByteArray getClientInfo);

    //UDP收到消息时的处理函数(SaveClientInfo)
    void udpRecvOrder_SaveClientInfo(QByteArray saveClientInfo);

    //UDP收到消息时的处理函数(GetAudios)
    void udpRecvOrder_GetAudios(QByteArray getAudios);

    //UDP收到消息时的处理函数(SetAudio)
    void udpRecvOrder_SetAudios(QByteArray setAudios);

    //UDP收到消息时的处理函数(SubtitleOsd)
    void udpRecvOrder_SubtitleOsd(QByteArray subtitleOsd);

    //Json_GetSignalResourcesTree回复示例
    QJsonObject json_getSignalResourcesTree();

    //场景Json示例
    QJsonObject sceneJson();

    //场景轮询Json示例
    QJsonObject scenesPollingJson();

    //端口计算
    quint16 portCacul(QString ip);

    //重新将信号源列表和转发列表内容读入内存
    bool reloadIdAndNodeListData();

    //transmit程序的弹窗提示
    void transMsgBoxRemind(QString text);

    //transmit程序的弹窗提示(重载1)
    void transMsgBoxRemind(QString text,quint32 ms);

    //transmit程序的弹窗提示(重载2)
    void transMsgBoxRemind(QString text,quint32 ms,QString iconUrl);

    //transmit程序模态弹窗
    void transModelMsgBoxRemind(QString text = "",quint32 ms = 1000,QString iconUrl = "",QFont font = QFont("微软雅黑",11));

    //处理子线程轮询开始后的参数回传
    void deal_scenesPollingCurrScene(QString setWindows);

    //处理子线程轮询暂停后的回传
    void deal_scenesPollingIsPaused(int panel_sn);

    //处理传输文件的状态参数回传
    void deal_downloadFileSituation(int panel_sn,DownloadFileThread::DownloadFileState state);

    //处理读取文件的内容回传
    void deal_fileReadContent(QByteArray content);

    //处理读取文件的状态回传
    void deal_fileReadState(FileReadThread::fileState state,QString fileName);

    //处理发送套接字定时触发槽
    void deal_sendMsgTimeOut();

    //返回当前时间
    QString currTime();

    //返回当前日期
    QString currDate();

    //软件参数默认值
    QJsonObject defSelfSetting();

    //根据日志保存的参数删除文件
    void recvLogFileAdjust();

    //保存软件参数
    void saveTransmitParams();

    //显示主窗(系统托盘图标菜单项)
    void showMainWindow();

    //关闭(系统托盘图标菜单项)
    void closeMainWindow();

    //处理FileListDisplay的信号回传
    void deal_infoListSendBack(QList<QStringList> content,FileListDisplay::InfoType type);

    //处理LogDialog的信号回传
    void deal_logContentSendBack(bool recvLogSave,QList<int> recvLogList,bool sendLogSave,QList<int> sendLogList);

    //处理SnapShot的信号回传
    void deal_snapContentSendBack(int snapW,int snapH,int snapL,int snapDelay,int snapQua,bool mcastEnable,int mcastFre,QString snapType);

    //请求转发节点IP列表
    bool requireNodeInfo();

    /******************************OVERRIDE******************************/
    //主窗口关闭函数 override
    void closeEvent(QCloseEvent *event) override;

    /******************************测试******************************/


signals:
    /******************************场景轮询相关信号******************************/
    //开始轮询信号
    void sig_scenesPollingStart(int panel_sn,QStringList setWindowsList,int time);
    //暂停轮询信号
    void sig_scenesPollingPause(int panel_sn);

    /******************************传输文件相关信号******************************/
    //文件传输开始信号
    void sig_downloadFileStart(int panel_sn,QString fileName,QString ip,quint16 port,quint32 len,quint32 interval,QList<quint32> pack_sn);

    //文件传输暂停(强行终止)信号
    void sig_downloadFileAbort(int panel_sn);

    /******************************读取文件相关信号******************************/
    //文件读取开始信号
    void sig_startFileRead(QString filePath);

    //文件读取暂停信号
    void sig_stopFileRead();

    /******************************保存文件相关信号******************************/
    //保存文件信号
    void sig_fileSave(QString absoluteFileName,QByteArray content);

private slots:

    /******************************私有槽******************************/
    //命令列表单击槽
    void on_listWidget_orderList_itemClicked(QListWidgetItem *item);

    //命令列表右键菜单
    void on_listWidget_orderList_customContextMenuRequested(const QPoint &pos);

    //运行信息表 对象单击槽
    void on_listWidget_runInfo_itemClicked(QListWidgetItem *item);

    //树形控件对象单击槽
    void on_treeWidget_opeSet_itemClicked(QTreeWidgetItem *item, int column);

private:
    /******************************私有成员******************************/
    Ui::SoapTransmit *ui;

    QMenu *trayMenu;                                    //系统托盘图标菜单
    QSystemTrayIcon *trayIcon;                          //系统托盘图标
    FileSortWay sortWay;                                //文件筛选方式(通过不同的文件夹)
    QUdpSocket *udpRecvSock;                            //udp接收套接字
    QUdpSocket *udpSendSock;                            //udp发送套接字
    quint16 udpListPort;                                //udp套接字监听的端口号
    QByteArray currUdpRecvData;                         //保存当前UDP接收到的数据
    QStringList ipHasFoundList;                         //保存当前SetWindows中与idList中相同的IP
    bool isIdAndNodeListCorrect;                        //标识信号源表和转发表是否正确
    int nodeListSigNum;                                 //转发表可转发的信号总数
    QString lastSetWindows;                             //保存最近收到的SetWindows命令

    QThread *fileReadSubThread;                         //读取文件子线程
    FileReadThread *fileReadThread;                     //读取文件自定义线程

    QThread *fileSaveSubThread;                         //保存文件子线程
    FileSaveThread *fileSaveThread;                     //保存文件自定义线程

    FileListDisplay *fileListDisplayWindow;             //内容显示窗口

    QStringList nodeIpList;                             //暂存转发节点IP列表变量
    QTimer *sendMsgTimer;                               //发送Udp数据定时器(发送数据时开启,当超出额定时间后没有收到特定消息则返回错误)

};
#endif // SOAPTRANSMIT_H
