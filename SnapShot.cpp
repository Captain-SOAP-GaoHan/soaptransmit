#include "SnapShot.h"
#include "ui_SnapShot.h"

//构造
SnapShot::SnapShot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SnapShot)
{
    ui->setupUi(this);

    /******************************本窗体的基础设置******************************/
    this->setAttribute(Qt::WA_DeleteOnClose);                                               //设置在关闭时释放
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::Dialog);   //窗口相关设置
    this->hide();

    //若父对象不为空,根据父对象的大小设置大小,若为空则需要额外设置样式表
    if(parent != nullptr)
    {
        this->resize(parent->width(),parent->height());
    }
    else
    {
        //设置子窗口的最小大小
        this->setMinimumSize(400,200);
        //加载样式表
        QFile styleSheetFile(":/qss/qss/blue.css");
        styleSheetFile.open(QFile::ReadOnly);
        this->setStyleSheet(styleSheetFile.readAll());
        styleSheetFile.close();
    }

    //控件初始化
    ui->box_snapType->addItems(QStringList()<<"bypass"<<"H264"<<"H265");
    ui->box_snapMcastEnable->addItems(QStringList()<<"是"<<"否");
    //设置输入范围
    ui->lineE_snapWid->setValidator(new QIntValidator(0,4096,this));
    ui->lineE_snapHei->setValidator(new QIntValidator(0,2160,this));
    ui->lineE_snapLen->setValidator(new QIntValidator(0,4096,this));
    ui->lineE_snapQua->setValidator(new QIntValidator(0,100,this));
    ui->lineE_snapDelay->setValidator(new QIntValidator(0,1000,this));
    ui->lineE_snapMcastFreq->setValidator(new QIntValidator(0,100,this));
}


//析构
SnapShot::~SnapShot()
{
    delete ui;
}

//加载内容
void SnapShot::loadContent(int snapWid,int snapHei,int snaplen,int snapDelay,int snapQua,bool mcastEnable,int mcastFre,QString snapType)
{
    //设置参数
    ui->lineE_snapWid->setText(QString::number(snapWid));
    ui->lineE_snapHei->setText(QString::number(snapHei));
    ui->lineE_snapLen->setText(QString::number(snaplen));
    ui->lineE_snapDelay->setText(QString::number(snapDelay));
    ui->lineE_snapQua->setText(QString::number(snapQua));
    (mcastEnable == true) ? ui->box_snapMcastEnable->setCurrentIndex(0) : ui->box_snapMcastEnable->setCurrentIndex(1);
    ui->lineE_snapMcastFreq->setText(QString::number(mcastFre));
    if(snapType == "bypass")
    {
        ui->box_snapType->setCurrentIndex(0);
    }
    else if(snapType == "H264")
    {
        ui->box_snapType->setCurrentIndex(1);
    }
    else if(snapType == "H265")
    {
        ui->box_snapType->setCurrentIndex(2);
    }
    else
    {
        qDebug()<<"警告!错误的抓图类型!请检查!";
    }
}

//取消按钮按下
void SnapShot::on_cancelBtn_clicked()
{
    //关闭窗口
    this->close();
}

//确定按钮按下
void SnapShot::on_yesBtn_clicked()
{
    //提取参数
    QString snapW,snapH,snapLen,snapQua,snapDelay,snapFreq;
    snapW = ui->lineE_snapWid->text();
    snapH = ui->lineE_snapHei->text();
    snapLen = ui->lineE_snapLen->text();
    snapQua = ui->lineE_snapQua->text();
    snapDelay = ui->lineE_snapDelay->text();
    snapFreq = ui->lineE_snapMcastFreq->text();
    if(snapW.isEmpty() || snapH.isEmpty() || snapLen.isEmpty() || snapQua.isEmpty() || snapDelay.isEmpty() || snapFreq.isEmpty())
    {
        //弹窗提示错误
        SoapPublicFunc::modelessBoxRemind("警告!存在未设置的参数!\n无法保存抓图参数!",3000,this);
        return;
    }
    bool snapMcastEnable;
    (ui->box_snapMcastEnable->currentIndex() == 0) ? snapMcastEnable = true : snapMcastEnable = false;
    QString snapType;
    switch (ui->box_snapType->currentIndex())
    {
    case 0:snapType = "bypass";break;
    case 1:snapType = "H264";break;
    case 2:snapType = "H265";break;
    }
    //参数回传
    emit sig_snapContentSendBack(snapW.toInt(),snapH.toInt(),snapLen.toInt(),snapDelay.toInt(),snapQua.toInt(),snapMcastEnable,snapFreq.toInt(),snapType);
}

//宽度输入栏编辑完成信号
void SnapShot::on_lineE_snapWid_editingFinished()
{
    ui->lineE_snapWid->setText(ui->lineE_snapWid->text().trimmed());
}

//高度输入栏编辑完成信号
void SnapShot::on_lineE_snapHei_editingFinished()
{
    ui->lineE_snapHei->setText(ui->lineE_snapHei->text().trimmed());
}

//长度输入栏编辑完成信号
void SnapShot::on_lineE_snapLen_editingFinished()
{
    ui->lineE_snapLen->setText(ui->lineE_snapLen->text().trimmed());
}

//质量输入栏编辑完成信号
void SnapShot::on_lineE_snapQua_editingFinished()
{
    ui->lineE_snapQua->setText(ui->lineE_snapQua->text().trimmed());
}

//延迟输入栏编辑完成信号
void SnapShot::on_lineE_snapDelay_editingFinished()
{
    ui->lineE_snapDelay->setText(ui->lineE_snapDelay->text().trimmed());
}

//频率输入栏编辑完成信号
void SnapShot::on_lineE_snapMcastFreq_editingFinished()
{
    ui->lineE_snapMcastFreq->setText(ui->lineE_snapMcastFreq->text().trimmed());
}
