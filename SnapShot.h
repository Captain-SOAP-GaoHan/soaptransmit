#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QDialog>
#include <QFile>
#include <QDebug>
#include <QIntValidator>

#include "SoapPublicFunc.h"

namespace Ui
{
class SnapShot;
}

class SnapShot : public QDialog
{
    Q_OBJECT

public:
    //构造
    explicit SnapShot(QWidget *parent = nullptr);
    //析构
    ~SnapShot();

    //加载内容
    void loadContent(int snapWid,int snapHei,int snaplen,int snapDelay,int snapQua,bool mcastEnable,int mcastFre,QString snapType);

signals:
    void sig_snapContentSendBack(int snapWid,int snapHei,int snapLen,int snapDelay,int snapQua,bool mcastEnable,int mcastFre,QString snapType);

private slots:
    //取消按钮按下
    void on_cancelBtn_clicked();

    //确定按钮按下
    void on_yesBtn_clicked();

    //宽度输入栏编辑完成信号
    void on_lineE_snapWid_editingFinished();

    //高度输入栏编辑完成信号
    void on_lineE_snapHei_editingFinished();

    //长度输入栏编辑完成信号
    void on_lineE_snapLen_editingFinished();

    //质量输入栏编辑完成信号
    void on_lineE_snapQua_editingFinished();

    //延迟输入栏编辑完成信号
    void on_lineE_snapDelay_editingFinished();

    //频率输入栏编辑完成信号
    void on_lineE_snapMcastFreq_editingFinished();

private:
    Ui::SnapShot *ui;
};

#endif // SNAPSHOT_H
