QT       += core gui
QT       += network
QT       += multimedia
RC_ICONS = appIcon.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BaseOrderLib.cpp \
    DownloadFileThread.cpp \
    FileListDisplay.cpp \
    FileReadThread.cpp \
    FileSaveThread.cpp \
    LogDialog.cpp \
    QSliderClick.cpp \
    ScenesPollingThread.cpp \
    SnapShot.cpp \
    SoapTabBar.cpp \
    SoundThread.cpp \
    main.cpp \
    SoapTransmit.cpp \
    SoapPublicFunc.cpp

HEADERS += \
    BaseOrderLib.h \
    DownloadFileThread.h \
    FileListDisplay.h \
    FileReadThread.h \
    FileSaveThread.h \
    LogDialog.h \
    QSliderClick.h \
    ScenesPollingThread.h \
    SnapShot.h \
    SoapTabBar.h \
    SoapTransmit.h \
    SoapPublicFunc.h \
    SoundThread.h

FORMS += \
    FileListDisplay.ui \
    LogDialog.ui \
    SnapShot.ui \
    SoapTransmit.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    qss.qrc \
    res.qrc



