#ifndef FILEREADTHREAD_H
#define FILEREADTHREAD_H

#include <QObject>
#include <QFile>
#include <QThread>

#include "SoapPublicFunc.h"

class FileReadThread : public QObject
{
    Q_OBJECT
public:
    //状态枚举量
    enum fileState
    {
        fileOpenFail,
        fileTooLarge,
        fileTypeUnsupported,
        fileReadDone,
        fileReadAbort
    };

    //构造
    explicit FileReadThread(QObject *parent = nullptr);

    //开始读取数据
    void deal_StartReadFile(QString AbsFilePath);

    //暂停文件读取
    void deal_stopReadFile();

signals:
    //返回每次读取的文件数据
    void sig_fileContent(QByteArray fileContent);

    //返回报错信息
    void sig_sitState(fileState state,QString fileName);

private:
    QFile file;                                 //文件对象
    QString filePath;                           //文件路径
    quint64 fileSize;                           //文件大小(长度)
    bool readFileFlag;                          //标识是否读取文件状态
};

#endif // FILEREADTHREAD_H
