#include "SoundThread.h"

//构造
SoundThread::SoundThread(QObject *parent) : QObject(parent)
{
    //私有成员初始化
    player = new QMediaPlayer(this);
    playList = new QMediaPlaylist(this);
    setPlayerState(false);

    //初始化时歌单列表为空
    musicListInfo.currMode = QMediaPlaylist::PlaybackMode::Loop;
    musicListInfo.currMusicList.clear();
    musicListInfo.currMusicListIndex = 0;
}

//处理音效播放(单次单个的音效文件)
void SoundThread::deal_soundEffectPlay(QString soundFilePath)
{
    //音效只播放一次
    QSound soundPlay(soundFilePath,this);
    soundPlay.setLoops(1);
    soundPlay.play();
}

//处理设置音乐列表
void SoundThread::deal_setMusicList(QStringList musicList,quint32 index,QMediaPlaylist::PlaybackMode mode,bool playNow)
{
    //若播放器在播放,暂停
    if(getPlayerState() == true)
    {
        player->stop();
        setPlayerState(false);
    }

    /******************************检查传递的参数和现有的参数的区别,根据情况处理******************************/
    //检查歌单列表是否一致
    if(musicList != musicListInfo.currMusicList)
    {
        //重置播放列表
        playList->clear();
        QList<QMediaContent> mediaList;
        for(int i=0;i<musicList.count();i++)
        {
            mediaList.append(QUrl(musicList.at(i)));
        }
        playList->addMedia(mediaList);
        musicListInfo.currMusicList = musicList;

    }
    //检查下标是否一致
    if(index != musicListInfo.currMusicListIndex)
    {
        playList->setCurrentIndex(index);
        musicListInfo.currMusicListIndex = index;
    }
    //检查播放模式是否一致
    if(mode != musicListInfo.currMode)
    {
        playList->setPlaybackMode(mode);
        musicListInfo.currMode = mode;
    }
    emit sig_musicRep(MusicListSetSucc);
    //根据情况播放
    if(playNow)
    {
        player->play();
    }
}

//处理播放音乐
void SoundThread::deal_musicPlay(bool isPlay)
{
    //根据值播放或暂停
    if(isPlay)
    {
        if(getPlayerState() == false)
        {
            player->play();
            setPlayerState(true);
        }
    }
    else
    {
        if(getPlayerState() == true)
        {
            player->stop();
            setPlayerState(false);
        }
    }
}

//设置播放器状态
void SoundThread::setPlayerState(bool isPlay)
{
    this->isPlay = isPlay;
}

//获取播放器状态
bool SoundThread::getPlayerState()
{
    return this->isPlay;
}
