#ifndef FILESAVETHREAD_H
#define FILESAVETHREAD_H

#include <QObject>

#include "SoapPublicFunc.h"

class FileSaveThread : public QObject
{
    Q_OBJECT
public:

    //构造
    explicit FileSaveThread(QObject *parent = nullptr);

    //保存文件
    void deal_FileSave(QString absFileName,QByteArray content);

signals:
    void sig_fileSaveState(bool isSaveDone);

private:

};

#endif // FILESAVETHREAD_H
