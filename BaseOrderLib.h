/*
    输出节点无服务器控制协议SCODe--2020.md
    详细信息请访问以下链接
    https://slangelec-rd.yuque.com/slangelec.rd/wmoeoy/kmuzdy#028cf19a

    通信说明
    无服务器版本协议：UDP命令，命令发送目标端口41234，使用组播时，默认为224.168.1.1
    预览回显使用从输入组播抓图，组播地址由设计器配置，输入节点由UMP配置，默认使用224.168.1.2，端口按照45600 + ip & 0xFF（通道0主流计算方案）作为端口。

    程序已用地址和端口公示:
    输出节点控制端口: 41234 (特殊:该端口上同时监听组播地址: 224.168.1.1,用于接收输出节点媒体控制SCODe协议控制协议)
    输出节点字幕OSD控件监听端口:41239
    输出节点回显抓图端口: 41235
    输出节点单播udp扫描端口: 11
    输出节点广播(255.255.255.255)扫描端口: 10
    输出节点组播(239.255.255.250)扫描端口: 3700

    命令汇总
    GetNodes：广播/组播/单播.获取输出节点
    SetLockFrame：单播.用于设置锁屏基数，实际锁屏时间为 锁屏基数+同步系数
    GetTransmit： 单播.用于获取指定信号源的输出组播转发
    SetTransmit：单播.用于设定指定信号源的输出组播转发
    GetWindows：单播.获取指定视频墙实时窗口信息（信号源IP，位置大小）
    SetWindows：广播/组播/单播.设定指定视频墙所有窗口（信号源IP，流URL，位置大小）
*/

#ifndef BASEORDERLIB_H
#define BASEORDERLIB_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QList>
#include <QJsonArray>
#include <QJsonObject>

#include "SoapPublicFunc.h"

class BaseOrderLib
{
public:
    //SetTransmit命令参数结构
    struct SetTransmitParam
    {
        QString src_url;            //源地址URL
        QString transmit_url;       //转发组播URL
    };

    //SetWindows命令参数结构
    struct SetWindowsParam
    {
        QString src_id;             //窗口的信号源id
        QString url;                //流的URL地址
        quint32 x;                  //真实像素的位置大小(x坐标)
        quint32 y;                  //(y坐标)
        quint32 w;                  //窗口宽度(width)
        quint32 h;                  //窗口高度(height)
        QString group_id;           //信号源组号(可忽略,默认为-1,即不属于任何组)
    };

    //转发节点抓图配置命令参数
    struct SnapParam
    {
        int snap_w = 320;
        int snap_h = 180;
        int snap_len = 1200;
        int snap_delay = 1000;
        int snap_quality = 50;
        bool mcast_enable = true;
        int mcast_freq = 20;
        QString snap_type = "bypass";
    };

    //码流参数结构体
    struct StreamParam
    {
        QString type = "h264";
        QString protocol = "udp";
        QString ip = "224.168.111.200";
        int port = 45678;
        int length = 1300;
    };

    //输出节点广播扫描端口号
    const quint16 broadcastScanPort = 10;
    //输出节点广播IP
    const QString opNodeBroadcastIp = "255.255.255.255";

    //输出节点组播扫描端口号
    const quint16 multicastScanPort = 3700;
    //输出节点组播IP
    const QString opNodeMulticastIp = "239.255.255.250";

    //输出节点单播扫描端口号
    const quint16 unicastScanPort = 11;

    //输出节点控制端口
    const quint16 opNodeCtrlPort = 41234;
    //无服务器版本协议UDP使用组播是的默认端口
    const QString udpBroadCastDefaultPort = "224.168.1.1";

    //输出节点字幕OSD控件监听端口
    const quint16 opNodeSubtitlesCtrlListenPort = 41239;

    //定义每月的天数
    const int monthDays[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
    //定义每月大约的天数
    const int monthDaysAver = 30;
    //定义每年大约的天数
    const int yearDays = 365;
    //定义每年的月数
    const int yearMonths = 12;

public:
    BaseOrderLib();

    //基本命令库

    //GetNodes 用于扫描所有节点,支持广播/组播/单播
    //客户端->设备
    static QString getNodes_CToD(QString ip = "",quint32 port = 0);
    //设备->客户端
    //static QString getNodes_DtoC();

    //SetLockFrame 用于设置锁屏基数,实际锁屏时间为 锁屏基数+同步系数
    //客户端->设备
    static QString setLockFrame_CToD(int panel_sn,int baseLockFrame);
    //设备->客户端 (无)

    //GetTransmit 用于或缺指定信号源的输出组播转发,支持单播
    //客户端->设备
    static QString getTransmit_CToD(int panel_sn,QString ip = "",quint32 port = 0);
    //设备->客户端
    //static QString getTransmit_DToC();

    //SetTransmit 用于指定信号源的输出组播转发,支持单播
    //客户端->设备
    static QString setTransmit_CToD(int panel_sn,QList<SetTransmitParam*> SetTransmitParamList);
    //设备->客户端 (无)

    //GetWindows 用于从某个输出节点获取当前节点属于的视频墙的实时窗口信息，单播
    //客户端->设备
    static QString getWindows_CToD(int panel_sn,QString ip = "",quint32 port = 0);
    //设备->客户端
    //static QString getWindows_DToC();

    //SetWindows 用于设定指定视频墙的实时窗口信息,支持广播/组播/单播
    //客户端->设备
    static QString setWidnows_CToD(int panel_sn,QList<SetWindowsParam*> SetWindowsParamList);
    //设备->客户端 (无)

    //为QString 变量添加大括号(示例   GetNodes;128.0.0.1,3220 -> {GetNodes;128.0.0.1,3220})
    static QString addBraceForString(QString str);

    //转发节点配置命令 默认值
    static QJsonObject defJson_SetRelayList();

    //回显控制Value数组对象
    static QJsonObject setRelayList_valueObj();

    //回显控制SNAP 默认值
    static QJsonObject defJson_snap();

    //setWindows回复到客户端的 默认值
    static QJsonObject defJson_setWindowsToC();

    //清空命令
    static QJsonObject closeRelayList();

private:

};

#endif // BASEORDERLIB_H
