#include "FileSaveThread.h"

//构造
FileSaveThread::FileSaveThread(QObject *parent) : QObject(parent)
{

}

//保存文件(不覆盖式的继续写入)
void FileSaveThread::deal_FileSave(QString absFileName,QByteArray content)
{
    //若指定的文件不存在,则会创建一个
    QFile file(absFileName);
    file.open(QFile::Append);
    file.write(content);
    file.close();
}

