#include "DownloadFileThread.h"

DownloadFileThread::DownloadFileThread(QObject *parent,int panel_sn,QString ip,quint16 port,QString resPath) : QObject(parent)
{
    /******************************初始化******************************/
    //视频墙号
    this->panel_sn = panel_sn;
    //传输文件的默认开始状态
    this->state = DownloadFileInit;
    //文件结构体变量初始化
    fileData.fileName = "";
    fileData.file.setFileName(fileData.fileName);
    fileData.fileLen = 0;
    fileData.fileSendedLen = 0;
    fileData.fileEveryPackLen = 0;
    fileData.filePack_sn.clear();
    fileData.fileSendInterval = 0;
    fileData.fileSumPack = 0;
    fileData.fileCurrPackNum = 0;
    //套接字
    downloadSocket = new QTcpSocket(this);
    isConnected = false;
    //保存对方的IP和端口
    clientIp = QHostAddress(ip);
    clientPort = port;
    //保存根路径
    resourceFilePath = resPath;
    //设置是否终止标记
    isAbort = false;
}

//处理开始传输文件信号
void DownloadFileThread::dealDownloadFileStart(int panel_sn,QString fileName,QString ip,quint16 port,quint32 len,quint32 interval,QList<quint32> pack_sn)
{
    //若视频墙号不一致,退出
    if(this->panel_sn != panel_sn)
    {
        return;
    }
    /******************************建立连接阶段******************************/
    //检查IP和端口是否一致
    if(clientIp.toString() != ip || clientPort != port)
    {
        return;
    }
    //尝试主动连接到指定的IP和端口
    if(isConnected == false)
    {
        downloadSocket->connectToHost(clientIp,clientPort);
        //最多在建立连接阶段等待10s
        isConnected = downloadSocket->waitForConnected(10000);
    }
    if(isConnected == false)
    {
        //发送建立连接失败信号
        emit sig_downloadSituation(this->panel_sn,state = DownloadFileConnectFail);
        return;
    }
    /******************************保存参数阶段******************************/
    //从根路径 + 文件名称获得文件的绝对路径
    QString obslouteFilePath = this->resourceFilePath + "/" + fileName;
    if(SoapPublicFunc::isFileNameExist(obslouteFilePath) == false)
    {
        emit sig_downloadSituation(this->panel_sn,state = DownloadFileNotFound);
        return;
    }
    fileData.fileName = obslouteFilePath;
    fileData.file.setFileName(fileData.fileName);
    fileData.fileLen = fileData.file.size();
    fileData.fileEveryPackLen = len;
    fileData.fileSendInterval = interval;
    fileData.filePack_sn = pack_sn;
    fileData.fileSendedLen = 0;
    /******************************传输文件阶段******************************/
    //以只读方式打开文件
    if(fileData.file.open(QFile::ReadOnly) == false)
    {
        emit sig_downloadSituation(this->panel_sn,state = DownloadFileOpenFail);
        return;
    }
    //计算出总包数(若文件总长能将每包中文件主体数据长度整除,则总包数等于文件总长除以每包文件数据长度,否则为前者+1)
    (fileData.fileLen % (fileData.fileEveryPackLen - 8) !=0) ? fileData.fileSumPack = fileData.fileLen/(fileData.fileEveryPackLen-8)+1
                                                             : fileData.fileSumPack = fileData.fileLen/(fileData.fileEveryPackLen-8);
    //根据每包长度动态分配字符数组的长度 并 从文件数据中读取长度
    char *data = new char[fileData.fileEveryPackLen - 8];
    quint32 lenFromFile = fileData.file.read(data,fileData.fileEveryPackLen - 8);
    //包号从1开始
    fileData.fileCurrPackNum = 1;
    //判断是发送所有数据包还是指定的数据包
    if(fileData.filePack_sn.isEmpty() == true)
    {
        //发送全部数据包
        while(lenFromFile>0 && isAbort == false)
        {
            //数据包格式(总包数(4位) + 当前包号(4位) + 文件主体数据 (fileData.fileEveryPackLen - 8位))
            QByteArray dataArray;
            //总包数占4位
            dataArray.append(toHex(fileData.fileSumPack,4));
            //当前包号占4位
            dataArray.append(toHex(fileData.fileCurrPackNum,4));
            //文件主体数据 每包长度 - 8位
            dataArray.append(data,lenFromFile);
            //发送数据
            downloadSocket->write(dataArray,dataArray.length());
            //发送数据后延时
            QThread::usleep(interval);
            //累加发送的文件数据(仅从文件读取的数据,不包含总包数和包号)
            fileData.fileSendedLen += lenFromFile;
            //累加当前包号
            fileData.fileCurrPackNum++;
            //从文件中读取数据
            lenFromFile = fileData.file.read(data,fileData.fileEveryPackLen - 8);
        }
        //关闭文件对象并释放内存
        fileData.file.close();
        delete []data;
        data = nullptr;
        //检查发送全部数据是否完成
        if(fileData.fileSendedLen == fileData.fileLen && fileData.fileSendedLen > 0 )
        {
            SoapPublicFunc::soapSleep(waitAfterSendDone(fileData.fileLen)*1000);
            emit sig_downloadSituation(this->panel_sn,state = DownloadFileRecvDone);
        }
        else
        {
            emit sig_downloadSituation(this->panel_sn,state = DownloadFileRecvFail);
        }
    }
    else
    {
        //发送指定包号的数据包(用计数器纪录)
        while(lenFromFile>0 && isAbort == false)
        {
            //数据包格式(总包数(4位) + 当前包号(4位) + 文件主体数据 (fileData.fileEveryPackLen - 8位))
            QByteArray dataArray;
            //总包数占4位
            dataArray.append(toHex(fileData.fileSumPack,4));
            //当前包号占4位
            dataArray.append(toHex(fileData.fileCurrPackNum,4));
            //文件主体数据 每包长度 - 8位
            dataArray.append(data,lenFromFile);
            //若当前的包号等于包号列表中的包号,则发送
            for(int i=0;i<fileData.filePack_sn.count();i++)
            {
                if(fileData.filePack_sn.at(i) == fileData.fileCurrPackNum)
                {
                    downloadSocket->write(dataArray,dataArray.length());
                    //发送数据后延时
                    QThread::usleep(interval);
                    //包号列表中包号不会重复,匹配后删除以提高效率
                    fileData.filePack_sn.removeAt(i);
                    break;
                }
            }
            //累加当前包号
            fileData.fileCurrPackNum++;
            //从文件中读取数据
            lenFromFile = fileData.file.read(data,fileData.fileEveryPackLen - 8);
        }
        //关闭文件对象并释放内存
        fileData.file.close();
        delete []data;
        data = nullptr;
        //判断是否发送完成所有指定的包号(即包号列表现在是否为空)
        if(fileData.filePack_sn.count() == 0)
        {
            //发送成功,等待一段时间后回传参数
            SoapPublicFunc::soapSleep(waitAfterSendDone(fileData.fileLen)*1000);
            emit sig_downloadSituation(this->panel_sn,state = DownloadFileSendDone);
        }
        else
        {
            emit sig_downloadSituation(this->panel_sn,state = DownloadFileSendFail);
        }
    }
}

//处理传输文件暂停(强行终止)信号
void DownloadFileThread::dealDownloadFileAbort(int panel_sn)
{
    //若视频墙号不一致,退出
    if(this->panel_sn != panel_sn)
    {
        return;
    }
    //设置终止标记
    isAbort = true;
    //发送终止信号
    emit sig_downloadSituation(this->panel_sn,state = DownloadFileAbort);
}

//10进制转为16进制QByteArray类型
QByteArray DownloadFileThread::toHex(int num,unsigned int place)
{
    QByteArray data;
    while(place--)
    {
        char c_num = num & 0xff;
        data.append(c_num);
        num = num >> 8;
    }
    return data;
}

//将一个QByteArray类型转为uint64
quint64 DownloadFileThread::toUint64(QByteArray data)
{
    quint64 sum = 0;
    for(int i=0; i < data.size(); i++)
    {
        unsigned char tmp_num = (unsigned char)data[i];
        sum += (quint64)(tmp_num<<(i*8));
    }
    return sum;
}

//根据发送的文件大小生成大约要延时的时间(单位:秒)
quint64 DownloadFileThread::waitAfterSendDone(quint64 fileSize)
{
    //根据经验换算(4G文件在局域网传输在服务端发送完成后大约有不超过40s的延迟客户端能全部收完),1G文件等待10s
    double delay = (double)fileSize/1073741824*10;
    return floor(delay);
}

