#ifndef FILELISTDISPLAY_H
#define FILELISTDISPLAY_H

#include <QWidget>
#include <QList>
#include <QMenu>
#include <QTableWidgetItem>
#include <QModelIndex>
#include <QShortcut>
#include <QTableWidget>
#include "SoapPublicFunc.h"

namespace Ui {
class FileListDisplay;
}

class FileListDisplay : public QDialog
{
    Q_OBJECT

public:
    //信息类型
    enum InfoType
    {
        IdType = 0,                                                 //信号源类型
        NodeType,                                                   //转发类型
        WindowType,                                                 //视频墙类型
        ScenesPollingType,                                          //场景轮询类型
        OtherType                                                   //未知类型
    };

    //信号源表参数结构体
    struct idListInfo
    {
        QString idListIp;                                           //信号源IP
        QString idListName;                                         //名称
        QString idListMajorStream;                                  //信号源主流URL
        QString idListMinorStream;                                  //信号源辅流URL
    };

    QFont bodyFont = QFont("微软雅黑",11);
    QFont menuFont = QFont("微软雅黑",13);

    //构造
    FileListDisplay(QWidget *parent = nullptr);

    //析构
    ~FileListDisplay();

    //清空列表控件
    void clearTabWidget();

    //为tabWidget控件添加水平表头内容
    void setHorizonHeaderContent(QStringList headerContent);

    //为tabWidget控件添加垂直表头内容
    void setVerticalHeaderContent(QStringList headerContent);

    //为TabWidget控件添加内容
    void loadContentIntoList(QList<QStringList> content,QStringList headerContent,InfoType Type,bool btnEnable = true);

    //设置当前列表的格式
    void setListType(InfoType type);

    //取出当前列表格式
    InfoType getListType();

    //修改选定(单选)的列表对象
    void alterCell();

    //删除选定的行(无论是一行还是多行)
    void removeSelectedRow();

signals:
    void sig_infoListSendBack(QList<QStringList> content,InfoType type);

private slots:

    //保存按钮按下
    void on_saveBtn_clicked();

    //取消按钮按下
    void on_cancelBtn_clicked();

    //新增按钮按下
    void on_addBtn_clicked();

    //tableWidget右键菜单
    void on_tableWidget_list_customContextMenuRequested(const QPoint &pos);

    //tableWidget单元格改变槽
    void on_tableWidget_list_itemChanged(QTableWidgetItem *item);

private:
    Ui::FileListDisplay *ui;

    InfoType fileType;                                          //记录当前的文件类型

};

#endif // FILELISTDISPLAY_H
