#include "SoapTabBar.h"

SoapTabBar::SoapTabBar()
{

}

QSize SoapTabBar::tabSizeHint(int index) const
{
    QSize s = QTabBar::tabSizeHint(index);
    s.transpose();
    return s;
}

void SoapTabBar::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    QStyleOptionTab option;
    for(int i=0;i<this->count();i++)
    {
        initStyleOption(&option,i);
        painter.drawControl(QStyle::CE_TabBarTabShape,option);
        painter.save();

        QSize s = option.rect.size();
        s.transpose();
        QRect r(QPoint(),s);
        r.moveCenter(option.rect.center());
        option.rect = r;

        QPoint c = tabRect(i).center();
        painter.translate(c);
        painter.rotate(90);
        painter.translate(-c);
        painter.drawControl(QStyle::CE_TabBarTabShape,option);
        painter.restore();
    }
}


