#include "SoapTransmit.h"
#include "ui_SoapTransmit.h"

//构造
SoapTransmit::SoapTransmit(QWidget *parent): QMainWindow(parent),ui(new Ui::SoapTransmit)
{
    ui->setupUi(this);

    //注册自定义的信号类型
    qRegisterMetaType<FileReadThread::fileState>("fileState");

    //主窗口设置
    setMinimumSize(650,400);
    resize(800,450);
    setWindowTitle("节点转发工具");
    setWindowIcon(QIcon(":/Transmit/transmit_Blue.png"));
    setShortcutEnabled(true);

    //关闭窗口快捷键
    QShortcut *closeApp = new QShortcut(QKeySequence(Qt::Key_Escape),this,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
    connect(closeApp,&QShortcut::activated,this,&SoapTransmit::closeMainWindow);
    //隐藏窗口快捷键
    QShortcut *hideApp = new QShortcut(QKeySequence(Qt::Key_H),this,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
    connect(hideApp,&QShortcut::activated,this,[=]()
    {
        if(this->isHidden() == false || this->isMinimized() == false)
        {
            this->showMinimized();
            transMsgBoxRemind("程序最小化",2000);
        }
    });

    //系统托盘图标,udp接收 发送套接字 成员初始化
    trayIcon = new QSystemTrayIcon(this);
    udpRecvSock = new QUdpSocket(this);
    udpSendSock = new QUdpSocket(this);
    sendMsgTimer = new QTimer(this);
    sendMsgTimer->setSingleShot(true);

    //绑定接收端口
    udpRecvSock->bind(41234);

    //发送套接字绑定地址并加入组播组
    //udpSendSock->bind(QHostAddress("192.168.168.169"),41237);
    //udpSendSock->joinMulticastGroup(QHostAddress("224.168.1.1"));

    /******************************信号和槽******************************/
    //UDP接收套接字 接收数据槽
    connect(udpRecvSock,&QUdpSocket::readyRead,this,&SoapTransmit::udpSockRecvData);

    //UDP发送套接字 接收数据槽
    connect(udpSendSock,&QUdpSocket::readyRead,this,&SoapTransmit::udpSockRecvData_toSendSock);

    //发送数据定时器槽
    connect(sendMsgTimer,&QTimer::timeout,this,&SoapTransmit::deal_sendMsgTimeOut);

    //命令列表设置右键菜单
    ui->listWidget_orderList->setContextMenuPolicy(Qt::CustomContextMenu);

    //初始化时创建必要的文件夹
    initCreateFoler();
    //初始化默认筛选scodeList文件夹
    sort_scodeList();

    //初始化运行信息分页
    initRunInfoTab();

    //应用程序系统托盘图标设置
    trayIcon->setIcon(QIcon(":/Transmit/transmit_Blue.png"));
    trayIcon->setToolTip("节点转发应用程序");
    //设置系统托盘图标右键菜单
    trayMenu = new QMenu(this);
    //显示主窗口动作
    trayMenu->addAction(QIcon(":/Transmit/examine_Blue.png"),"显示主窗",this,&SoapTransmit::showMainWindow)->setFont(bodyFont);
    //退出动作
    trayMenu->addAction(QIcon(":/Transmit/leave_Black.png"),"退出",this,&SoapTransmit::closeMainWindow)->setFont(bodyFont);
    //托盘图标设置菜单
    trayIcon->setContextMenu(trayMenu);
    //绑定系统托盘图标的双击(点击)信号槽
    connect(trayIcon,&QSystemTrayIcon::activated,this,&SoapTransmit::trayIconActivatedSlot);
    //托盘图标显示
    trayIcon->show();

    //取出双表内容并添加进内存(信号源表 转发表)
    setIsIdAndNodeListCorrect(reloadIdAndNodeListData());
    (getIsIdAndNodeListCorrect()) ? ui->statusbar->showMessage("本地双表内容读取成功",statusBarShowTime)
                                  : ui->statusbar->showMessage("本地双表内容读取失败,请检查内容和格式",statusBarShowTime_error);

    //读取节点转发程序参数文件
    QString selfSettingFileName = QDir::currentPath() + "/" + scodeDataFolderName + "/" + transmitDataFileName;
    QFile selfSettingFile(selfSettingFileName);
    if(selfSettingFile.open(QFile::ReadOnly) == false)
    {
        ui->statusbar->showMessage("节点转发程序设置参数读取失败!",statusBarShowTime_error);
    }
    else
    {
        QByteArray settingData = selfSettingFile.readAll();
        //软件参数(总)
        QJsonObject settingObj = SoapPublicFunc::QStringToJsonObj(QString(settingData));
        //读取接收日志参数
        QJsonObject recvLogSettingObj = settingObj.find("RecvLogSetting")->toObject();
        myRecvLogInfo.recvLogSave = recvLogSettingObj["LogSaveSwitch"].toBool();
        myRecvLogInfo.recvLogUseLife = recvLogSettingObj["LogUseLife"].toInt();
        myRecvLogInfo.recvLogUseLifeUnit = recvLogSettingObj["LogUseLifeUnit"].toInt();
        //读取发送日志参数
        QJsonObject sendLogSettingObj = settingObj.find("SendLogSetting")->toObject();
        mySendLogInfo.sendLogSave = sendLogSettingObj["LogSaveSwitch"].toBool();
        mySendLogInfo.sendLogUseLife = sendLogSettingObj["LogUseLife"].toInt();
        mySendLogInfo.sendLogUseLifeUnit = sendLogSettingObj["LogUseLifeUnit"].toInt();
        //读取抓图参数
        QJsonObject snapParamsObj = settingObj.find("SnapParams")->toObject();
        snapParam.snap_w = snapParamsObj["snap_w"].toInt();
        snapParam.snap_h = snapParamsObj["snap_h"].toInt();
        snapParam.snap_len = snapParamsObj["snap_len"].toInt();
        snapParam.snap_delay = snapParamsObj["snap_delay"].toInt();
        snapParam.snap_quality = snapParamsObj["snap_quality"].toInt();
        snapParam.mcast_enable = snapParamsObj["mcast_enable"].toBool();
        snapParam.mcast_freq = snapParamsObj["mcast_freq"].toInt();
        snapParam.snap_type = snapParamsObj["snap_type"].toString();
    }
    selfSettingFile.close();

    //读取文件内容线程,以及相关的信号和槽
    fileReadSubThread = new QThread(this);
    fileReadThread = new FileReadThread;
    fileReadThread->moveToThread(fileReadSubThread);
    connect(this,&SoapTransmit::sig_startFileRead,fileReadThread,&FileReadThread::deal_StartReadFile);
    connect(this,&SoapTransmit::sig_stopFileRead,fileReadThread,&FileReadThread::deal_stopReadFile);
    connect(fileReadThread,&FileReadThread::sig_fileContent,this,&SoapTransmit::deal_fileReadContent);
    connect(fileReadThread,&FileReadThread::sig_sitState,this,&SoapTransmit::deal_fileReadState);

    //保存文件内容线程,以及相关的信号和槽
    fileSaveSubThread = new QThread(this);
    fileSaveThread = new FileSaveThread;
    fileSaveThread->moveToThread(fileSaveSubThread);

    //展开QTreeWidget内容
    ui->treeWidget_opeSet->expandAll();

    //设置所处分页
    ui->tabWidget->setCurrentIndex(0);

    /******************************加载样式表文件******************************/
    QFile styleSheetFile(":/qss/qss/blue.css");
    styleSheetFile.open(QFile::ReadOnly);
    setStyleSheet(styleSheetFile.readAll());
    styleSheetFile.close();

}

//显示主窗(系统托盘图标菜单项)
void SoapTransmit::showMainWindow()
{
    this->showNormal();
}

//关闭(系统托盘图标菜单项)
void SoapTransmit::closeMainWindow()
{
    if(isHidden() || isMinimized() )
    {
        this->showNormal();
    }
    this->close();
}

//析构
SoapTransmit::~SoapTransmit()
{
    //保存接收的日志
    if(myRecvLogInfo.recvLog.isEmpty() == false && myRecvLogInfo.recvLogSave)
    {
        QFile recvLogFile(QDir::currentPath() + "/" + scodeLogFolderName + "/" + "RecvOrderLog_" + currDate() + ".txt");
        recvLogFile.open(QFile::Append);
        recvLogFile.write(myRecvLogInfo.recvLog);
        recvLogFile.close();
        myRecvLogInfo.recvLog.clear();
    }
    //保存发送的日志
    if(mySendLogInfo.sendLog.isEmpty() == false && mySendLogInfo.sendLogSave)
    {
        QFile sendLogFile(QDir::currentPath() + "/" + scodeLogFolderName + "/" + "SendOrderLog_" + currDate() + ".txt");
        sendLogFile.open(QFile::Append);
        sendLogFile.write(mySendLogInfo.sendLog);
        sendLogFile.close();
        mySendLogInfo.sendLog.clear();
    }

    //根据日志参数删除在有效期外的日志文件
    recvLogFileAdjust();

    //析构时保存软件参数
    saveTransmitParams();

    //清空链表(信号源,转发节点,历史视频墙信息链表)
    idListInfoList.clear();
    nodeListInfoList.clear();
    windowsInfoList.clear();
    setSceneStateInfoList.clear();

    //终止文件读取线程
    if(fileReadSubThread->isRunning() == true)
    {
        emit sig_stopFileRead();
        fileReadSubThread->quit();
        fileReadSubThread->wait();
    }
    delete fileReadThread;
    fileReadThread = nullptr;

    //终止文件保存线程
    if(fileSaveSubThread->isRunning() == true)
    {
        fileReadSubThread->quit();
        fileReadSubThread->wait();
    }
    delete fileSaveThread;
    fileSaveThread = nullptr;

    //终止所有轮询子线程对象,无论其是否处于正在轮询的状态
    if(scenesPollingInfoList.count() != 0)
    {
        for(int i=scenesPollingInfoList.count()-1;i!=-1;i--)
        {
            scenesPollingInfoList[i]->closeAfterPause = true;
            //发送暂停信号
            emit sig_scenesPollingPause(scenesPollingInfoList.at(i)->scenesPollingPanel);
        }
    }

    //终止所有文件传输子线程对象,无论其是否传输文件完毕
    if(downloadFileInfoList.count() != 0)
    {
        for(int i=downloadFileInfoList.count()-1;i!=-1;i--)
        {
            //发送终止信号
            emit sig_downloadFileAbort(downloadFileInfoList.at(i)->downloadFilePanel);
        }
        //文件传输线程退出
        ui->statusbar->showMessage("等待文件传输线程退出",statusBarShowTime);
        while(downloadFileInfoList.count() != 0)
        {
            SoapPublicFunc::soapSleep(300);
        }
    }

    //释放UI对象
    delete ui;
}

//系统托盘图标槽
void SoapTransmit::trayIconActivatedSlot(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
    //双击图标(隐藏与显示窗口 相互切换)
    case QSystemTrayIcon::ActivationReason::DoubleClick: (isHidden() || isMinimized()) ? showNormal(): hide();break;
    default:return;
    }
}

//初始化创建文件夹
void SoapTransmit::initCreateFoler()
{
    //应用程序主要路径
    QDir scodeDir;
    /******************************创建保存设置用文件夹******************************/
    QString saveDataFolderPath = QDir::currentPath() + "/" + scodeDataFolderName;
    if(SoapPublicFunc::isFilePathExist(saveDataFolderPath) == false)
    {
        scodeDir.mkpath(saveDataFolderPath);
    }
    //创建软件设置的各类参数保存文件
    QString transDataFileName = saveDataFolderPath + "/" + transmitDataFileName;
    if(SoapPublicFunc::isFileNameExist(transDataFileName) == false)
    {
        QFile file(transDataFileName);
        file.open(QFile::ReadWrite);
        file.write(SoapPublicFunc::jsonObjToQstring(defSelfSetting()).toUtf8());
        file.close();
    }
    /******************************创建保存列表用文件夹******************************/
    QString saveListFolderPath = QDir::currentPath() + "/" + scodeListFolderName;
    if(SoapPublicFunc::isFilePathExist(saveListFolderPath) == false)
    {
        scodeDir.mkpath(saveListFolderPath);
    }
    //创建转发表
    QString nodeListName = saveListFolderPath + "/" + nodeListFileName;
    if(SoapPublicFunc::isFileNameExist(nodeListName) == false)
    {
        QFile file(nodeListName);
        file.open(QFile::ReadWrite);
        file.close();
    }
    //创建信号源表
    QString idListName = saveListFolderPath + "/" + idListFileName;
    if(SoapPublicFunc::isFileNameExist(idListName) == false)
    {
        QFile file(idListName);
        file.open(QFile::ReadWrite);
        file.close();
    }
    /******************************创建保存资源用文件夹******************************/
    QString saveResFolderPath = QDir::currentPath() + "/" + scodeResourceFolderName;
    if(SoapPublicFunc::isFilePathExist(saveResFolderPath) == false)
    {
        scodeDir.mkpath(saveResFolderPath);
    }
    /******************************创建保存日志用文件夹******************************/
    QString saveLogFolderPath = QDir::currentPath() + "/" + scodeLogFolderName;
    if(SoapPublicFunc::isFilePathExist(saveLogFolderPath) == false)
    {
        scodeDir.mkpath(saveLogFolderPath);
    }
}

//初始化运行信息分页
void SoapTransmit::initRunInfoTab()
{
    //运行信息添加到listWidget控件中
    for(int i=0;i<runInfoNameList.count();i++)
    {
        QListWidgetItem *item = new QListWidgetItem(QIcon(":/Transmit/ongoing_blue.png"),runInfoNameList.at(i));
        item->setSizeHint(QSize(ui->listWidget_runInfo->width()-5,40));
        item->setToolTip(item->text());
        ui->listWidget_runInfo->addItem(item);
    }
}

//将数据写入(覆盖)指定的文件(需要文件的绝对路径)
bool SoapTransmit::setAbsoluteFileData(QString absoluteFilePath,QByteArray fileData)
{
    bool isDone;
    //若指定的文件不存在,则会创建一个
    QFile file(absoluteFilePath);
    file.open(QFile::ReadWrite);
    file.resize(0);
    qint64 len = file.write(fileData);
    file.close();
    //若给定的数据为空,则文件同样会清空(但在该情况下会返回false)
    (len>0) ? isDone = true : isDone = false;
    return isDone;
}

//取出一个绝对路径下的文件数据
QByteArray SoapTransmit::getAbsoluteFileData(QString absoluteFilePath)
{
    //读取文件数据并返回
    QFile file(absoluteFilePath);
    QByteArray fileData;
    (file.open(QFile::ReadOnly) == true) ? fileData = file.readAll() : fileData = "";
    file.close();
    return fileData;
}

//文件列表右键菜单 打开文件所在位置
void SoapTransmit::openOrderFileDir()
{
    //根据当前的筛选方式确定父路径
    QString parentPath;
    switch(getFileSortWay())
    {
    case SoapTransmit::FileSortWay::ScodeDataFolder:parentPath = QDir::currentPath() + "/" + scodeDataFolderName;break;
    case SoapTransmit::FileSortWay::ScodeListFolder:parentPath = QDir::currentPath() + "/" + scodeListFolderName;break;
    case SoapTransmit::FileSortWay::ScodeLogFolder:parentPath = QDir::currentPath() + "/" + scodeLogFolderName;break;
    case SoapTransmit::FileSortWay::ScodeResFolder:parentPath = QDir::currentPath() + "/" + scodeResourceFolderName;break;
    default:return;
    }
    //若存在则打开,反之跳出
    if(SoapPublicFunc::isFilePathExist(parentPath))
    {
        QDesktopServices::openUrl(QUrl(parentPath));
    }
    else
    {
        ui->statusbar->showMessage("错误,未找到选定文件所在位置",statusBarShowTime_error);
    }
}

//文件列表右键菜单 查看内容
void SoapTransmit::examineOrderFile()
{
    //触发单击事件(右键菜单 查看内容和点击事件一致)
    on_listWidget_orderList_itemClicked(ui->listWidget_orderList->currentItem());
}

//文件列表右键菜单 删除文件
void SoapTransmit::deleteFile()
{
    //检查选定的对象的名称是否是关键文件,若是则不允许删除
    QString name = ui->listWidget_orderList->currentItem()->text();
    for(int i=0;i<transmitKeyFileList.count();i++)
    {
        if(transmitKeyFileList.at(i) == name)
        {
            transMsgBoxRemind(QString("警告!%1 为关键文件\n不允许删除!").arg(name),5000);
            return;
        }
    }
    QString parentPath;
    switch (getFileSortWay())
    {
    case SoapTransmit::FileSortWay::ScodeDataFolder:parentPath = QDir::currentPath() + "/" + scodeDataFolderName;break;
    case SoapTransmit::FileSortWay::ScodeListFolder:parentPath = QDir::currentPath() + "/" + scodeListFolderName;break;
    case SoapTransmit::FileSortWay::ScodeLogFolder:parentPath = QDir::currentPath() + "/" + scodeLogFolderName;break;
    case SoapTransmit::FileSortWay::ScodeResFolder:parentPath = QDir::currentPath() + "/" + scodeResourceFolderName;break;
    default:transMsgBoxRemind("未知的父目录,删除文件无效");return;
    }
    QString fileName = parentPath + "/" + ui->listWidget_orderList->currentItem()->text();
    QMessageBox *switchBox = new QMessageBox(QMessageBox::Warning,"注意",QString("你确定要删除文件\n%1 吗?").arg(name),QMessageBox::Yes | QMessageBox::No);
    switchBox->setButtonText(QMessageBox::Yes,"确定");
    switchBox->setButtonText(QMessageBox::No,"取消");
    switchBox->setDefaultButton(QMessageBox::Yes);
    switchBox->setFont(bodyFont);
    switchBox->setAttribute(Qt::WA_DeleteOnClose);
    int result = switchBox->exec();
    if(result == QMessageBox::Yes)
    {
        //删除文件
        QFile file(fileName);
        bool isDone = file.remove();
        if(isDone)
        {
            //删除列表中被选定的对象
            QListWidgetItem *item = ui->listWidget_orderList->currentItem();
            ui->listWidget_orderList->removeItemWidget(item);
            delete item;
            item = nullptr;
            //清空预览区
            ui->textEdit_orderView->clear();
            //提示
            ui->statusbar->showMessage(name + " 已删除",statusBarShowTime);
        }
        else
        {
            ui->statusbar->showMessage(name + " 删除失败",statusBarShowTime_error);
        }
    }
}

//将命令头转换为枚举值(参数为命令的关键字(命令头) 如:GetNodes)
SoapTransmit::FileOrderType SoapTransmit::getOrderType(QString orderKey)
{
    if(orderKey == "GetNodes")
    {
        return FileOrderType::GetNodes;
    }
    else if(orderKey == "SetLockFrame")
    {
        return FileOrderType::SetLockFrame;
    }
    else if(orderKey == "GetTransmit")
    {
        return FileOrderType::GetTransmit;
    }
    else if(orderKey == "SetTransmit")
    {
        return FileOrderType::SetTransmit;
    }
    else if(orderKey == "GetWindows")
    {
        return FileOrderType::GetWindows;
    }
    else if(orderKey == "SetWindows")
    {
        return FileOrderType::SetWindows;
    }
    else if(orderKey == "GetAudios")
    {
        return  FileOrderType::GetAudios;
    }
    else if(orderKey == "SetAudios")
    {
        return FileOrderType::SetAudios;
    }
    else if(orderKey == "GetSignalResourcesTree")
    {
        return FileOrderType::Json_GetSignalResourcesTree;
    }
    else if(orderKey == "GetSceneState")
    {
        return FileOrderType::GetSceneState;
    }
    else if(orderKey == "SetSceneState")
    {
        return FileOrderType::SetSceneState;
    }
    else if(orderKey == "GetScene")
    {
        return FileOrderType::GetScene;
    }
    else if(orderKey == "GetScenes")
    {
        return FileOrderType::GetScenes;
    }
    else if(orderKey == "GetSceneNames")
    {
        return FileOrderType::GetSceneNames;
    }
    else if(orderKey == "SaveScene")
    {
        return FileOrderType::SaveScene;
    }
    else if(orderKey == "DeleteScene")
    {
        return FileOrderType::DeleteScene;
    }
    else if(orderKey == "ClearScenes")
    {
        return FileOrderType::ClearScenes;
    }
    else if(orderKey == "CallScene")
    {
        return FileOrderType::CallScene;
    }
    else if(orderKey == "SetScenesPolling")
    {
        return FileOrderType::SetScenesPolling;
    }
    else if(orderKey == "PauseScenesPolling")
    {
        return FileOrderType::PauseScenesPolling;
    }
    else if(orderKey == "DownloadFile")
    {
        return FileOrderType::DownloadFile;
    }
    else if(orderKey == "GetClientInfo")
    {
        return FileOrderType::GetClientInfo;
    }
    else if(orderKey == "SaveClientInfo")
    {
        return FileOrderType::SaveClientInfo;
    }
    else if(orderKey == "SubtitleOsd")
    {
        return FileOrderType::SubtitleOsd;
    }
    else
    {
        return FileOrderType::Unknown;
    }
}

//设置命令文件筛选方式
void SoapTransmit::setFileSortWay(SoapTransmit::FileSortWay way)
{
    sortWay = way;
}

//取出命令文件筛选方式
SoapTransmit::FileSortWay SoapTransmit::getFileSortWay()
{
    return sortWay;
}

//设置当前UDP接收的数据
void SoapTransmit::setCurrUdpRecvData(QByteArray currData)
{
    currUdpRecvData = currData;
}

//取出当前UDP接收的数据
QByteArray SoapTransmit::getCurrUdpRecvData()
{
    return currUdpRecvData;
}

//设置客户端信息(ip和端口号)
void SoapTransmit::setClientInfo(ClientIpPortInfo clientInfo)
{
    this->clientInfo.ip = clientInfo.ip;
    this->clientInfo.port = clientInfo.port;
}

//取出客户端信息(ip和端口号)
SoapTransmit::ClientIpPortInfo SoapTransmit::getClientInfo()
{
    return this->clientInfo;
}

//设置需要回复端的信息(IP和端口)
void SoapTransmit::setRespInfo(SoapTransmit::RespIpPortInfo respInfo)
{
    this->respInfo.ip = respInfo.ip;
    this->respInfo.port = respInfo.port;
}

//取出当前回复端的信息(IP和端口)
SoapTransmit::RespIpPortInfo SoapTransmit::getRespInfo()
{
    return this->respInfo;
}

//设置信号源表和转发表是否正确
void SoapTransmit::setIsIdAndNodeListCorrect(bool isCorrect)
{
    isIdAndNodeListCorrect = isCorrect;
}

//取出信号源表和转发表标识
bool SoapTransmit::getIsIdAndNodeListCorrect()
{
    return isIdAndNodeListCorrect;
}


//UDP发送套接字接收消息槽
void SoapTransmit:: udpSockRecvData_toSendSock()
{
    while(udpSendSock->hasPendingDatagrams())
    {
        QByteArray datagram;
        //设置接收缓冲区大小
        datagram.resize(udpSendSock->pendingDatagramSize());
        RespIpPortInfo respInfo;
        udpSendSock->readDatagram(datagram.data(),datagram.size(),&respInfo.ip,&respInfo.port);
        //判断返回的是否是Json格式数据
        QJsonObject nodeObj = SoapPublicFunc::QStringToJsonObj(QString(datagram));
        if(nodeObj.isEmpty() == false)
        {
            //取出"node_ip"关键字的值并加入nodeIpList中
            nodeIpList.append(nodeObj["node_ip"].toString());
        }
        else
        {
            //不符合Json格式,退出
            return;
        }
    }
}

//UDP接收套接字接收消息槽
void SoapTransmit::udpSockRecvData()
{
    while(udpRecvSock->hasPendingDatagrams())
    {
        QByteArray datagram;
        //设置接收数据缓冲大小
        datagram.resize(udpRecvSock->pendingDatagramSize());
        ClientIpPortInfo clientInfo;
        udpRecvSock->readDatagram(datagram.data(),datagram.size(),&clientInfo.ip,&clientInfo.port);
        //保存接收到的数据
        setCurrUdpRecvData(datagram);
        //保存对方IP和端口
        setClientInfo(clientInfo);
        //处理接收到的消息
        udpRecvOrder(datagram);
    }
}

//取出命令头(注意,该函数只能取出符合scode2020标准的消息的命令头)
QString SoapTransmit::getOrderKey(QByteArray order)
{
    //检查是否符合scode2020标准,若不符合,返回空
    QString orderKey = "";
    QString tmpOrder = QString(order);
    if(tmpOrder.startsWith("{") == false || tmpOrder.endsWith("}") == false)
    {
        return orderKey;
    }
    tmpOrder.remove("{").remove("}");
    (tmpOrder.contains(";")) ? orderKey = tmpOrder.split(";")[0].trimmed() : orderKey = tmpOrder.trimmed();
    return orderKey;
}

//UDP收到消息时的处理函数(总函数)
void SoapTransmit::udpRecvOrder(QByteArray orderData)
{
    //每次接受到信息重置日志错误信息
    myRecvLogInfo.recvLogErr.clear();
    //判断消息是否符合Json格式,若不是,继续检查是否符合scode2020标准
    bool isScodeOrder = false;
    if(SoapPublicFunc::checkIsJson(orderData) == true)
    {
        QJsonObject jsonObj = SoapPublicFunc::QStringToJsonObj(QString(orderData));
        if(jsonObj.contains("request"))
        {
            //取出命令头
            QString orderKey = jsonObj.value("request").toString();
            switch (getOrderType(orderKey))
            {
            case FileOrderType::Json_GetSignalResourcesTree:udpRecvOrder_Json_GetSignalResourcesTree();break;
            default:return;
            }
            isScodeOrder = true;
        }
    }
    else
    {
        //验证消息是否符合scode2020标准(以"{"开始 以"}"结束 并包含已知的命令头)
        QString orderKey = getOrderKey(orderData);
        //根据命令头的类型调用对应的处理函数
        switch (getOrderType(orderKey))
        {
        case FileOrderType::GetNodes:udpRecvOrder_GetNodes(orderData);break;
        case FileOrderType::SetLockFrame:udpRecvOrder_SetLockFrame(orderData);break;
        case FileOrderType::GetTransmit:udpRecvOrder_GetTransmit(orderData);break;
        case FileOrderType::SetTransmit:udpRecvOrder_SetTransmit(orderData);break;
        case FileOrderType::GetWindows:udpRecvOrder_GetWindows(orderData);break;
        case FileOrderType::SetWindows:
        {
            lastSetWindows = QString(orderData);
            udpRecvOrder_SetWindows(orderData);
        }break;
        case FileOrderType::GetAudios:udpRecvOrder_GetAudios(orderData);break;
        case FileOrderType::SetAudios:udpRecvOrder_SetAudios(orderData);break;
        case FileOrderType::GetSceneState:udpRecvOrder_GetSceneState(orderData);break;
        case FileOrderType::SetSceneState:udpRecvOrder_SetSceneState(orderData);break;
        case FileOrderType::GetScene:udpRecvOrder_GetScene(orderData);break;
        case FileOrderType::GetScenes:udpRecvOrder_GetScenes(orderData);break;
        case FileOrderType::GetSceneNames:udpRecvOrder_GetSceneNames(orderData);break;
        case FileOrderType::SaveScene:udpRecvOrder_SaveScene(orderData);break;
        case FileOrderType::DeleteScene:udpRecvOrder_DeleteScene(orderData);break;
        case FileOrderType::ClearScenes:udpRecvOrder_ClearScenes(orderData);break;
        case FileOrderType::CallScene:udpRecvOrder_CallScene(orderData);break;
        case FileOrderType::SetScenesPolling:udpRecvOrder_SetScenesPolling(orderData);break;
        case FileOrderType::PauseScenesPolling:udpRecvOrder_PauseScenesPolling(orderData);break;
        case FileOrderType::DownloadFile:udpRecvOrder_DownloadFile(orderData);break;
        case FileOrderType::GetClientInfo:udpRecvOrder_GetClientInfo(orderData);break;
        case FileOrderType::SaveClientInfo:udpRecvOrder_SaveClientInfo(orderData);break;
        case FileOrderType::SubtitleOsd:udpRecvOrder_SubtitleOsd(orderData);break;
        default:return;
        }
        isScodeOrder = true;
    }

    //保存日志(日志格式:时间:xx,来源(目的地):xx,命令:xx,错误:xx)
    if(isScodeOrder)
    {
        if(myRecvLogInfo.recvLogSave)
        {
            myRecvLogInfo.recvLog.append("TIME:" + currTime().toUtf8() + ",SOURCE:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + orderData + ",ERROR:" + myRecvLogInfo.recvLogErr + "\n");
            if(myRecvLogInfo.recvLog.length() > 10000)
            {
                QFile recvLogFile(QDir::currentPath() + "/" + scodeLogFolderName + "/" + "RecvOrderLog_" + currDate() + ".txt");
                recvLogFile.open(QFile::Append);
                recvLogFile.write(myRecvLogInfo.recvLog);
                recvLogFile.close();
                myRecvLogInfo.recvLog.clear();
            }
        }
        if(mySendLogInfo.sendLogSave && mySendLogInfo.sendLog.length() >10000)
        {
            QFile sendLogFile(QDir::currentPath() + "/" + scodeLogFolderName + "/" + "SendOrderLog_" + currDate() + ".txt");
            sendLogFile.open(QFile::Append);
            sendLogFile.write(mySendLogInfo.sendLog);
            sendLogFile.close();
            mySendLogInfo.sendLog.clear();
        }
    }
}

//UDP收到消息时的处理函数(GetNodes)
void SoapTransmit::udpRecvOrder_GetNodes(QByteArray getNodes)
{
    Q_UNUSED(getNodes);
}

//UDP收到消息时的处理函数(SetLockFrame)
void SoapTransmit::udpRecvOrder_SetLockFrame(QByteArray setLockFrame)
{
    Q_UNUSED(setLockFrame);
}

//UDP收到消息时的处理函数(GetTransmit)
void SoapTransmit::udpRecvOrder_GetTransmit(QByteArray getTransmit)
{
    //提取参数
    QStringList getTransmitList = QString(getTransmit).remove("{").remove("}").split(";");
    //检查格式是否正确
    if(getTransmitList.count()<2 || getTransmitList.count()>3)
    {
        ui->statusbar->showMessage("警告!获取转发(GetTransmit)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetTransmit)order form incorrect";
        return;
    }
    int panel_sn = getTransmitList[1].toInt();
    quint16 port = 0;
    QString ip;
    bool sendBackTrack = true;
    if(getTransmitList.count() == 3)
    {
        ip = getTransmitList[2].split(",")[0];
        port = getTransmitList[2].split(",")[1].toInt();
        sendBackTrack = false;
    }
    //找到对应的SetTransmit文件并取出内容回复
    QFile setTransmitFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + "SetTransmit_" + QString::number(panel_sn) + ".txt");
    if(setTransmitFile.open(QFile::ReadOnly) == false)
    {
        ui->statusbar->showMessage(QString("警告!未找到SetTransmit_%1.txt文件").arg(panel_sn),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetTransmit)same panel file not found";
        return;
    }
    QByteArray setTransmitFileData = setTransmitFile.readAll();
    if(setTransmitFileData.isEmpty())
    {
        myRecvLogInfo.recvLogErr = "(GetTransmit)file data empty";
        return;
    }
    QByteArray getTransmitToD = "GetTransmit;panel;" + setTransmitFileData;
    SoapPublicFunc::addBraceForByteArray(getTransmitToD);
    if(sendBackTrack)
    {
        udpSendSock->writeDatagram(getTransmitToD,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + getTransmitToD + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(getTransmitToD,QHostAddress(ip),port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + ip + ":" + QByteArray::number(port) + ",ORDER:" + getTransmitToD + "\n");
        }
    }
}

//UDP收到消息时的处理函数(SetTransmit)
void SoapTransmit::udpRecvOrder_SetTransmit(QByteArray setTransmit)
{
    QStringList setTransmitList = QString(setTransmit).remove("{").remove("}").split(";");
    //检查命令格式是否正确
    if(setTransmitList.count() < 3)
    {
        ui->statusbar->showMessage("警告!设置转发(SetTransmit)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SetTransmit)order form incorrect";
        return;
    }
    //提取参数
    int panel_sn = setTransmitList[1].toInt();
    //保存参数到文件
    setTransmitList.removeAt(0);
    setTransmitList.removeAt(0);
    setTransmit = setTransmitList.join(";").toUtf8();
    QFile setTransmitFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + "SetTransmit_" + QString::number(panel_sn) + ".txt");
    setTransmitFile.open(QFile::ReadWrite);
    setTransmitFile.resize(0);
    setTransmitFile.write(setTransmit);
    setTransmitFile.close();
}

//UDP收到消息时的处理函数(GetWindows)
void SoapTransmit::udpRecvOrder_GetWindows(QByteArray getWindows)
{
    QStringList getWindowsList = QString(getWindows).remove("{").remove("}").split(";");
    //检查命令格式是否正确
    if(getWindowsList.count() <2 || getWindowsList.count() >3)
    {
        ui->statusbar->showMessage("警告!获取视频墙(GetWindows)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetWindows)order form incorrect";
        return;
    }
    //提取参数
    int panel_sn = getWindowsList[1].toInt();
    bool sendBackTrack = true;
    RespIpPortInfo respInfo;
    if(getWindowsList.count() == 3)
    {
        sendBackTrack = false;
        respInfo.ip = QHostAddress(getWindowsList[2].split(",")[0]);
        respInfo.port = getWindowsList[2].split(",")[1].toInt();
        setRespInfo(respInfo);
    }
    //生成回应的getWindowsToC信息
    QByteArray getWindowsToC;
    getWindowsToC.append("GetWindows;" + QByteArray::number(panel_sn));
    for(int i=0;i<windowsInfoList.count();i++)
    {
        if(panel_sn == windowsInfoList.at(i)->windowsNum)
        {
            QByteArray setWindows = windowsInfoList.at(i)->lastSetWindowsCont.trimmed();
            if(setWindows.isEmpty() == false)
            {
                getWindowsToC.append(";");
                QByteArrayList setWindowsList = setWindows.remove(0,1).remove(setWindows.count()-1,1).split(';');
                setWindowsList.removeAt(0);
                setWindowsList.removeAt(0);
                getWindowsToC.append(setWindowsList.join(";"));
            }
            break;
        }
    }
    //为命令添加大括号
    SoapPublicFunc::addBraceForByteArray(getWindowsToC);
    //回应消息并纪录发送日志
    if(sendBackTrack)
    {
        udpSendSock->writeDatagram(getWindowsToC,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + getWindowsToC + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(getWindowsToC,getRespInfo().ip,getRespInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + getWindowsToC + "\n");
        }
    }
}

//UDP收到消息时的处理函数(SetWindows)
void SoapTransmit::udpRecvOrder_SetWindows(QByteArray setWindows)
{
    //重置ipHasFoundList
    ipHasFoundList.clear();
    //提取视频墙号
    QStringList setWindowsList = QString(setWindows).remove("{").remove("}").split(";");
    int windowsNum = setWindowsList[1].toInt();
    //判断是开窗命令还是清空视频墙
    if(setWindowsList.count() == 2)
    {
        //在视频墙链表中找寻对应的视频墙号并提取出历史中该视频墙对应的信号列表
        QStringList removeIpList;
        for(int i=0;i<windowsInfoList.count();i++)
        {
            if(windowsNum == windowsInfoList[i]->windowsNum)
            {
                removeIpList = windowsInfoList[i]->transmitedIpList;
                //将对应的历史视频墙清空
                windowsInfoList.removeAt(i);
                break;
            }
        }
        //从该视频墙上删除的信号还要经过筛选
        int removeIndex = 0;
        M1:
        for(int i = removeIndex;i<removeIpList.count();i++)
        {
            for(int j=0;j<windowsInfoList.count();j++)
            {
                for(int k=0;k<windowsInfoList[j]->transmitedIpList.count();k++)
                {
                    if(removeIpList[i] == windowsInfoList[j]->transmitedIpList[k])
                    {
                        removeIpList.removeAt(i);
                        removeIndex = i;
                        goto M1;
                    }
                }
            }
        }
        for(int i=0;i<removeIpList.count();i++)
        {
            //在转发节点信息链表中寻找相应的IP,若找到清除该信号
            for(int j=0;j<nodeListInfoList.count();j++)
            {
                for(int k=0;k<nodeListInfoList[j]->transmitedIpList.count();k++)
                {
                    if(removeIpList[i] == nodeListInfoList[j]->transmitedIpList[k])
                    {
                        nodeListInfoList[j]->transmitedIpList.removeAt(k);
                        if(nodeListInfoList[j]->transmitedIpList.isEmpty())
                        {
                            nodeListInfoList[j]->currNodeState = NodeState::RemoveSigState;
                        }
                        goto M2;
                    }
                }
            }
            M2:;
        }
    }
    else
    {
        //生成ipHasFoundList,开窗命令与信号源链表中的相同的IP列表
        for(int i=0;i<idListInfoList.count();i++)
        {
            for(int j=2;j<setWindowsList.count();j++)
            {
                QString setWindowsIp = setWindowsList[j].split(",")[0];
                if(idListInfoList.at(i)->idListIp == setWindowsIp)
                {
                    ipHasFoundList.append(setWindowsIp);
                    break;
                }
            }
        }
        if(nodeListSigNum < ipHasFoundList.count())
        {
            ui->statusbar->showMessage("警告!转发节点可分配的信号总数不足!",statusBarShowTime_error);
            myRecvLogInfo.recvLogErr = "(SetWindows)signals not enough for distribute";
            return;
        }
        if(ipHasFoundList.isEmpty() == false)
        {
            //不包含历史转发或曾经清空了所有历史的视频墙
            if(windowsInfoList.isEmpty() == true)
            {
                //视频墙号链表为空,直接添加
                windowsInfo *myWindowsInfo = new windowsInfo;
                myWindowsInfo->windowsNum = windowsNum;
                myWindowsInfo->transmitedIpList = ipHasFoundList;
                windowsInfoList.append(myWindowsInfo);
                //将信号列表分配到转发节点链表中
                int index = 0;
                for(int i=0;i<nodeListInfoList.count();i++)
                {
                    for(int j=0;j<nodeListInfoList[i]->nodeListTransmitSigLim;j++)
                    {
                        nodeListInfoList[i]->transmitedIpList.append(ipHasFoundList[index]);
                        index++;
                        if(nodeListInfoList[i]->currNodeState != NodeState::ContainSigState)
                        {
                            nodeListInfoList[i]->currNodeState = NodeState::ContainSigState;
                        }
                        if(index == ipHasFoundList.count())
                        {
                            break;
                        }
                    }
                    if(index == ipHasFoundList.count())
                    {
                        break;
                    }
                }
            }
            //包含历史转发
            else
            {
                //寻找历史中是否出现过相同的视频墙号
                for(int i=0;i<windowsInfoList.count();i++)
                {
                    //若出现相同的视频墙号,先查找历史对应视频墙号的信号列表,若其中存在不在此刻信号墙对应列表的信号,从转发节点链表中删去
                    if(windowsNum == windowsInfoList.at(i)->windowsNum)
                    {
                        //生成待删除的信号列表,并在转发表中遍历删除
                        QStringList removeIpList;
                        //取出该视频墙上次的对应信号表
                        QStringList oldIpList = windowsInfoList[i]->transmitedIpList;
                        int sameIndex = i;
                        for(int i=0;i<oldIpList.count();i++)
                        {
                            for(int j=0;j<ipHasFoundList.count();j++)
                            {
                                if(oldIpList[i] == ipHasFoundList[j])
                                {
                                    break;
                                }
                                if(j == ipHasFoundList.count()-1)
                                {
                                    removeIpList.append(oldIpList[i]);
                                }
                            }
                        }
                        //待删除的信号列表需要筛选
                        int removeIndex = 0;
                        M3:
                        for(int i=removeIndex;i<removeIpList.count();i++)
                        {
                            for(int j=0;j<windowsInfoList.count();j++)
                            {
                                if(j == sameIndex)
                                {
                                    continue;
                                }
                                for(int k=0;k<windowsInfoList[j]->transmitedIpList.count();k++)
                                {
                                    if(removeIpList[i] == windowsInfoList[j]->transmitedIpList[k])
                                    {
                                        removeIpList.removeAt(i);
                                        removeIndex = i;
                                        goto M3;
                                    }
                                }
                            }
                        }
                        //在转发表中删除需要删除的信号
                        for(int i=0;i<removeIpList.count();i++)
                        {
                            for(int j=0;j<nodeListInfoList.count();j++)
                            {
                                for(int k=0;k<nodeListInfoList[j]->transmitedIpList.count();k++)
                                {
                                    if(removeIpList[i] == nodeListInfoList[j]->transmitedIpList[k])
                                    {
                                        nodeListInfoList[j]->transmitedIpList.removeAt(k);
                                        if(nodeListInfoList[j]->transmitedIpList.isEmpty())
                                        {
                                            nodeListInfoList[j]->currNodeState = NodeState::RemoveSigState;
                                        }
                                        goto M4;
                                    }
                                }
                            }
                            M4:;
                        }
                        //提取转发节点链表当前的所有信号,对应视频墙上此刻不存在的信号已被删去
                        QStringList transmitedIpList;
                        for(int i=0;i<nodeListInfoList.count();i++)
                        {
                            for(int j=0;j<nodeListInfoList[i]->transmitedIpList.count();j++)
                            {
                                transmitedIpList.append(nodeListInfoList[i]->transmitedIpList[j]);
                            }
                        }
                        //生成要添加的Ip列表
                        QStringList appendIpList;
                        if(transmitedIpList.isEmpty() == false)
                        {
                            for(int i=0;i<ipHasFoundList.count();i++)
                            {
                                for(int j=0;j<transmitedIpList.count();j++)
                                {
                                    if(ipHasFoundList[i] == transmitedIpList[j])
                                    {
                                        break;
                                    }
                                    if(j == transmitedIpList.count()-1)
                                    {
                                        appendIpList.append(ipHasFoundList[i]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            appendIpList = ipHasFoundList;
                        }

                        //若要添加的IP列表不为空,则添加
                        if(appendIpList.isEmpty() == false)
                        {
                            int appendIpListIndex = 0;
                            for(int j=0;j<nodeListInfoList.count();j++)
                            {
                                for(int k = nodeListInfoList[j]->transmitedIpList.count();k<nodeListInfoList[j]->nodeListTransmitSigLim;k++)
                                {
                                    nodeListInfoList[j]->transmitedIpList.append(appendIpList[appendIpListIndex]);
                                    appendIpListIndex ++;
                                    if(nodeListInfoList[j]->currNodeState != NodeState::ContainSigState)
                                    {
                                       nodeListInfoList[j]->currNodeState = NodeState::ContainSigState;
                                    }
                                    if(appendIpListIndex == appendIpList.count())
                                    {
                                        break;
                                    }
                                }
                                if(appendIpListIndex == appendIpList.count())
                                {
                                    break;
                                }
                            }
                        }
                        //将现有的IP列表覆盖到此视频墙
                        windowsInfoList[i]->transmitedIpList = ipHasFoundList;
                        //跳出
                        break;
                    }
                    if(i == windowsInfoList.count()-1)
                    {
                        //未找到匹配的视频墙号,增添新的结构体
                        windowsInfo *myWindowsInfo = new windowsInfo;
                        myWindowsInfo->windowsNum = windowsNum;
                        myWindowsInfo->transmitedIpList = ipHasFoundList;
                        windowsInfoList.append(myWindowsInfo);
                        //将转发节点链表中的所有信号取出并对比
                        QStringList transmitedIpList;
                        for(int i=0;i<nodeListInfoList.count();i++)
                        {
                            for(int j=0;j<nodeListInfoList[i]->transmitedIpList.count();j++)
                            {
                                transmitedIpList.append(nodeListInfoList[i]->transmitedIpList[j]);
                            }
                        }
                        //生成要添加的信号列表
                        QStringList appendIpList;
                        for(int i=0;i<ipHasFoundList.count();i++)
                        {
                            for(int j=0;j<transmitedIpList.count();j++)
                            {
                                if(ipHasFoundList[i] == transmitedIpList[j])
                                {
                                    break;
                                }
                                if(j == transmitedIpList.count()-1)
                                {
                                    appendIpList.append(ipHasFoundList[i]);
                                }
                            }
                        }
                        //若要添加的IP列表不为空,则添加
                        if(appendIpList.isEmpty() == false)
                        {
                            int appendIpListIndex = 0;
                            for(int j=0;j<nodeListInfoList.count();j++)
                            {
                                for(int k = nodeListInfoList[j]->transmitedIpList.count();k<nodeListInfoList[j]->nodeListTransmitSigLim;k++)
                                {
                                    nodeListInfoList[j]->transmitedIpList.append(appendIpList[appendIpListIndex]);
                                    appendIpListIndex++;
                                    if(nodeListInfoList[j]->currNodeState != NodeState::ContainSigState)
                                    {
                                        nodeListInfoList[j]->currNodeState = NodeState::ContainSigState;
                                    }
                                    if(appendIpListIndex == appendIpList.count())
                                    {
                                        break;
                                    }
                                }
                                if(appendIpListIndex == appendIpList.count())
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        //未找到任何对应信号源表的IP,只需保存
        else
        {
            bool findSamePan = false;
            for(int i=0;i<windowsInfoList.count();i++)
            {
                if(windowsInfoList.at(i)->windowsNum == windowsNum)
                {
                    findSamePan = true;
                    windowsInfoList[i]->transmitedIpList.clear();
                    break;
                }
            }
            if(findSamePan == false)
            {
                windowsInfo *myWindowsInfo = new windowsInfo;
                myWindowsInfo->windowsNum = windowsNum;
                myWindowsInfo->transmitedIpList.clear();
                windowsInfoList.append(myWindowsInfo);
            }
        }
    }

    //生成发送到组播地址的SetWindows命令
    QByteArray setWindowsToO;
    if(ipHasFoundList.isEmpty() == false)
    {
        for(int i=2;i<setWindowsList.count();i++)
        {
            QStringList setWindowsPerRowList = setWindowsList[i].split(",");
            QString setWindowsToOIp = setWindowsPerRowList[0];
            for(int j=0;j<ipHasFoundList.count();j++)
            {
                if(setWindowsToOIp == ipHasFoundList[j])
                {
                    QString ip = SoapPublicFunc::alterIpSection(setWindowsToOIp,0,224),transmitIp;
                    //找到该信号源在分配在那个转发节点中并取出下标
                    bool isFind = false;
                    int idIndex;
                    for(int k=0;k<nodeListInfoList.count();k++)
                    {
                        for(int l=0;l<nodeListInfoList.at(k)->transmitedIpList.count();l++)
                        {
                            if(setWindowsToOIp == nodeListInfoList.at(k)->transmitedIpList.at(l))
                            {
                                isFind = true;
                                transmitIp = nodeListInfoList.at(k)->nodeListIp;
                                idIndex = l;
                                break;
                            }
                        }
                        if(isFind == true)
                        {
                            break;
                        }
                    }
                    //计算主码流和辅码流端口
                    quint32 mainStreamPort = 20001 + (SoapPublicFunc::getIpSection(transmitIp,3)-1)*27 + (idIndex*3);
                    quint32 minorStreamPort = mainStreamPort + 1;
                    //判断需要的是主流还是辅流,以此使用端口
                    (setWindowsPerRowList[1] == "url0") ? setWindowsPerRowList[1] = QString("udp://%1:%2").arg(ip).arg(mainStreamPort)
                                                        : setWindowsPerRowList[1] = QString("udp://%1:%2").arg(ip).arg(minorStreamPort);
                    //检查该信号源是否含有辅流,如有直接退出,没有则将端口调整为辅码流端口后退出
                    for(int m=0;m<idListInfoList.count();m++)
                    {
                        if(setWindowsToOIp == idListInfoList.at(m)->idListIp)
                        {
                            if(idListInfoList.at(m)->idListMinorStream.isEmpty() == true)
                            {
                                setWindowsPerRowList[1] = QString("udp://%1:%2").arg(ip).arg(minorStreamPort);
                            }
                            break;
                        }
                    }
                    //重置setWindowsList的对应行
                    setWindowsList[i] = setWindowsPerRowList.join(",");
                    break;
                }
            }
        }
        setWindowsToO = setWindowsList.join(";").toUtf8();
        //为命令添加大括号
        SoapPublicFunc::addBraceForByteArray(setWindowsToO);
    }
    else
    {
        setWindowsToO = setWindows;
    }

    //将setWindows命令保存到对应视频墙号的windowsInfoList中
    for(int i=0;i<windowsInfoList.count();i++)
    {
        if(windowsNum == windowsInfoList.at(i)->windowsNum)
        {
            windowsInfoList[i]->lastSetWindowsCont = setWindowsToO;
            break;
        }
    }

    //用于存放抓图的数组
    QJsonArray snapUrlArray;
    //开始根据每个转发节点中分配的信号列表(不一定每次都能分配满!)转发到指定的转发节点和端口
    for(int i=0;i<nodeListInfoList.count();i++)
    {
        switch (nodeListInfoList.at(i)->currNodeState)
        {
        //该转发节点存在信号列表,转发到对应的节点
        case SoapTransmit::NodeState::ContainSigState:
        {
            QByteArray setTransmitToT = "SetTransmit;-1",setSnapToT;
            QJsonObject setSnapObj = BaseOrderLib::defJson_SetRelayList();
            QJsonValueRef refValue = setSnapObj.find("Value").value();
            QJsonArray valueArr = refValue.toArray();
            //取出该转发节点当前要转发的信号源列表
            QStringList sigList = nodeListInfoList.at(i)->transmitedIpList;

            //计算抓图端口的起始位
            quint16 snapPort = 20001 + (SoapPublicFunc::getIpSection(nodeListInfoList.at(i)->nodeListIp,3)-1)*27;
            for(int j=0;j<sigList.count();j++)
            {
                QString ip;
                quint16 port = 0;
                for(int k=0;k<idListInfoList.count();k++)
                {
                    //找到对应的信号IP
                    if(sigList[j] == idListInfoList.at(k)->idListIp)
                    {
                        //生成SetTranmitToT的原地址URL和转发地址URL
                        ip = SoapPublicFunc::alterIpSection(sigList[j],0,224);
                        port = portCacul(ip);

                        setTransmitToT.append(QString("\n;%1,udp://%2:%3").arg(idListInfoList.at(k)->idListMajorStream).arg(ip).arg(port));
                        if(idListInfoList.at(k)->idListMinorStream.isEmpty() == false)
                        {
                            setTransmitToT.append(QString("\n;%1,udp://%2:%3").arg(idListInfoList.at(k)->idListMinorStream).arg(ip).arg(port+1));
                        }

                        //生成抓图Value数组对象,引用修改其中的默认键值对
                        QJsonObject valueObj = BaseOrderLib::setRelayList_valueObj();
                        valueObj["id"] = j;
                        QJsonValueRef refUrl = valueObj.find("url").value();
                        QJsonObject urlObj = refUrl.toObject();
                        //若辅码流不为空则对应码流填入参数,否则只将唯一的流填入辅码流
                        if(idListInfoList.at(k)->idListMinorStream.isEmpty() == false)
                        {
                            urlObj["main"] = idListInfoList.at(k)->idListMajorStream;
                            urlObj["sub"] = idListInfoList.at(k)->idListMinorStream;
                        }
                        else
                        {
                            urlObj["sub"] = idListInfoList.at(k)->idListMajorStream;
                        }
                        refUrl = urlObj;
                        QJsonValueRef refTransmit = valueObj.find("transmit").value();
                        QJsonObject transmitObj = refTransmit.toObject();
                        //添加主码流字段
                        QJsonValueRef refMain = transmitObj.find("main").value();
                        QJsonObject mainObj = refMain.toObject();
                        mainObj["ip"] = ip;
                        mainObj["port"] = snapPort + j*3;
                        mainObj["length"] = snapParam.snap_len;
                        mainObj["type"] = snapParam.snap_type;
                        refMain = mainObj;
                        //添加辅码流字段(若辅码流不为空)
                        QJsonValueRef refSub = transmitObj.find("sub").value();
                        QJsonObject subObj = refSub.toObject();
                        subObj["ip"] = ip;
                        subObj["port"] = snapPort + j*3 +1;
                        subObj["length"] = snapParam.snap_len;
                        subObj["type"] = snapParam.snap_type;
                        refSub = subObj;
                        //添加抓图字段
                        QJsonValueRef refSnap = transmitObj.find("snap").value();
                        QJsonObject snapObj = refSnap.toObject();
                        snapObj["snap_w"] = snapParam.snap_w;
                        snapObj["snap_h"] = snapParam.snap_h;
                        snapObj["snap_len"] = snapParam.snap_len;
                        snapObj["snap_delay"] = snapParam.snap_delay;
                        snapObj["snap_quality"] = snapParam.snap_quality;
                        snapObj["mcast_enable"] = snapParam.mcast_enable;
                        snapObj["mcast_ip"] = SoapPublicFunc::alterIpSection(ip,1,169);
                        snapObj["mcast_port"] = snapPort + j*3 + 2;
                        snapObj["mcast_freq"] = snapParam.mcast_freq;
                        refSnap  = snapObj;
                        refTransmit = transmitObj;
                        valueArr.append(valueObj);
                        //将对象加入数组
                        QJsonObject snapUrlObj;
                        snapUrlObj.insert("id",sigList.at(j));
                        snapUrlObj.insert("snapUrl",QString("%1:%2").arg(SoapPublicFunc::alterIpSection(ip,1,169)).arg(snapPort + j*3 + 2));
                        snapUrlArray.append(snapUrlObj);
                        //跳出
                        break;
                    }
                }
            }
            //添加大括号
            SoapPublicFunc::addBraceForByteArray(setTransmitToT);
            //处理抓图Json
            refValue = valueArr;
            setSnapToT = SoapPublicFunc::jsonObjToQstring_lite(setSnapObj).toUtf8();
            //发送setTransmit命令到转发节点
            udpSendSock->writeDatagram(setTransmitToT,QHostAddress(nodeListInfoList.at(i)->nodeListIp),41234);
            //发送抓图回显清空命令
            udpSendSock->writeDatagram(SoapPublicFunc::jsonObjToQstring_lite(BaseOrderLib::closeRelayList()).toUtf8(),QHostAddress(nodeListInfoList.at(i)->nodeListIp),41234);
            //发送抓图回显到转发节点
            udpSendSock->writeDatagram(setSnapToT,QHostAddress(nodeListInfoList.at(i)->nodeListIp),41234);
        }break;
        //该转发节点的节点本次被清除,发送重置命令并重置该节点状态
        case SoapTransmit::NodeState::RemoveSigState:
        {
            //发送{SetTransmit;-1}到转发节点
            udpSendSock->writeDatagram("{SetTransmit;-1}",QHostAddress(nodeListInfoList[i]->nodeListIp),41234);
            nodeListInfoList[i]->transmitedIpList.clear();
            nodeListInfoList[i]->currNodeState = NodeState::InitState;
        }break;
        default:;
        }
    }

    //套接字等待
    udpSendSock->waitForBytesWritten(50);
    //发送开窗命令到组播地址
    udpSendSock->writeDatagram(setWindowsToO,QHostAddress("224.168.1.1"),41234);

    //在发送完后发送SetWindows到客户端(原路返回)
    QJsonObject setWindowsToCObj = BaseOrderLib::defJson_setWindowsToC();
    setWindowsToCObj.insert("data",snapUrlArray);
    QByteArray setWindowsToC = SoapPublicFunc::jsonObjToQstring_lite(setWindowsToCObj).toUtf8();
    udpSendSock->writeDatagram(setWindowsToC,getClientInfo().ip,getClientInfo().port);
}

//UDP收到消息时的处理函数(JSON格式的(GetSignalResourcesTree))
void SoapTransmit::udpRecvOrder_Json_GetSignalResourcesTree()
{
    //引用修改GetSignalResourcesTree对象的data值
    QJsonObject GetSignalResourcesTreeObj = json_getSignalResourcesTree();
    QJsonValueRef refData = GetSignalResourcesTreeObj.find("data").value();
    QJsonObject dataObj = refData.toObject();
    QJsonValueRef refList = dataObj.find("list").value();
    QJsonArray listArra = refList.toArray();
    for(int i=0;i<idListInfoList.count();i++)
    {
        QJsonObject listSubObj;
        listSubObj.insert("id",idListInfoList[i]->idListIp);
        listSubObj.insert("name",idListInfoList[i]->idListName);
        listArra.append(listSubObj);
    }
    refList = listArra;
    refData = dataObj;
    //回复消息并纪录发送日志
    QByteArray getSignalRes = SoapPublicFunc::jsonObjToQstring(GetSignalResourcesTreeObj).toUtf8();
    udpSendSock->writeDatagram(getSignalRes,getClientInfo().ip,getClientInfo().port);
    if(mySendLogInfo.sendLogSave)
    {
        mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + getSignalRes + "\n");
    }
}

//UDP收到消息时的处理函数(GetSceneState)
void SoapTransmit::udpRecvOrder_GetSceneState(QByteArray getSceneState)
{
    //提取参数
    QStringList getSceneStateList = QString(getSceneState).remove("{").remove("}").split(";");
    if(getSceneStateList.count() < 2 || getSceneStateList.count() > 3)
    {
        ui->statusbar->showMessage("警告!获取场景状态(GetSceneState)命令格式错误",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetSceneState)form incorrect";
        return;
    }
    int panel_sn = getSceneStateList[1].toInt();
    RespIpPortInfo respInfo;
    bool sendBackTrack = true;
    if(getSceneStateList.count() == 3)
    {
        sendBackTrack = false;
        respInfo.ip = QHostAddress(getSceneStateList[2].split(",")[0]);
        respInfo.port = getSceneStateList[2].split(",")[1].toUInt();
        setRespInfo(respInfo);
    }
    //生成要回应的GetSceneState命令
    QByteArray getSceneState_resp = "GetSceneState;" + QByteArray::number(panel_sn);
    for(int i=0;i<setSceneStateInfoList.count();i++)
    {
        if(panel_sn == setSceneStateInfoList.at(i)->panel_sn)
        {
            getSceneState_resp.append(";" + QByteArray::number(setSceneStateInfoList.at(i)->time));
            break;
        }
    }
    SoapPublicFunc::addBraceForByteArray(getSceneState_resp);
    //回应消息
    if(sendBackTrack)
    {
        udpSendSock->writeDatagram(getSceneState_resp,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + getSceneState_resp + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(getSceneState_resp,getRespInfo().ip,getRespInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + getSceneState_resp + "\n");
        }
    }
}

//UDP收到消息时的处理函数(SetSceneState)
void SoapTransmit::udpRecvOrder_SetSceneState(QByteArray setSceneState)
{
    //提取参数
    QStringList setSceneStateList = QString(setSceneState).remove("{").remove("}").split(";'");
    if(setSceneStateList.count() != 3)
    {
        ui->statusbar->showMessage("警告!设置场景状态(SetSceneState)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SetSceneState)form incorrect";
        return;
    }
    int panel_sn = setSceneStateList[1].toInt();
    int time = setSceneStateList[2].toInt();
    //保存场景状态到链表
    bool findSamePanel = false;
    for(int i=0;i<setSceneStateInfoList.count();i++)
    {
        if(setSceneStateInfoList.at(i)->panel_sn == panel_sn)
        {
            findSamePanel = true;
            setSceneStateInfoList[i]->time = time;
            break;
        }
    }
    //若未找到对应的视频墙号,新增
    if(findSamePanel == false)
    {
        setSceneStateInfo *mySceneState = new setSceneStateInfo;
        mySceneState->panel_sn = panel_sn;
        mySceneState->time = time;
    }
}

//UDP收到消息时的处理函数(GetScene)
void SoapTransmit::udpRecvOrder_GetScene(QByteArray getScene)
{
    //提取参数(视频墙号,可能需要回应的IP和端口)
    QStringList getSceneList = QString(getScene).remove("{").remove("}").split(";");
    int panel_sn;
    QString scene_id;
    RespIpPortInfo respDevInfo;
    bool sendBacktrack = true;
    switch (getSceneList.count())
    {
    case 3:
    {
        panel_sn = getSceneList[1].toInt();
        scene_id = getSceneList[2];
    }
        break;
    case 4:
    {
        panel_sn = getSceneList[1].toInt();
        scene_id = getSceneList[2];
        respDevInfo.ip = QHostAddress(getSceneList[3].split(",")[0]);
        respDevInfo.port = getSceneList[3].split(",")[1].toInt();
        setRespInfo(respDevInfo);
        sendBacktrack = false;
    }
        break;
    default:
    {
        ui->statusbar->showMessage("警告!获取场景(GetScene)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScene)order form incorrect";
    }
        return;
    }
    //寻找对应视频墙号的文件
    QString sceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(sceneFileName) == false)
    {
        ui->statusbar->showMessage(QString("未找到 %1 视频墙号场景信息文件").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr= "(GetScenes)SaveScenes file not found";
        return;
    }
    //打开文件并取出内容
    QFile sceneFile(sceneFileName);
    sceneFile.open(QFile::ReadWrite);
    QByteArray sceneData = sceneFile.readAll();
    if(sceneData.isEmpty() == true)
    {
        ui->statusbar->showMessage(QString("%1 视频墙场景信息文件内容为空").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScene)SaveScenes file is empty";
        return;
    }
    QString getScenes_resp;
    QJsonObject sceneObj = SoapPublicFunc::QStringToJsonObj(QString(sceneData));
    QJsonArray sceneListArra = sceneObj.find("SceneList")->toArray();
    if(sceneListArra.count() == 0)
    {
        ui->statusbar->showMessage(QString("%1 视频墙暂不包含任何场景信息!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenes)panel_sn contains no scenes info";
        //回复没有场景的GetScenes消息
        getScenes_resp = "{GetScenes;0}";
    }
    else
    {
        getScenes_resp = "GetScenes;" + QString::number(panel_sn);
        for(int i=0;i<sceneListArra.count();i++)
        {
            QJsonObject sceneListObj = sceneListArra[i].toObject();
            getScenes_resp.append(";" + sceneListObj.find("Scene_id")->toString() + "," + sceneListObj.find("Name")->toString() + "," + sceneListObj.find("SetWindows")->toString());
        }
        //为命令添加大括号
        SoapPublicFunc::addBraceForString(getScenes_resp);
    }
    //转换编码格式utf8->gbk
    QTextCodec *utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);
    QTextCodec *gbk = QTextCodec::codecForName("gbk");
    QByteArray gbk_getScenes = gbk->fromUnicode(utf8->toUnicode(getScenes_resp.toLocal8Bit().data()));
    //回应消息并纪录发送日志
    if(sendBacktrack)
    {
        udpSendSock->writeDatagram(gbk_getScenes,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + gbk_getScenes + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(gbk_getScenes,getRespInfo().ip,getRespInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + gbk_getScenes + "\n");
        }
    }
    //恢复编码格式
    QTextCodec::setCodecForLocale(nullptr);
}

//UDP收到消息时的处理函数(GetScenes)
void SoapTransmit::udpRecvOrder_GetScenes(QByteArray getScenes)
{
    //提取参数(视频墙号,可能需要回应的IP和端口)
    QStringList getScenesList = QString(getScenes).remove("{").remove("}").split(";");
    int panel_sn;
    RespIpPortInfo respDevInfo;
    bool sendBacktrack = true;
    switch (getScenesList.count())
    {
    case 2:panel_sn = getScenesList[1].toInt();break;
    case 3:
    {
        panel_sn = getScenesList[1].toInt();
        respDevInfo.ip = QHostAddress(getScenesList[2].split(",")[0]);
        respDevInfo.port = getScenesList[2].split(",")[1].toInt();
        setRespInfo(respDevInfo);
        sendBacktrack = false;
    }
        break;
    default:
    {
        ui->statusbar->showMessage("警告!获取场景(GetScenes)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenes)order form incorrect";
    }
        return;
    }
    //寻找对应视频墙号的文件
    QString sceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(sceneFileName) == false)
    {
        ui->statusbar->showMessage(QString("未找到 %1 视频墙号场景信息文件").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr= "(GetScenes)SaveScenes file not found";
        return;
    }
    //打开文件并取出内容
    QFile sceneFile(sceneFileName);
    sceneFile.open(QFile::ReadWrite);
    QByteArray sceneData = sceneFile.readAll();
    if(sceneData.isEmpty() == true)
    {
        ui->statusbar->showMessage(QString("%1 视频墙场景信息文件内容为空").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenes)SaveScenes file is empty";
        return;
    }
    QString getScenes_resp;
    QJsonObject sceneObj = SoapPublicFunc::QStringToJsonObj(QString(sceneData));
    QJsonArray sceneListArra = sceneObj.find("SceneList")->toArray();
    if(sceneListArra.count() == 0)
    {
        ui->statusbar->showMessage(QString("%1 视频墙暂不包含任何场景信息!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenes)panel_sn contains no scenes info";
        //回复没有场景的GetScenes消息
        getScenes_resp = "{GetScenes;0}";
    }
    else
    {
        getScenes_resp = "GetScenes;" + QString::number(panel_sn);
        for(int i=0;i<sceneListArra.count();i++)
        {
            QJsonObject sceneListObj = sceneListArra[i].toObject();
            getScenes_resp.append(";" + sceneListObj.find("Scene_id")->toString() + "," + sceneListObj.find("Name")->toString() + "," + sceneListObj.find("SetWindows")->toString());
        }
        //为命令添加大括号
        SoapPublicFunc::addBraceForString(getScenes_resp);
    }
    //转换编码格式utf8->gbk
    QTextCodec *utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);
    QTextCodec *gbk = QTextCodec::codecForName("gbk");
    QByteArray gbk_getScenes = gbk->fromUnicode(utf8->toUnicode(getScenes_resp.toLocal8Bit().data()));
    //回应消息并纪录发送日志
    if(sendBacktrack)
    {
        udpSendSock->writeDatagram(gbk_getScenes,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + gbk_getScenes + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(gbk_getScenes,getRespInfo().ip,getRespInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + gbk_getScenes + "\n");
        }
    }
    //恢复编码格式
    QTextCodec::setCodecForLocale(nullptr);
}

//UDP收到消息时的处理函数(GetSceneNames)
void SoapTransmit::udpRecvOrder_GetSceneNames(QByteArray getSceneNames)
{
    //提取参数(视频墙号,需要回应的IP和端口)
    QStringList getSceneNamesList = QString(getSceneNames).remove("{").remove("}").split(";");
    int panel_sn;
    bool sendBacktrack = true;
    switch (getSceneNamesList.count())
    {
    case 2:panel_sn = getSceneNamesList[1].toInt();break;
    case 3:
    {
        RespIpPortInfo respDevInfo;
        panel_sn = getSceneNamesList[1].toInt();
        //保存需要回应的设备的信息
        respDevInfo.ip = QHostAddress(getSceneNamesList[2].split(",")[0]);
        respDevInfo.port = getSceneNamesList[2].split(",")[1].toInt();
        setRespInfo(respDevInfo);
        sendBacktrack = false;
    }
        break;
    default:
    {
        ui->statusbar->showMessage("警告!获取场景名(GetSceneNames)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenesNames)form incorrect";
        return;
    }
    }
    //寻找对应视频墙号的文件
    QString sceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(sceneFileName) == false)
    {
        ui->statusbar->showMessage(QString("警告!未找到对应 %1 视频墙号的文件!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenesNames)SaveScenes file not found";
        return;
    }
    QFile sceneFile(sceneFileName);
    sceneFile.open(QFile::ReadWrite);
    QByteArray sceneData = sceneFile.readAll();
    if(sceneData.isEmpty())
    {
        ui->statusbar->showMessage(QString("警告! %1 视频墙文件内容为空!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenesNames)SaveScenes file is empty";
        return;
    }
    QJsonObject sceneObj = SoapPublicFunc::QStringToJsonObj(QString(sceneData));
    QJsonArray sceneListArra = sceneObj.find("SceneList")->toArray();
    if(sceneListArra.count() == 0)
    {
        ui->statusbar->showMessage(QString("%1 视频墙暂未保存有任何场景信息!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(GetScenesNames)panel_sn contains no scenes info";
        return;
    }
    QString getSceneNames_resp = "GetSceneNames;" + QString::number(panel_sn);
    for(int i=0;i<sceneListArra.count();i++)
    {
        QJsonObject sceneListObj = sceneListArra[i].toObject();
        getSceneNames_resp.append(";" + sceneListObj.find("Scene_id")->toString() + "," + sceneListObj.find("Name")->toString());
    }
    //为命令添加大括号
    SoapPublicFunc::addBraceForString(getSceneNames_resp);

    //转换编码格式 utf8->gbk
    QTextCodec *utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);
    QTextCodec *gbk = QTextCodec::codecForName("gbk");
    QByteArray gbk_getSceneNames = gbk->fromUnicode(utf8->toUnicode(getSceneNames_resp.toLocal8Bit().data()));
    //回应消息并纪录发送日志
    if(sendBacktrack)
    {
        udpSendSock->writeDatagram(gbk_getSceneNames,getClientInfo().ip,getClientInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getClientInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getClientInfo().port) + ",ORDER:" + gbk_getSceneNames + "\n");
        }
    }
    else
    {
        udpSendSock->writeDatagram(gbk_getSceneNames,getRespInfo().ip,getRespInfo().port);
        if(mySendLogInfo.sendLogSave)
        {
            mySendLogInfo.sendLog.append("TIME:" + currTime().toUtf8() + ",DESTINATION:" + getRespInfo().ip.toString().remove(":").remove("f").toUtf8() + ":" + QByteArray::number(getRespInfo().port) + ",ORDER:" + gbk_getSceneNames + "\n");
        }
    }
    //恢复编码格式
    QTextCodec::setCodecForLocale(nullptr);
}

//UDP收到消息时的处理函数(SaveScene)
void SoapTransmit::udpRecvOrder_SaveScene(QByteArray saveScene)
{
    //防止中文乱码(场景名称可能带有中文)
    saveScene = QString::fromLocal8Bit(saveScene).toUtf8();
    //取出改名中的参数(视频墙号,场景序号和名称)
    QStringList saveSceneList = QString(saveScene).remove("{").remove("}").split(";");
    //检查命令格式
    if(saveSceneList.count() != 3)
    {
        ui->statusbar->showMessage("警告!SaveScene的格式不正确!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SaveScene)form incorrect";
        return;
    }
    int panel_sn = saveSceneList[1].toInt();
    QString scene_id , name;
    scene_id = saveSceneList[2].split(",")[0];
    name = saveSceneList[2].split(",")[1];

    //检查对应的视频墙是否存在最近的SetWindows命令
    QByteArray currPanelLastSetWindows;
    bool findSamePanel = false;
    for(int i=0;i<windowsInfoList.count();i++)
    {
        if(windowsInfoList.at(i)->windowsNum == panel_sn)
        {
            currPanelLastSetWindows = windowsInfoList.at(i)->lastSetWindowsCont;
            findSamePanel = true;
            break;
        }
    }
    //检查是否对应的视频墙
    if(findSamePanel == false)
    {
        ui->statusbar->showMessage(QString("错误!未找到对应的视频墙号 %1 信息,无法保存").arg(panel_sn),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SaveScene)panel not found";
        return;
    }
    //检查该视频墙号上对应的最近的SetWindows命令是否为空
    if(currPanelLastSetWindows.isEmpty())
    {
        ui->statusbar->showMessage(QString("警告!%1视频墙最近的SetWindows命令为空\n无法保存一个空场景!").arg(panel_sn),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SaveScene)panel's last setWindows order is empty,can't save scene";
    }
    else
    {
        //打开场景保存用Json文件
        QFile saveSceneFile(QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json");
        saveSceneFile.open(QFile::ReadWrite);
        QByteArray saveSceneData = saveSceneFile.readAll();
        if(saveSceneData.isEmpty() == true)
        {
            //若场景内容为空,直接添加当前场景内容
            QJsonObject sceneData = sceneJson();
            QJsonValueRef sceneListRef = sceneData.find("SceneList").value();
            QJsonArray sceneListArray = sceneListRef.toArray();
            QJsonObject sceneObj;
            sceneObj.insert("Scene_id",scene_id);
            sceneObj.insert("Name",name);
            sceneObj.insert("SetWindows",QString(currPanelLastSetWindows));
            sceneListArray.append(sceneObj);
            sceneListRef = sceneListArray;
            //写入文件
            saveSceneData = SoapPublicFunc::jsonObjToQstring(sceneData).toUtf8();
            saveSceneFile.write(saveSceneData);
        }
        else
        {
            //存在历史场景内容,先查找是否存在相同的场景序号,若找到则覆盖场景名称和SetWindows命令;否则增加场景信息
            QJsonObject sceneData = SoapPublicFunc::QStringToJsonObj(QString(saveSceneData));
            QJsonValueRef sceneListRef = sceneData.find("SceneList").value();
            QJsonArray sceneListArray = sceneListRef.toArray();
            bool findSameSceneId = false;
            for(int i=0;i<sceneListArray.count();i++)
            {
                QJsonValueRef sceneListObjRef = sceneListArray[i];
                QJsonObject sceneListObj = sceneListObjRef.toObject();
                //找到相同的场景序号,覆盖信息
                if(scene_id == sceneListObj.find("Scene_id")->toString())
                {
                    sceneListObj["Name"] = name;
                    sceneListObj["SetWindows"] = QString(currPanelLastSetWindows);
                    sceneListObjRef = sceneListObj;
                    findSameSceneId = true;
                    break;
                }
            }
            //若没有找到相同的场景序号,则新增
            if(findSameSceneId == false)
            {
                QJsonObject sceneObj;
                sceneObj.insert("Scene_id",scene_id);
                sceneObj.insert("Name",name);
                sceneObj.insert("SetWindows",QString(currPanelLastSetWindows));
                sceneListArray.append(sceneObj);
            }
            sceneListRef = sceneListArray;
            //重写文件内容
            saveSceneData = SoapPublicFunc::jsonObjToQstring(sceneData).toUtf8();
            saveSceneFile.resize(0);
            saveSceneFile.write(saveSceneData);
        }
        saveSceneFile.close();
        //提示
        ui->statusbar->showMessage("保存场景完成",statusBarShowTime);
        //刷新列表
        sort_scodeList();
    }
}

//UDP收到消息时的处理函数(DeleteScene)
void SoapTransmit::udpRecvOrder_DeleteScene(QByteArray deleteScene)
{
    //取出参数(视频墙号，场景序号)
    //QByteArray deleteScene = getCurrUdpRecvData();
    QStringList deleteSceneList = QString(deleteScene).remove("{").remove("}").split(";");
    if(deleteSceneList.count() != 3)
    {
        ui->statusbar->showMessage("警告!删除场景(DeleteScene)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(DeleteScene)form incorrect";
        return;
    }
    int panel_sn = deleteSceneList[1].toInt();
    QString del_scene_id = deleteSceneList[2];
    //寻找对应视频墙的文件
    QString deleteSceneName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(deleteSceneName) == false)
    {
        ui->statusbar->showMessage("警告!未找到对应视频墙号的保存文件!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(DeleteScene)SaveScene file not found";
        return;
    }
    QFile saveSceneFile(deleteSceneName);
    saveSceneFile.open(QFile::ReadWrite);
    QByteArray sceneData = saveSceneFile.readAll();
    if(sceneData.isEmpty() == true)
    {
        ui->statusbar->showMessage(QString("%1 视频墙文件内容为空!无法删除场景!").arg(QString::number(panel_sn)),statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(DeleteScene)SaveScene file is empty";
        return;
    }
    //将内容转换为QJsonObject然后查找对应的场景序号
    QJsonObject sceneObj = SoapPublicFunc::QStringToJsonObj(QString(sceneData));
    QJsonValueRef sceneListRef = sceneObj.find("SceneList").value();
    QJsonArray sceneListArra = sceneListRef.toArray();
    bool isFindSameScene_id = false;
    for(int i=0;i<sceneListArra.count();i++)
    {
        QJsonObject sceneListObj = sceneListArra[i].toObject();
        QString scene_id = sceneListObj.find("Scene_id")->toString();
        if(scene_id == del_scene_id)
        {
            //找到相同的场景序号删除改下标
            sceneListArra.removeAt(i);
            //跳出
            isFindSameScene_id = true;
            break;
        }
    }
    if(isFindSameScene_id == false)
    {
        //遍历结束后未找到相同场景序号,报错
        ui->statusbar->showMessage("警告!未找到对应场景序号的场景\n删除场景失败!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(DeleteScene)panel_sn not found";
        saveSceneFile.close();
        return;
    }
    sceneListRef = sceneListArra;
    sceneData = SoapPublicFunc::jsonObjToQstring(sceneObj).toUtf8();
    //修改文件后将内容重写
    saveSceneFile.resize(0);
    saveSceneFile.write(sceneData);
    saveSceneFile.close();
    //提示
    ui->statusbar->showMessage(QString("删除视频墙 %1 的场景 %2 完成").arg(QString::number(panel_sn)).arg(del_scene_id),statusBarShowTime);
}

//UDP收到消息时的处理函数(ClearScenes)
void SoapTransmit::udpRecvOrder_ClearScenes(QByteArray clearScenes)
{
    //取出参数(视频墙号)
    QStringList clearScenesList = QString(clearScenes).remove("{").remove("}").split(";");
    if(clearScenesList.count() != 2)
    {
        ui->statusbar->showMessage("警告!清空场景(ClearScenes)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(ClearScenes)form incorrect";
        return;
    }
    int panel_sn = clearScenesList[1].toInt();
    //寻找相同视频墙号的SaveScene_panel_sn 文件
    QString sceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(sceneFileName) == false)
    {
        ui->statusbar->showMessage("警告!不存在保存 " + QString::number(panel_sn) + " 视频墙号的文件",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(ClearScenes)SaveScene file not found";
        return;
    }
    //清空该视频墙信息列表
    QFile clearSceneFile(sceneFileName);
    clearSceneFile.open(QFile::ReadWrite);
    clearSceneFile.resize(0);
    clearSceneFile.close();
    //提示
    ui->statusbar->showMessage(QString("清空 %1 视频墙列表成功!").arg(QString::number(panel_sn)),statusBarShowTime);
}

//UDP收到消息时的处理函数(CallScene)
void SoapTransmit::udpRecvOrder_CallScene(QByteArray callScene)
{
    //获取命令
    QStringList callSceneList = QString(callScene).remove("{").remove("}").split(";");
    //检查callScene是否符合格式
    if(callSceneList.count() != 3)
    {
        ui->statusbar->showMessage("警告!调取场景(CallScene)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(CallScene)form incorrect";
        return;
    }
    //提取参数(视频墙号,场景序号)
    int panel_sn = callSceneList[1].trimmed().toInt();
    QString call_scene_id = callSceneList[2].trimmed();
    //寻找是否存在对应视频墙号的Json文件
    QString sceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/SaveScene_" + QString::number(panel_sn) + ".json";
    if(SoapPublicFunc::isFileNameExist(sceneFileName) == false)
    {
        transMsgBoxRemind("警告!未找到对应视频墙号的文件!",3000);
        myRecvLogInfo.recvLogErr = "(CallScene)SaveScene file not found";
        return;
    }
    QFile sceneFile(sceneFileName);
    sceneFile.open(QFile::ReadWrite);
    QByteArray sceneData = sceneFile.readAll();
    sceneFile.close();
    if(sceneData.isEmpty() == true)
    {
        transMsgBoxRemind(QString("%1 视频墙文件内容为空!\n无法调取场景!").arg(QString::number(panel_sn)),3000);
        myRecvLogInfo.recvLogErr = "(CallScene)SaveScene file is empty";
        return;
    }
    QJsonObject sceneObj = SoapPublicFunc::QStringToJsonObj(QString(sceneData));
    QJsonArray sceneListArra = sceneObj.find("SceneList")->toArray();
    bool isFindSameSceneId = false;
    QString setWindows;
    for(int i=0;i<sceneListArra.count();i++)
    {
        QJsonObject sceneListObj = sceneListArra[i].toObject();
        QString scene_id = sceneListObj.find("Scene_id")->toString();
        if(scene_id == call_scene_id)
        {
            //找到相同的场景序号,取出SetWindows命令
            setWindows = sceneListObj.find("SetWindows")->toString();
            //跳出
            isFindSameSceneId = true;
            break;
        }
    }
    if(isFindSameSceneId == false)
    {
        transMsgBoxRemind("警告!未找到对应的场景序号,调取场景失败!",3000);
        myRecvLogInfo.recvLogErr = "(CallScene)panel_sn not found";
        return;
    }
    //处理SetWindows命令
    udpRecvOrder_SetWindows(setWindows.toUtf8());
}

//UDP收到消息时的处理函数(SetScenesPolling)
void SoapTransmit::udpRecvOrder_SetScenesPolling(QByteArray setScenesPolling)
{
    //提取参数(视频墙号,场景间隔时间,场景序号)
    QStringList setScenesPollingList = QString(setScenesPolling).remove("{").remove("}").split(";");
    if(setScenesPollingList.count() <3 && setScenesPollingList.count() >4)
    {
        ui->statusbar->showMessage("场景轮询设置(SetScenesPolling)命令格式错误！",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(SetScenesPolling)form incorrect";
        return;
    }
    int panel_sn = setScenesPollingList[1].toInt();
    int polling_time = setScenesPollingList[2].toInt();
    //判断是设置并开始轮询还是关闭轮询
    if(polling_time == 0)
    {
        //关闭轮询(查找对应的视频墙号,并将其暂停然后关闭)
        bool isFindSamePanel = false;
        for(int i=0;i<scenesPollingInfoList.count();i++)
        {
            //找到相同的视频墙号并关闭场景轮询,无论是否正在轮询
            if(panel_sn == scenesPollingInfoList.at(i)->scenesPollingPanel)
            {
                isFindSamePanel = true;
                //设置该子线程对象在场景轮询暂停后关闭
                scenesPollingInfoList[i]->closeAfterPause = true;
                //发送暂停信号
                emit sig_scenesPollingPause(panel_sn);
                break;
            }
        }
        //若未找到对应的视频墙号,可能已经关闭了该视频墙的场景轮询
        if(isFindSamePanel == false)
        {
            ui->statusbar->showMessage(QString("未找到 %1 视频墙轮询信息,场景轮询可能已关闭！").arg(QString::number(panel_sn)),statusBarShowTime_error);
            return;
        }
    }
    else
    {
        //设置并开启场景轮询
        //提取场景序号列表
        if(setScenesPollingList.count() != 4)
        {
            ui->statusbar->showMessage("场景轮询设置命令格式错误！",statusBarShowTime_error);
            myRecvLogInfo.recvLogErr = "(SetScenesPolling)form incorrect";
            return;
        }
        QStringList scene_idList = setScenesPollingList[3].split(",");
        //查找对应的视频墙号场景保存文件提取出场景序号对应的SetWindows命令
        QString saveSceneFileName = QDir::currentPath() + "/" + scodeListFolderName + "/" + "SaveScene_" + QString::number(panel_sn) + ".json";
        if(SoapPublicFunc::isFileNameExist(saveSceneFileName) == false)
        {
            ui->statusbar->showMessage(QString("警告！未找到 %1 视频墙场景保存文件\n无法设置场景轮询").arg(QString::number(panel_sn)),statusBarShowTime_error);
            myRecvLogInfo.recvLogErr = "(SetScenesPolling)SaveScene file not found";
            return;
        }
        //打开场景保存文件(SaveScene_视频墙号.json)并取出对应场景序号的setWindows命令
        QFile saveSceneFile(saveSceneFileName);
        saveSceneFile.open(QFile::ReadWrite);
        QByteArray saveSceneData = saveSceneFile.readAll();
        if(saveSceneData.isEmpty() == true)
        {
            ui->statusbar->showMessage("警告!%1 视频墙场景保存文件数据为空!",statusBarShowTime_error);
            myRecvLogInfo.recvLogErr = "(SetScenesPolling)SaveScene file is empty";
            return;
        }
        //取出数据后通过寻找scene_idList(场景序号列表)对应的SetWindows命令列表
        QJsonObject saveSceneObj = SoapPublicFunc::QStringToJsonObj(QString(saveSceneData));
        QJsonArray saveSceneListArra = saveSceneObj.find("SceneList")->toArray();
        QStringList setWindowsList;
        for(int i=0;i<scene_idList.count();i++)
        {
            for(int j=0;j<saveSceneListArra.count();j++)
            {
                if(scene_idList[i] == saveSceneListArra[j].toObject().find("Scene_id")->toString())
                {
                    //找到相同的场景序号,添加SetWindows命令
                    setWindowsList.append(saveSceneListArra[j].toObject().find("SetWindows")->toString());
                    break;
                }
            }
        }
        //检查是否添加了任何场景轮询信息
        if(setWindowsList.isEmpty() == true)
        {
            ui->statusbar->showMessage("警告!未找到任何对应场景序号的SetWindows消息\n设置轮询失败",statusBarShowTime_error);
            myRecvLogInfo.recvLogErr = "(SetScenesPolling)no SetWindows-list match scene_id-list";
            return;
        }
        //打开(若没有则创建(SetScenesPolling_视频墙号.json))对应视频墙的场景轮询文件
        QFile setScenesPollingFile(QDir::currentPath() + "/" + scodeListFolderName + "/SetScenesPolling_" + QString::number(panel_sn) + ".json");
        setScenesPollingFile.open(QFile::ReadWrite);
        //生成场景轮询Json
        QJsonObject setScenesPollingObj = scenesPollingJson();
        setScenesPollingObj["Polling_Time"] = polling_time;
        QJsonValueRef setScenePollingRef = setScenesPollingObj.find("ScenesPollingInfo").value();
        QJsonArray scenesPollingInfoArra = setScenePollingRef.toArray();
        for(int i=0;i<setWindowsList.count();i++)
        {
            QJsonObject setWindowsObj;
            setWindowsObj.insert("SetWindows",setWindowsList[i]);
            scenesPollingInfoArra.append(setWindowsObj);
        }
        setScenePollingRef = scenesPollingInfoArra;
        //将场景轮询Json写入(覆盖)此文件
        setScenesPollingFile.resize(0);
        setScenesPollingFile.write(SoapPublicFunc::jsonObjToQstring(setScenesPollingObj).toUtf8());
        setScenesPollingFile.close();
        //列表刷新
        sort_scodeList();

        /******************************场景轮询执行部分******************************/
        //在场景轮询链表中查找(设置轮询并开始)
        bool isFindSamePanel = false;
        for(int i=0;i<scenesPollingInfoList.count();i++)
        {
            if(panel_sn == scenesPollingInfoList.at(i)->scenesPollingPanel)
            {
                isFindSamePanel = true;
                //找到相同的视频墙号,发送开始轮询的信号(注意：若在视频墙1正在轮询时对该墙覆盖设置轮询且参数不一致时,该视频墙的场景轮询会覆盖且重置(场景间隔时间或者SetWindowsList命令与之前的不一致))
                scenesPollingInfoList[i]->scenesPollingList = setWindowsList;
                scenesPollingInfoList[i]->scenesPollingTime = polling_time;
                emit sig_scenesPollingStart(scenesPollingInfoList.at(i)->scenesPollingPanel,scenesPollingInfoList.at(i)->scenesPollingList,scenesPollingInfoList.at(i)->scenesPollingTime);
                //跳出
                break;
            }
        }
        //若未找到对应的视频墙号,曾创建一个新的子线程并开始其场景轮询
        if(isFindSamePanel == false)
        {
            //数量小于场景轮询子线程可分配上限,允许创建新的子线程
            if(scenesPollingInfoList.count() < this->maxScenesPollingThread)
            {
                //动态分配场景轮询线程(初始化参数:1轮询的视频墙号 2轮询间隔 3轮询的开窗命令列表 4轮询状态 5轮询的子线程对象 6轮询的自定义线程对象 7标志位:是否在轮询停止后关闭释放自身)
                scenesPollingInfo *myScenesPollingInfo = new scenesPollingInfo;
                myScenesPollingInfo->scenesPollingPanel = panel_sn;
                myScenesPollingInfo->scenesPollingTime = polling_time;
                myScenesPollingInfo->scenesPollingList = setWindowsList;
                myScenesPollingInfo->scenesIsPolling = false;
                myScenesPollingInfo->scenesSubThread = new QThread(nullptr);
                myScenesPollingInfo->scenesPollingThread = new ScenesPollingThread(nullptr,panel_sn);
                myScenesPollingInfo->closeAfterPause = false;
                //将自定义线程对象移入子线程
                myScenesPollingInfo->scenesPollingThread->moveToThread(myScenesPollingInfo->scenesSubThread);
                //保存该结构体指针到链表
                scenesPollingInfoList.append(myScenesPollingInfo);
                /******************************为创建的场景轮询子线程绑定信号和槽******************************/
                //场景轮询启动信号
                connect(this,&SoapTransmit::sig_scenesPollingStart,myScenesPollingInfo->scenesPollingThread,&ScenesPollingThread::deal_scenesPollingStart,Qt::AutoConnection);
                //场景轮询暂停信号
                connect(this,&SoapTransmit::sig_scenesPollingPause,myScenesPollingInfo->scenesPollingThread,&ScenesPollingThread::deal_scenesPollingPause,Qt::AutoConnection);
                //场景轮询启动回传信号
                connect(myScenesPollingInfo->scenesPollingThread,&ScenesPollingThread::sig_scenesPollingCurrScene,this,&SoapTransmit::deal_scenesPollingCurrScene,Qt::AutoConnection);
                //场景轮询暂停回传信号
                connect(myScenesPollingInfo->scenesPollingThread,&ScenesPollingThread::sig_scenesPollingIsPaused,this,&SoapTransmit::deal_scenesPollingIsPaused,Qt::AutoConnection);
                //启动子线程对象
                if(myScenesPollingInfo->scenesSubThread->isRunning() == false)
                {
                    myScenesPollingInfo->scenesSubThread->start();
                }
                //发送场景轮询开始信号
                emit sig_scenesPollingStart(myScenesPollingInfo->scenesPollingPanel,myScenesPollingInfo->scenesPollingList,myScenesPollingInfo->scenesPollingTime);
                //设置轮询对象的轮询状态
                myScenesPollingInfo->scenesIsPolling = true;
            }
            else
            {
                ui->statusbar->showMessage("警告！场景轮询子线程数达到上限,不可继续分配！",statusBarShowTime_error);
                myRecvLogInfo.recvLogErr = "(SetScenesPolling)ScenesPollingInfoList reach to limit,no more distribute";
            }
        }
    }
}

//UDP收到消息时的处理函数(PauseScenesPolling)
void SoapTransmit::udpRecvOrder_PauseScenesPolling(QByteArray pauseScenesPolling)
{
    //提取参数(视频墙号,轮询暂停或继续)
    QStringList pauseScenesPollingList = QString(pauseScenesPolling).remove("{").remove("}").split(";");
    if(pauseScenesPollingList.count() != 3)
    {
        ui->statusbar->showMessage("警告!暂停(继续)场景轮询(PauseScenesPolling)命令格式错误!",statusBarShowTime_error);
        myRecvLogInfo.recvLogErr = "(PauseScensPolling)form incorrect";
        return;
    }
    int panel_sn = pauseScenesPollingList[1].toInt();
    //提取状态参数(1为暂停,0为继续)
    int state = pauseScenesPollingList[2].toInt();
    //首先找出对应的视频墙
    bool isFindSamePanel = false;
    for(int i=0;i<scenesPollingInfoList.count();i++)
    {
        if(panel_sn == scenesPollingInfoList.at(i)->scenesPollingPanel)
        {
            isFindSamePanel = true;
            //根据状态参数操作
            switch (state)
            {
            //请求继续,只在该视频墙场景轮询状态为暂停时有效
            case 0:
            {
                if(scenesPollingInfoList.at(i)->scenesIsPolling == false)
                {
                    //发送继续信号
                    emit sig_scenesPollingStart(scenesPollingInfoList.at(i)->scenesPollingPanel,scenesPollingInfoList.at(i)->scenesPollingList,scenesPollingInfoList.at(i)->scenesPollingTime);
                    //改变该轮询对象的轮询状态
                    scenesPollingInfoList[i]->scenesIsPolling = true;
                }
            }
                break;
            //请求暂停,只在该视频墙场景轮询状态为继续时有效
            case 1:
            {
                if(scenesPollingInfoList.at(i)->scenesIsPolling == true)
                {
                    //发送暂停信号
                    emit sig_scenesPollingPause(scenesPollingInfoList.at(i)->scenesPollingPanel);
                }
            }
                break;
            default:
            {
                ui->statusbar->showMessage(QString("PauseScenesPolling命令 存在无效的状态参数！"),statusBarShowTime_error);
                myRecvLogInfo.recvLogErr = "(PauseScensPolling)contains invalid parametors";
            }
                return;
            }
            //跳出
            break;
        }
    }
    //若未找到对应的视频墙号,无论是继续还是暂停都无效
    if(isFindSamePanel == false)
    {
        ui->statusbar->showMessage("警告！未找到对应视频墙号的场景轮询对象!",statusBarShowTime_error);
        return;
    }
}

//UDP收到消息时的处理函数(DownloadFile)
void SoapTransmit::udpRecvOrder_DownloadFile(QByteArray downloadFile)
{
    //提取参数(视频墙号,文件名称,发送iP,发送端口,每包长度,发送数据包时的时间间隔)
    QStringList downloadFileList = QString(downloadFile).remove("{").remove("}").split(";");
    if(downloadFileList.count() <4 ||downloadFileList.count() >5)
    {
        transMsgBoxRemind(QString("警告!传输文件命令格式错误!\nDownloadFile :%1").arg(QString(downloadFile)),5000);
        myRecvLogInfo.recvLogErr = "(DownloadFile)form incorrect";
        return;
    }
    int panel_sn = downloadFileList[1].toInt();
    QString fileName = downloadFileList[2];
    QStringList paramList = downloadFileList[3].split(",");
    if(paramList.count() <3 || paramList.count() >4)
    {
        transMsgBoxRemind(QString("警告!传输文件参数格式错误!\nDownloadFile :%1").arg(QString(downloadFile)),5000);
        myRecvLogInfo.recvLogErr = "(DownloadFile)parametors form incorrect";
        return;
    }
    QString send_ip = paramList[0];
    quint16 send_port = paramList[1].toInt();
    quint32 send_len = paramList[2].toInt();
    quint32 send_interval = 10;
    if(paramList.count() == 4)
    {
        send_interval = paramList[3].toInt();
    }
    QList<quint32> package_snList;
    if(downloadFileList.count() == 5)
    {
        QStringList packList = downloadFileList[4].split(",");
        for(int i=0;packList.count();i++)
        {
            package_snList.append(packList[i].toInt());
        }
    }
    //寻找是否存在相同视频墙号的文件传输线程
    for(int i=0;i<downloadFileInfoList.count();i++)
    {
        //若找到相同的视频墙号文件线程,退出
        if(downloadFileInfoList.at(i)->downloadFilePanel == panel_sn)
        {
            transMsgBoxRemind(QString("注意!存在相同视频墙号的文件传输命令!\n请等待 %1 文件传输完成后继续!").arg(QString::number(panel_sn)),3000);
            myRecvLogInfo.recvLogErr = "(DownloadFile)same panel_sn DownloadFile thread is on going";
            return;
        }
    }
    //未找到相同的视频墙号,创建一个新的子线程(前提小于可分配的文件传输线程上限)
    if(downloadFileInfoList.count() < this->maxDownloadFileThread)
    {
        //动态分配文件传输线程
        downloadFileInfo *myDownloadFileInfo = new downloadFileInfo;
        myDownloadFileInfo->downloadFilePanel = panel_sn;
        myDownloadFileInfo->downloadFileName = fileName;
        myDownloadFileInfo->downloadFilePort = send_port;
        myDownloadFileInfo->downloadFileIp = send_ip;
        myDownloadFileInfo->downloadFilePackLen = send_len;
        myDownloadFileInfo->downloadFileInterval = send_interval;
        myDownloadFileInfo->downloadFilePack_sn = package_snList;
        myDownloadFileInfo->downloadFileAbort = false;
        myDownloadFileInfo->downloadFileState = DownloadFileThread::DownloadFileState::DownloadFileInit;
        myDownloadFileInfo->downloadFileSubThread = new QThread(nullptr);
        myDownloadFileInfo->downloadFileThread = new DownloadFileThread(nullptr,panel_sn,myDownloadFileInfo->downloadFileIp,myDownloadFileInfo->downloadFilePort);
        //将自定义线程对象移入子线程
        myDownloadFileInfo->downloadFileThread->moveToThread(myDownloadFileInfo->downloadFileSubThread);
        //保存该结构体指针到链表
        downloadFileInfoList.append(myDownloadFileInfo);
        /******************************为创建的文件传输子线程绑定信号和槽******************************/
        //文件传输开始信号
        connect(this,&SoapTransmit::sig_downloadFileStart,myDownloadFileInfo->downloadFileThread,&DownloadFileThread::dealDownloadFileStart,Qt::AutoConnection);
        //文件传输暂停(强行终止)信号
        connect(this,&SoapTransmit::sig_downloadFileAbort,myDownloadFileInfo->downloadFileThread,&DownloadFileThread::dealDownloadFileAbort,Qt::AutoConnection);
        //文件传输状态回传的处理
        connect(myDownloadFileInfo->downloadFileThread,&DownloadFileThread::sig_downloadSituation,this,&SoapTransmit::deal_downloadFileSituation,Qt::AutoConnection);
        //启动子线程对象
        if(myDownloadFileInfo->downloadFileSubThread->isRunning() == false)
        {
            myDownloadFileInfo->downloadFileSubThread->start();
        }
        //发送文件传输开始信号
        emit sig_downloadFileStart(myDownloadFileInfo->downloadFilePanel,myDownloadFileInfo->downloadFileName,myDownloadFileInfo->downloadFileIp,myDownloadFileInfo->downloadFilePort,myDownloadFileInfo->downloadFilePackLen,myDownloadFileInfo->downloadFileInterval,myDownloadFileInfo->downloadFilePack_sn);
    }
    else
    {
        transMsgBoxRemind("警告！传输文件子线程数达到上限,不可继续分配！",5000);
        myRecvLogInfo.recvLogErr = "(DownloadFile)downloadFileInfoList reach to limit,no more distribute";
    }
}

//UDP收到消息时的处理函数(GetClientInfo)
void SoapTransmit::udpRecvOrder_GetClientInfo(QByteArray getClientInfo)
{
    //提取参数(客户端id,应答发送的目的地址和端口,可能省略)
    QStringList getClientInfoList = QString(getClientInfo).remove("{").remove("}").split(";");
    QString client_id = getClientInfoList[1];
    //标记原路返回或发送到指定的应答IP和端口
    bool sendBacktrack = true;
    if(getClientInfoList.count() == 3)
    {
        RespIpPortInfo respInfo;
        respInfo.ip = QHostAddress(getClientInfoList[2].split(",")[0]);
        respInfo.port = getClientInfoList[2].split(",")[1].toInt();
        setRespInfo(respInfo);
        sendBacktrack = false;
    }
    //取出对应的客户端文件信息
    QString clientFileName = QDir::currentPath() + "/" + scodeListFolderName + "/" + "ClientInfo(" + client_id + ")" + ".txt";
    QFile clientFile(clientFileName);
    if(clientFile.open(QFile::ReadOnly) == false)
    {
        transMsgBoxRemind(QString("未找到客户端 %1 信息文件\n获取该客户端信息失败！").arg(client_id),5000);
        myRecvLogInfo.recvLogErr = "(GetClientInfo)ClientInfo file not found";
    }
    else
    {
        QByteArray clientInfo = "GetClientInfo;";
        clientInfo.append(clientFile.readAll());
        SoapPublicFunc::addBraceForByteArray(clientInfo);
        //回复命令
        (sendBacktrack == true) ? udpSendSock->writeDatagram(clientInfo,clientInfo.length(),this->getClientInfo().ip,this->getClientInfo().port)
                                : udpSendSock->writeDatagram(clientInfo,clientInfo.length(),this->getRespInfo().ip,this->getRespInfo().port);
    }
    //关闭文件
    clientFile.close();
}

//UDP收到消息时的处理函数(SaveClientInfo)
void SoapTransmit::udpRecvOrder_SaveClientInfo(QByteArray saveClientInfo)
{
    //提取参数(客户端id和客户希望存储于节点上的内容,内容不可以包含";"分号)
    QStringList saveClientInfoList = QString(saveClientInfo).remove(saveClientInfo.length()-1,1).remove(0,1).split(";");
    if(saveClientInfoList.count() != 2)
    {
        transMsgBoxRemind(QString("错误!设置客户端信息(SaveClientInfo)命令格式不合法"),3000);
        myRecvLogInfo.recvLogErr = "(SaveClientInfo)form incorrect";
    }
    QString client_id = saveClientInfoList[1].section(",",0,0).trimmed();
    QString client_data = saveClientInfoList[1].mid(saveClientInfoList[1].indexOf(',')+1);
    //将数据写入文件(覆盖)
    QString clientInfoFileName = QDir::currentPath() + "/" + scodeListFolderName + "/" + "ClientInfo(" + client_id + ")" + ".txt";
    QFile clientInfoFile(clientInfoFileName);
    clientInfoFile.open(QFile::ReadWrite);
    clientInfoFile.resize(0);
    clientInfoFile.write(client_data.toUtf8());
    clientInfoFile.close();
    //刷新列表
    sort_scodeList();
    //提示
    ui->statusbar->showMessage(QString("保存客户端 %1 信息完成").arg(client_id),statusBarShowTime);
}

//UDP收到消息时的处理函数(GetAudio)
void SoapTransmit::udpRecvOrder_GetAudios(QByteArray getAudios)
{
    //提取参数(视频墙号,应答的IP和端口)
    QStringList getAudiosList = QString(getAudios).remove("{").remove("}").split(";");
    int panel_sn;
    bool sendBacktrack = true;
    switch (getAudiosList.count())
    {
    case 2:panel_sn = getAudiosList[1].toInt();break;
    case 3:
    {
        panel_sn = getAudiosList[1].toInt();
        RespIpPortInfo respInfo;
        respInfo.ip = QHostAddress(getAudiosList[2].split(",")[0]);
        respInfo.port = getAudiosList[2].split(",")[1].toInt();
        setRespInfo(respInfo);
        sendBacktrack = false;
    }
        break;
    default:
    {
        transMsgBoxRemind(QString("警告!获取音频命令格式错误!\nGetAudios : %1").arg(QString(getAudios)),3000);
        myRecvLogInfo.recvLogErr = "(GetAudios)form incorrect";
    }
        return;
    }
    //取出内容(从已保存的SetAudios_视频墙号.txt中)
    QString setAudiosFileName = QDir::currentPath() + "/" + scodeListFolderName + "/" + "SetAudios_" + QString::number(panel_sn) + ".txt";
    QFile setAudiosFile(setAudiosFileName);
    if(setAudiosFile.open(QFile::ReadOnly) == false)
    {
        transMsgBoxRemind(QString("警告!未找到对应视频墙号 %1 的音频信息文件!").arg(QString::number(panel_sn)),3000);
        myRecvLogInfo.recvLogErr = "(GetAudios)panel_sn auidos file not found";
    }
    else
    {
        QByteArray audiosData = "GetAudios;" + QByteArray::number(panel_sn) + ";";
        audiosData.append(setAudiosFile.readAll());
        SoapPublicFunc::addBraceForByteArray(audiosData);
        //回复内容
        (sendBacktrack == true) ? udpSendSock->writeDatagram(audiosData,audiosData.length(),getClientInfo().ip,getClientInfo().port)
                                : udpSendSock->writeDatagram(audiosData,audiosData.length(),getRespInfo().ip,getRespInfo().port);
    }
    //关闭文件
    setAudiosFile.close();
}

//UDP收到消息时的处理函数(SetAudio)
void SoapTransmit::udpRecvOrder_SetAudios(QByteArray setAudios)
{
    //提取参数(视频墙号,音频信号源的流URL地址)
    QStringList setAudiosList = QString(setAudios).remove("{").remove("}").split(";");
    if(setAudiosList.count() != 3)
    {
        transMsgBoxRemind(QString("警告!设置音频命令格式错误!\nSetAudios : %1").arg(QString(setAudios)),3000);
        myRecvLogInfo.recvLogErr = "(SetAudios)form incorrect";
        return;
    }
    int panel_sn = setAudiosList[1].toInt();
    QString audio_url = setAudiosList[2].trimmed();
    //保存内容(覆盖内容)
    QString setAudioFileName = QDir::currentPath() + "/" + scodeListFolderName + "/" + "SetAudios_" + QString::number(panel_sn) + ".txt";
    QFile setAudioFile(setAudioFileName);
    setAudioFile.open(QFile::ReadWrite);
    setAudioFile.resize(0);
    setAudioFile.write(audio_url.toUtf8());
    setAudioFile.close();
    //刷新列表
    sort_scodeList();
    //取出消息并转发给组播地址
    udpSendSock->writeDatagram(getCurrUdpRecvData(),getCurrUdpRecvData().length(),QHostAddress("224.168.1.1"),41234);
}

//UDP收到消息时的处理函数(SubtitleOsd)
void SoapTransmit::udpRecvOrder_SubtitleOsd(QByteArray subtitleOsd)
{
    //转发给组播地址
    udpSendSock->writeDatagram(subtitleOsd,subtitleOsd.length(),QHostAddress("224.168.1.1"),41234);
}

//https://slangelec-rd.yuque.com/docs/share/9a904afd-3b9f-4058-bfc3-08c094e2a749?#
//Json_GetSignalResourcesTree回复示例
QJsonObject SoapTransmit::json_getSignalResourcesTree()
{
    //默认的原始消息
    QJsonObject GetSignalResourcesTreeObj;
    GetSignalResourcesTreeObj.insert("code",0);
    GetSignalResourcesTreeObj.insert("msg","ok");
    GetSignalResourcesTreeObj.insert("response","GetSignalResourcesTree");
    QJsonObject dataObj;
    QJsonArray listArra;
    dataObj.insert("widget","600001");
    dataObj.insert("list",listArra);
    GetSignalResourcesTreeObj.insert("data",dataObj);
    //返回QJsonObject对象
    return GetSignalResourcesTreeObj;
}

//场景Json示例
QJsonObject SoapTransmit::sceneJson()
{
    QJsonObject sceneObj;
    QJsonArray sceneListArra;
    sceneObj.insert("SceneList",sceneListArra);
    return sceneObj;
}

//场景轮询Json示例
QJsonObject SoapTransmit::scenesPollingJson()
{
    QJsonObject scenesPollingObj;
    QJsonArray scenesPollingInfoArra;
    scenesPollingObj.insert("Polling_Time",1);
    scenesPollingObj.insert("ScenesPollingInfo",scenesPollingInfoArra);
    return scenesPollingObj;
}

//端口计算
quint16 SoapTransmit::portCacul(QString ip)
{
    QStringList ipList = ip.split(".");
    return (30000 + (ipList[2].right(1).toInt()*256 + ipList[3].toInt())*4);
}

//重新将信号源列表和转发列表内容读入内存
bool SoapTransmit::reloadIdAndNodeListData()
{
    //清空链表
    idListInfoList.clear();
    nodeListInfoList.clear();
    //取出idList.txt(信号源表)内容
    QFile idListFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + idListFileName);
    idListFile.open(QFile::ReadWrite);
    QByteArray idListFileData = idListFile.readAll();
    idListFile.close();
    //取出nodeList.txt(转发表)内容
    QFile nodeListFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + nodeListFileName);
    nodeListFile.open(QFile::ReadWrite);
    QByteArray nodeListFileData = nodeListFile.readAll();
    nodeListFile.close();
    //检查内容是否为空
    if(idListFileData.isEmpty() || nodeListFileData.isEmpty())
    {
        //数据为空,提前返回
        return false;
    }
    //转发表信号总数置0
    nodeListSigNum = 0;
    //通过";"分割信号源表和转发表的每一行内容并将内容读入结构体链表
    QStringList idListFileList = QString(idListFileData).split(";");
    QStringList nodeListFileList = QString(nodeListFileData).split(";");
    for(int i=0;i<idListFileList.count();i++)
    {
        QStringList idPerRowList = idListFileList[i].split(",");
        //若信号源表格式不正确,提前返回并清除链表信息
        if(idPerRowList.count() != 4)
        {
            idListInfoList.clear();
            return false;
        }
        idListInfo *myIdListInfo = new idListInfo;
        myIdListInfo->idListIp = idPerRowList[0].trimmed();
        myIdListInfo->idListName = idPerRowList[1].trimmed();
        myIdListInfo->idListMajorStream = idPerRowList[2].trimmed();
        myIdListInfo->idListMinorStream = idPerRowList[3].trimmed();
        idListInfoList.append(myIdListInfo);
    }
    for(int i=0;i<nodeListFileList.count();i++)
    {
        QStringList nodePerRowList = nodeListFileList[i].split(",");
        //若转发表格式不正确,提前返回并清除链表信息
        if(nodePerRowList.count() != 2)
        {
            nodeListInfoList.clear();
            nodeListSigNum = 0;
            return false;
        }
        nodeListInfo *myNodeListInfo = new nodeListInfo;
        myNodeListInfo->nodeListIp = nodePerRowList[0].trimmed();
        myNodeListInfo->nodeListTransmitSigLim = nodePerRowList[1].trimmed().toUInt();
        //初始化时该节点已转发的IP列表为空
        myNodeListInfo->transmitedIpList.clear();
        myNodeListInfo->currNodeState = NodeState::InitState;
        nodeListInfoList.append(myNodeListInfo);
        //累加可转发的信号总数
        nodeListSigNum += myNodeListInfo->nodeListTransmitSigLim;
    }
    //检查可转发的信号总数
    if(nodeListSigNum <=0)
    {
        nodeListInfoList.clear();
        nodeListSigNum = 0;
        return false;
    }
    return true;
}

//transmit程序的弹窗提示
void SoapTransmit::transMsgBoxRemind(QString text)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",text,QMessageBox::Ok,this);
    box->setWindowIcon(QIcon(":/Transmit/txt_Blue.png"));
    box->setButtonText(QMessageBox::Ok,"确定");
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->show();
}

//transmit程序的弹窗提示(重载1)
void SoapTransmit::transMsgBoxRemind(QString text,quint32 ms)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",text,QMessageBox::Ok,this);
    box->setWindowIcon(QIcon(":/Transmit/txt_Blue.png"));
    box->setButtonText(QMessageBox::Ok,"确定");
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->show();
}

//transmit程序的弹窗提示(重载2)
void SoapTransmit::transMsgBoxRemind(QString text,quint32 ms,QString iconUrl)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",text,QMessageBox::Ok,this);
    box->setWindowIcon(QIcon(iconUrl));
    box->setButtonText(QMessageBox::Ok,"确定");
    box->setFont(QFont("微软雅黑",11));
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->show();
}

//transmit程序模态弹窗
void SoapTransmit::transModelMsgBoxRemind(QString text,quint32 ms,QString iconUrl,QFont font)
{
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"提示",text,QMessageBox::Ok,this);
    box->setWindowIcon(QIcon(iconUrl));
    box->setButtonText(QMessageBox::Ok,"确定");
    box->setFont(font);
    box->setAttribute(Qt::WA_DeleteOnClose);
    QTimer::singleShot(ms,box,SLOT(accept()));
    box->exec();
}

//处理子线程轮询开始后的参数回传
void SoapTransmit::deal_scenesPollingCurrScene(QString setWindows)
{
    //处理SetWindows命令
    udpRecvOrder_SetWindows(setWindows.toUtf8());
}

//处理子线程轮询暂停后的回传
void SoapTransmit::deal_scenesPollingIsPaused(int panel_sn)
{
    //暂停对应视频墙号的场景子线程
    bool isFindSamePanel = false;
    for(int i=0;i<scenesPollingInfoList.count();i++)
    {
        //若找到对应的视频墙号,暂停该线程对象
        if(panel_sn == scenesPollingInfoList.at(i)->scenesPollingPanel)
        {
            isFindSamePanel = true;
            //设置该视频墙轮询暂停标记
            scenesPollingInfoList[i]->scenesIsPolling = false;
            //判断是否需要释放该子线程对象
            if(scenesPollingInfoList.at(i)->closeAfterPause == true)
            {
                //线程退出
                while(scenesPollingInfoList[i]->scenesSubThread->isRunning())
                {
                    scenesPollingInfoList[i]->scenesSubThread->quit();
                    QThread::msleep(50);
                }
                scenesPollingInfoList[i]->scenesSubThread->deleteLater();
                //解除信号和槽绑定
                disconnect(this,&SoapTransmit::sig_scenesPollingStart,scenesPollingInfoList[i]->scenesPollingThread,&ScenesPollingThread::deal_scenesPollingStart);
                disconnect(this,&SoapTransmit::sig_scenesPollingPause,scenesPollingInfoList[i]->scenesPollingThread,&ScenesPollingThread::deal_scenesPollingPause);
                disconnect(scenesPollingInfoList[i]->scenesPollingThread,&ScenesPollingThread::sig_scenesPollingCurrScene,this,&SoapTransmit::deal_scenesPollingCurrScene);
                disconnect(scenesPollingInfoList[i]->scenesPollingThread,&ScenesPollingThread::sig_scenesPollingIsPaused,this,&SoapTransmit::deal_scenesPollingIsPaused);
                //释放子线程对象
                delete scenesPollingInfoList[i]->scenesPollingThread;
                scenesPollingInfoList[i]->scenesPollingThread = nullptr;
                //从容器中删除该元素
                scenesPollingInfoList.removeAt(i);
            }
            //跳出
            break;
        }
    }
    (isFindSamePanel == true) ? ui->statusbar->showMessage(QString("%1 视频墙场景轮询以暂停").arg(QString::number(panel_sn)),statusBarShowTime)
                              : ui->statusbar->showMessage(QString("错误!未找到 %1 视频墙号！暂停轮询无效!").arg(QString::number(panel_sn)),statusBarShowTime_error);
}

//处理传输文件的状态参数回传
void SoapTransmit::deal_downloadFileSituation(int panel_sn,DownloadFileThread::DownloadFileState state)
{
    //查找对应的视频墙号
    bool isFindSamePanel = false;
    int i=0;
    for(;i<downloadFileInfoList.count();i++)
    {
        if(downloadFileInfoList.at(i)->downloadFilePanel == panel_sn)
        {
            //更改状态
            isFindSamePanel = true;
            downloadFileInfoList[i]->downloadFileState = state;
            //跳出
            break;
        }
    }
    if(isFindSamePanel == false)
    {
        transMsgBoxRemind(QString("错误!未找到对应的视频墙号 %1 !文件传输状态回传无效").arg(QString::number(panel_sn)),3000);
        return;
    }
    QString sitState;
    switch (state)
    {
    case DownloadFileThread::DownloadFileState::DownloadFileAbort:sitState = "传输文件终止";break;
    case DownloadFileThread::DownloadFileState::DownloadFileNotFound:sitState = "传输文件未找到";break;
    case DownloadFileThread::DownloadFileState::DownloadFileOpenFail:sitState = "传输文件打开失败";break;
    case DownloadFileThread::DownloadFileState::DownloadFileSendFail:sitState = "传输文件发送失败";break;
    case DownloadFileThread::DownloadFileState::DownloadFileRecvFail:sitState = "传输文件接收失败";break;
    case DownloadFileThread::DownloadFileState::DownloadFileConnectFail:sitState = "传输文件建立连接失败";break;
    case DownloadFileThread::DownloadFileState::DownloadFileSendDone:sitState = "传输文件发送成功";break;
    default:break;
    }
    //状态栏提示
    ui->statusbar->showMessage(sitState,statusBarShowTime_error);
    //终止对应视频墙号的线程对象
    while(downloadFileInfoList[i]->downloadFileSubThread->isRunning())
    {
        downloadFileInfoList[i]->downloadFileSubThread->quit();
        SoapPublicFunc::soapSleep(50);
    }
    downloadFileInfoList[i]->downloadFileSubThread->deleteLater();
    //解除信号和槽绑定
    disconnect(this,&SoapTransmit::sig_downloadFileStart,downloadFileInfoList[i]->downloadFileThread,&DownloadFileThread::dealDownloadFileStart);
    disconnect(this,&SoapTransmit::sig_downloadFileAbort,downloadFileInfoList[i]->downloadFileThread,&DownloadFileThread::dealDownloadFileAbort);
    disconnect(downloadFileInfoList[i]->downloadFileThread,&DownloadFileThread::sig_downloadSituation,this,&SoapTransmit::deal_downloadFileSituation);
    //释放子线程对象
    delete downloadFileInfoList[i]->downloadFileThread;
    downloadFileInfoList[i]->downloadFileThread = nullptr;
    //从容器中删除该元素
    downloadFileInfoList.removeAt(i);
}

//返回当前时间(格式:年.月.日-时:分)
QString SoapTransmit::currTime()
{
    return QDateTime::currentDateTime().toString("yyyy.MM.dd-HH:mm");
}

//返回当前日期
QString SoapTransmit::currDate()
{
    return QDateTime::currentDateTime().toString("yyyy.MM.dd");
}

//软件参数默认值
QJsonObject SoapTransmit::defSelfSetting()
{
    //接收日志对象
    QJsonObject recvLogSetting;
    recvLogSetting.insert("LogSaveSwitch",true);
    recvLogSetting.insert("LogUseLife",1);
    recvLogSetting.insert("LogUseLifeUnit",4);
    //发送日志对象
    QJsonObject sendLogSetting;
    sendLogSetting.insert("LogSaveSwitch",false);
    sendLogSetting.insert("LogUseLife",1);
    sendLogSetting.insert("LogUseLifeUnit",4);
    //udp监听端口参数对象
    QJsonObject udpListenPort;
    udpListenPort.insert("Port",10);
    //抓图参数对象
    QJsonObject snapParams;
    snapParams.insert("snap_w",320);
    snapParams.insert("snap_h",180);
    snapParams.insert("snap_len",1200);
    snapParams.insert("snap_delay",1000);
    snapParams.insert("snap_quality",50);
    snapParams.insert("mcast_enable",true);
    snapParams.insert("mcast_freq",20);
    snapParams.insert("snap_type","bypass");
    //软件参数默认值对象
    QJsonObject selfSettingObj;
    selfSettingObj.insert("RecvLogSetting",recvLogSetting);
    selfSettingObj.insert("SendLogSetting",sendLogSetting);
    selfSettingObj.insert("UdpListenPort",udpListenPort);
    selfSettingObj.insert("SnapParams",snapParams);
    return selfSettingObj;
}

//根据日志保存的参数删除文件
void SoapTransmit::recvLogFileAdjust()
{
    //获取所有日志文件的绝对路径(仅限日志文件夹下的.txt文件)
    QStringList logFileNames = SoapPublicFunc::getFileNames(QDir::currentPath() + "/" + scodeLogFolderName,QStringList()<<"*.txt");
    if(logFileNames.isEmpty() == false)
    {
        //从日志文件中区分是接收日志还是发送日志
        QStringList recvLogList,sendLogList;
        for(int i=0;i<logFileNames.count();i++)
        {
            if(logFileNames[i].contains("Recv"))
            {
                recvLogList.append(QDir::currentPath() + "/" + scodeLogFolderName + "/" + logFileNames[i]);
            }
            else if(logFileNames[i].contains("Send"))
            {
                sendLogList.append(QDir::currentPath() + "/" + scodeLogFolderName + "/" + logFileNames[i]);
            }
        }
        //删除在有效期外的日志文件(接收日志)
        if(myRecvLogInfo.recvLogSave == false)
        {
            for(int i=0;i<recvLogList.count();i++)
            {
                QFile recvLogFile(recvLogList.at(i));
                recvLogFile.remove();
            }
        }
        else
        {
            //确定有效期的大约天数
            quint32 usefulLife;
            switch(myRecvLogInfo.recvLogUseLifeUnit)
            {
            case Day:usefulLife = myRecvLogInfo.recvLogUseLife;break;
            case Month:usefulLife = myRecvLogInfo.recvLogUseLife * 30;break;
            case Year:usefulLife = myRecvLogInfo.recvLogUseLife * 365;break;
            case Eternal:usefulLife = 36500;break;
            default:return;
            }
            //取出当前时间和文件的创建时间(精确到天)以及两个日期相差的天数
            qint64 daysDiff;
            for(int i=0;i<recvLogList.count();i++)
            {
                QFileInfo recvLogFile(recvLogList.at(i));
                daysDiff = QDateTime::currentDateTime().daysTo(recvLogFile.birthTime());
                if(qAbs(daysDiff) > usefulLife)
                {
                    QFile file(recvLogList.at(i));
                    file.remove();
                }
            }
        }
        //删除在有效期外的日志文件(发送日志)
        if(mySendLogInfo.sendLogSave == false)
        {
            for(int i=0;i<sendLogList.count();i++)
            {
                QFile sendLogFile(sendLogList.at(i));
                sendLogFile.remove();
            }
        }
        else
        {
            quint32 usefulLife;
            switch(mySendLogInfo.sendLogUseLifeUnit)
            {
            case Day:usefulLife = mySendLogInfo.sendLogUseLife;break;
            case Month:usefulLife = mySendLogInfo.sendLogUseLife * 30;break;
            case Year:usefulLife = mySendLogInfo.sendLogUseLife * 365;break;
            case Eternal:usefulLife = 36500;break;
            default:return;
            }
            //取出当前时间和文件的创建时间(精确到天)以及两个日期相差的天数
            qint64 daysDiff;
            for(int i=0;i<sendLogList.count();i++)
            {
                QFileInfo sendLogFile(sendLogList.at(i));
                daysDiff = QDateTime::currentDateTime().daysTo(sendLogFile.birthTime());
                if(qAbs(daysDiff) > usefulLife)
                {
                    QFile file(sendLogList.at(i));
                    file.remove();
                }
            }
        }
    }
}

//保存软件参数
void SoapTransmit::saveTransmitParams()
{
    //打开软件参数文件
    QFile selfSettingFile(QDir::currentPath() + "/" + scodeDataFolderName + "/" + transmitDataFileName);
    selfSettingFile.open(QFile::ReadWrite);
    QByteArray settingData = selfSettingFile.readAll();
    QJsonObject selfSettingObj;
    (settingData.isEmpty()) ? selfSettingObj = defSelfSetting()
                            : selfSettingObj = SoapPublicFunc::QStringToJsonObj(QString(settingData));
    //重写文件
    selfSettingFile.resize(0);
    selfSettingFile.write(SoapPublicFunc::jsonObjToQstring(selfSettingObj).toUtf8());
    selfSettingFile.close();
}

//主窗口关闭函数(重写)
void SoapTransmit::closeEvent(QCloseEvent *event)
{
    //关闭时询问是否确认退出
    QString checkStr = "你确定要退出该程序吗?";
    if(scenesPollingInfoList.count() || downloadFileInfoList.count())
    {
        checkStr.append("\n这会终止正在轮训的场景或传输文件");
    }
    QMessageBox *box = new QMessageBox(QMessageBox::Information,"离开",checkStr,QMessageBox::Yes | QMessageBox::No,this);
    box->setFont(QFont("微软雅黑",15));
    box->setButtonText(QMessageBox::Yes,"确定");
    box->setButtonText(QMessageBox::No,"取消");
    box->setDefaultButton(QMessageBox::Yes);
    box->setWindowIcon(QIcon(":/Transmit/leave_Black.png"));
    box->setAttribute(Qt::WA_DeleteOnClose);
    //确定按钮则允许事件继续,否则忽略事件
    (box->exec() == QMessageBox::Yes) ? event->accept() : event->ignore();
}

//命令列表单击槽
void SoapTransmit::on_listWidget_orderList_itemClicked(QListWidgetItem *item)
{
    if(item == nullptr)
    {
        return;
    }
    QString parentPath;
    //判断当前的筛选方式,判断文件所属的文件夹
    FileSortWay currWay = getFileSortWay();
    switch(currWay)
    {
    case SoapTransmit::FileSortWay::ScodeDataFolder:parentPath = QDir::currentPath() + "/" + scodeDataFolderName;break;
    case SoapTransmit::FileSortWay::ScodeListFolder:parentPath = QDir::currentPath() + "/" + scodeListFolderName;break;
    case SoapTransmit::FileSortWay::ScodeLogFolder:parentPath = QDir::currentPath() + "/" + scodeLogFolderName;break;
    case SoapTransmit::FileSortWay::ScodeResFolder:parentPath = QDir::currentPath() + "/" + scodeResourceFolderName;break;
    default:return;
    }
    QString filePath = parentPath + "/" + item->text();

    //清空预览区
    ui->textEdit_orderView->clear();

    //发送文件的绝对路径,在线程中读取文件
    if(fileReadSubThread->isRunning() == true)
    {
        emit sig_stopFileRead();
        fileReadSubThread->quit();
        fileReadSubThread->wait();
    }
    else
    {
        fileReadSubThread->start();
    }
    emit sig_startFileRead(filePath);
    ui->statusbar->showMessage(QString("读取 %1 文件...").arg(item->text()),10000);
}

//处理读取文件的内容回传
void SoapTransmit::deal_fileReadContent(QByteArray content)
{
    //在主界面的预览区显示选定文件的内容
    ui->textEdit_orderView->append(QString(content));
}

//处理读取文件的状态回传
void SoapTransmit::deal_fileReadState(FileReadThread::fileState state,QString fileName)
{
    QString str;
    switch (state)
    {
    case FileReadThread::fileState::fileOpenFail:str = "打开失败";break;
    case FileReadThread::fileState::fileTooLarge:str = "文件过大,不支持预览";break;
    case FileReadThread::fileState::fileTypeUnsupported:str = "不支持的文件类型";break;
    case FileReadThread::fileState::fileReadAbort:str = "读取终止";break;
    case FileReadThread::fileState::fileReadDone:str = "读取完毕";break;
    default:return;
    }
    //提示
    ui->statusbar->showMessage(fileName + " " + str,statusBarShowTime);
    //终止子线程
    if(fileReadSubThread->isRunning())
    {
        fileReadSubThread->quit();
        fileReadSubThread->wait();
    }
}

//处理发送套接字定时触发槽
void SoapTransmit::deal_sendMsgTimeOut()
{
    //检查nodeIpList是否为空
    if(nodeIpList.isEmpty())
    {
        ui->statusbar->showMessage("警告!未扫描到任何转发节点IP",statusBarShowTime_error);
    }
    else
    {
        //处理并覆盖原来的转发节点数据
        SoapPublicFunc::stringListRemoveDuplicates(&nodeIpList);
        //将数据写入内存
        nodeListInfoList.clear();
        QByteArray nodeData;
        for(int i=0;i<nodeIpList.count();i++)
        {
            nodeListInfo *nodeInfo = new nodeListInfo;
            nodeInfo->nodeListIp = nodeIpList.at(i);
            nodeInfo->nodeListTransmitSigLim = 9;
            nodeInfo->currNodeState = NodeState::InitState;
            nodeListInfoList.append(nodeInfo);
            //生成要写入文件的数据
            if(i<nodeIpList.count()-1)
            {
                nodeData.append(nodeIpList.at(i).toUtf8() + "," + QByteArray::number(9) + ";\n");
            }
        }
        //写入文件
        QFile nodeFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + nodeListFileName);
        nodeFile.open(QFile::ReadWrite);
        nodeFile.resize(0);
        nodeFile.write(nodeData);
        nodeFile.close();
        //提示
        ui->statusbar->showMessage("扫描到转发节点IP",statusBarShowTime);
    }

    //对象使能(扫描转发节点项)
    ui->treeWidget_opeSet->itemAt(0,0)->child(0)->setText(0,"扫描转发节点");
}

//命令列表右键菜单
void SoapTransmit::on_listWidget_orderList_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->listWidget_orderList->indexAt(pos);
    //主菜单
    QMenu *mainMenu = new QMenu(this);
    mainMenu->setAttribute(Qt::WA_DeleteOnClose);
    mainMenu->setShortcutEnabled(true);
    mainMenu->setMinimumWidth(150);
    //判断点击位置是对象还是空白
    if(index.row() == -1 || index.column() == -1)
    {
        //二级菜单
        QMenu *sortMenu = new QMenu(mainMenu);
        sortMenu->addAction(QString("scodeData文件夹"),this,&SoapTransmit::sort_scodeData)->setFont(bodyFont);
        sortMenu->addAction(QString("scodeList文件夹"),this,&SoapTransmit::sort_scodeList)->setFont(bodyFont);
        sortMenu->addAction(QString("scodeLog文件夹"),this,&SoapTransmit::sort_scodeLog)->setFont(bodyFont);
        sortMenu->addAction(QString("scodeRes文件夹"),this,&SoapTransmit::sort_scodeRes)->setFont(bodyFont);
        QAction *sortAct = mainMenu->addAction(sortIcon,"筛选文件夹");
        sortAct->setMenu(sortMenu);
        sortAct->setFont(bodyFont);

    }
    else
    {
        mainMenu->addAction(QIcon(":/Transmit/folder_Blue.png"),QString("打开文件所在位置"),this,&SoapTransmit::openOrderFileDir,QKeySequence(Qt::Key_O))->setFont(bodyFont);
        QShortcut *openOrderFileDir_Sc = new QShortcut(QKeySequence(Qt::Key_O),mainMenu,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
        connect(openOrderFileDir_Sc,&QShortcut::activated,this,&SoapTransmit::openOrderFileDir);
        mainMenu->addSeparator();

        mainMenu->addAction(QIcon(":/Transmit/examine_Blue.png"),QString("查看内容"),this,&SoapTransmit::examineOrderFile,QKeySequence(Qt::Key_V))->setFont(bodyFont);
        QShortcut *examineFile_Sc = new QShortcut(QKeySequence(Qt::Key_V),mainMenu,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
        connect(examineFile_Sc,&QShortcut::activated,this,&SoapTransmit::examineOrderFile);

        mainMenu->addAction(QIcon(":/Transmit/clear_Blue.png"),QString("删除文件"),this,&SoapTransmit::deleteFile,QKeySequence(Qt::Key_C))->setFont(bodyFont);
        QShortcut *deleteFile_Sc = new QShortcut(QKeySequence(Qt::Key_C),mainMenu,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
        connect(deleteFile_Sc,&QShortcut::activated,this,&SoapTransmit::deleteFile);
    }
    //在鼠标位置显示右键菜单
    mainMenu->move(QCursor::pos());
    mainMenu->show();
}

//筛选文件夹(scodeList)
void SoapTransmit::sort_scodeList()
{
    //清空预览区
    ui->textEdit_orderView->clear();
    //根据文件夹路径筛选
    QString scodeListDir = QDir::currentPath() + "/" + scodeListFolderName;
    if(SoapPublicFunc::isFilePathExist(scodeListDir) == false)
    {
        ui->statusbar->showMessage("警告!scodeList 文件夹不存在!",statusBarShowTime_error);
        return;
    }
    //刷新列表
    ui->listWidget_orderList->clear();
    QStringList fileNameList = SoapPublicFunc::getFileNames(scodeListDir,QStringList()<<"*.txt"<<"*.json");
    for(int i=0;i<fileNameList.count();i++)
    {
        QFileInfo info(fileNameList[i]);
        QListWidgetItem *item = new QListWidgetItem(txtIcon,info.fileName());
        item->setSizeHint(QSize(ui->listWidget_orderList->width()-5,40));
        item->setToolTip(item->text());
        ui->listWidget_orderList->addItem(item);
    }
    setFileSortWay(FileSortWay::ScodeListFolder);
}

//筛选文件夹(scodeData)
void SoapTransmit::sort_scodeData()
{
    //清空预览区
    ui->textEdit_orderView->clear();
    //根据文件夹路径筛选
    QString scodeDataDir = QDir::currentPath() + "/" + scodeDataFolderName;
    if(SoapPublicFunc::isFilePathExist(scodeDataDir) == false)
    {
        ui->statusbar->showMessage("警告!scodeData 文件夹不存在!",statusBarShowTime_error);
        return;
    }
    //刷新列表
    ui->listWidget_orderList->clear();
    QStringList fileNameList = SoapPublicFunc::getFileNames(scodeDataDir,QStringList()<<"*.json");
    for(int i=0;i<fileNameList.count();i++)
    {
        QFileInfo info(fileNameList[i]);
        QListWidgetItem *item = new QListWidgetItem(txtIcon,info.fileName());
        item->setSizeHint(QSize(ui->listWidget_orderList->width()-5,40));
        item->setToolTip(item->text());
        ui->listWidget_orderList->addItem(item);
    }
    setFileSortWay(FileSortWay::ScodeDataFolder);
}

//筛选文件夹(scodeLog)
void SoapTransmit::sort_scodeLog()
{
    //清空预览区
    ui->textEdit_orderView->clear();
    //根据路径筛选
    QString scodeLogDir = QDir::currentPath() + "/" + scodeLogFolderName;
    if(SoapPublicFunc::isFilePathExist(scodeLogDir) == false)
    {
        ui->statusbar->showMessage("警告!scodeLog 文件夹不存在!",statusBarShowTime_error);
        return;
    }
    ui->listWidget_orderList->clear();
    QStringList fileNameList = SoapPublicFunc::getFileNames(scodeLogDir,QStringList()<<"*.txt");
    for(int i=0;i<fileNameList.count();i++)
    {
        QFileInfo info(fileNameList[i]);
        QListWidgetItem *item = new QListWidgetItem(txtIcon,info.fileName());
        item->setSizeHint(QSize(ui->listWidget_orderList->width()-5,40));
        item->setToolTip(item->text());
        ui->listWidget_orderList->addItem(item);
    }
    setFileSortWay(FileSortWay::ScodeLogFolder);
}

//筛选文件夹(scodeRes)(用来存放资源文件)
void SoapTransmit::sort_scodeRes()
{
    ui->textEdit_orderView->clear();
    QString scodeResDir = QDir::currentPath() + "/" + scodeResourceFolderName;
    if(SoapPublicFunc::isFilePathExist(scodeResDir) == false)
    {
        ui->statusbar->showMessage("警告!scodeRes 文件夹不存在!",statusBarShowTime_error);
        return;
    }
    ui->listWidget_orderList->clear();
    QStringList fileNameList = SoapPublicFunc::getFileNames(scodeResDir,QStringList()<<"*");
    for(int i=0;i<fileNameList.count();i++)
    {
        QFileInfo info(fileNameList[i]);
        QListWidgetItem *item = new QListWidgetItem(txtIcon,info.fileName());
        item->setSizeHint(QSize(ui->listWidget_orderList->width()-5,40));
        item->setToolTip(item->text());
        ui->listWidget_orderList->addItem(item);
    }
    setFileSortWay(FileSortWay::ScodeResFolder);
}

//处理FileListDisplay的信号回传
void SoapTransmit::deal_infoListSendBack(QList<QStringList> content,FileListDisplay::InfoType type)
{
    //若数据为空直接跳出
    if(content.isEmpty())
    {
        return;
    }
    //根据编辑区的类型处理
    switch (type)
    {
    case FileListDisplay::InfoType::IdType:
    {
        //重置信号源表链表
        idListInfoList.clear();
        //将内容写入自定义容器
        for(int i=0;i<content.count();i++)
        {
            idListInfo *info = new idListInfo;
            for(int j=0;j<4;j++)
            {
                switch (j)
                {
                case 0:info->idListIp = content.at(i).at(j);break;
                case 1:info->idListName = content.at(i).at(j);break;
                case 2:info->idListMajorStream = content.at(i).at(j);break;
                case 3:info->idListMinorStream = content.at(i).at(j);break;
                }
            }
            idListInfoList.append(info);
        }
        //将数据覆盖文件
        QByteArray idListData;
        for(int i=0;i<idListInfoList.count();i++)
        {
            idListData.append(idListInfoList.at(i)->idListIp + "," + idListInfoList.at(i)->idListName + "," + idListInfoList.at(i)->idListMajorStream + "," + idListInfoList.at(i)->idListMinorStream + ";\n");
        }
        idListData.remove(idListData.count()-2,2);
        QFile idListFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + idListFileName);
        idListFile.open(QFile::ReadWrite);
        idListFile.resize(0);
        idListFile.write(idListData);
        idListFile.close();
    }
        break;
    case FileListDisplay::InfoType::NodeType:
    {
        //重置转发节点链表
        nodeListInfoList.clear();
        //将内容写入自定义容器
        for(int i=0;i<content.count();i++)
        {
            nodeListInfo *info = new nodeListInfo;
            for(int j=0;j<2;j++)
            {
                switch (j)
                {
                case 0:info->nodeListIp = content.at(i).at(j);break;
                case 1:info->nodeListTransmitSigLim = content.at(i).at(j).toInt();break;
                }
            }
            nodeListInfoList.append(info);
        }
        //将数据覆盖文件
        QByteArray nodeListData;
        for(int i=0;i<nodeListInfoList.count();i++)
        {
            nodeListData.append(nodeListInfoList.at(i)->nodeListIp + "," + QString::number(nodeListInfoList.at(i)->nodeListTransmitSigLim) + ";\n");
        }
        nodeListData.remove(nodeListData.count()-2,2);
        QFile nodeListFile(QDir::currentPath() + "/" + scodeListFolderName + "/" + nodeListFileName);
        nodeListFile.open(QFile::ReadWrite);
        nodeListFile.resize(0);
        nodeListFile.write(nodeListData);
        nodeListFile.close();
    }
        break;
    default:return;
    }
    //刷新编辑区
    on_listWidget_orderList_itemClicked(ui->listWidget_orderList->currentItem());
    //关闭编辑区窗口
    FileListDisplay *editWindow = this->findChild<FileListDisplay *>(QString("FileListDisplay"));
    if(editWindow != nullptr)
    {
        editWindow->close();
        ui->statusbar->showMessage("设置已保存",statusBarShowTime);
    }
    else
    {
        ui->statusbar->showMessage("错误!未找到编辑窗口!",statusBarShowTime_error);
    }
}

//处理LogDialog的信号回传
void SoapTransmit::deal_logContentSendBack(bool recvLogSave,QList<int> recvLogList,bool sendLogSave,QList<int> sendLogList)
{
    //检查格式
    if(recvLogList.count() != 2 || sendLogList.count() != 2)
    {
        ui->statusbar->showMessage("警告!日志设置回传参数错误!",statusBarShowTime_error);
        return;
    }

    /******************************接收并保存回传的参数******************************/
    //保存接收日志参数
    if(recvLogSave == false)
    {
        myRecvLogInfo.recvLogSave = false;
        myRecvLogInfo.recvLog.clear();
        myRecvLogInfo.recvLogErr.clear();
        myRecvLogInfo.recvLogUseLife = 1;
        myRecvLogInfo.recvLogUseLifeUnit = TimeUnit::Month;
    }
    else
    {
        myRecvLogInfo.recvLogSave = true;
        myRecvLogInfo.recvLog.clear();
        myRecvLogInfo.recvLogErr.clear();
        myRecvLogInfo.recvLogUseLife = recvLogList.at(0);
        myRecvLogInfo.recvLogUseLifeUnit = recvLogList.at(1);
    }
    //保存发送日志参数
    if(sendLogSave == false)
    {
        mySendLogInfo.sendLogSave = false;
        mySendLogInfo.sendLog.clear();
        mySendLogInfo.sendLogUseLife = 1;
        mySendLogInfo.sendLogUseLifeUnit = TimeUnit::Month;
    }
    else
    {
        mySendLogInfo.sendLogSave = true;
        mySendLogInfo.sendLogUseLife  = sendLogList.at(0);
        mySendLogInfo.sendLogUseLifeUnit = sendLogList.at(1);
    }

    //打开参数保存文件并覆盖设置
    QFile selfSettingFile(QDir::currentPath() + "/" + scodeDataFolderName + "/" + transmitDataFileName);
    selfSettingFile.open(QFile::ReadWrite);
    QByteArray settingData = selfSettingFile.readAll();
    QJsonObject selfSettingObj;
    (settingData.isEmpty()) ? selfSettingObj = defSelfSetting()
                            : selfSettingObj = SoapPublicFunc::QStringToJsonObj(QString(settingData));
    //通过引用修改Json的键值对并修改对应的参数
    QJsonValueRef refRecvLogSetting = selfSettingObj.find("RecvLogSetting").value();
    QJsonObject recvLogSettingObj = refRecvLogSetting.toObject();
    recvLogSettingObj["LogSaveSwitch"] = myRecvLogInfo.recvLogSave;
    recvLogSettingObj["LogUseLife"] = myRecvLogInfo.recvLogUseLife;
    recvLogSettingObj["LogUseLifeUnit"] = myRecvLogInfo.recvLogUseLifeUnit;
    refRecvLogSetting = recvLogSettingObj;
    QJsonValueRef refSendLogSetting = selfSettingObj.find("SendLogSetting").value();
    QJsonObject sendLogSettingObj = refSendLogSetting.toObject();
    sendLogSettingObj["LogSaveSwitch"] = mySendLogInfo.sendLogSave;
    sendLogSettingObj["LogUseLife"] = mySendLogInfo.sendLogUseLife;
    sendLogSettingObj["LogUseLifeUnit"] = mySendLogInfo.sendLogUseLifeUnit;
    refSendLogSetting = sendLogSettingObj;
    settingData = SoapPublicFunc::jsonObjToQstring(selfSettingObj).toUtf8();
    selfSettingFile.resize(0);
    selfSettingFile.write(settingData);
    selfSettingFile.close();
    //提示
    ui->statusbar->showMessage("日志设置已保存",statusBarShowTime);
}

//处理SnapShot的信号回传
void SoapTransmit::deal_snapContentSendBack(int snapW,int snapH,int snapL,int snapDelay,int snapQua,bool mcastEnable,int mcastFre,QString snapType)
{
    //保存参数(内存和文件)
    QFile selfSettingFile(QDir::currentPath() + "/" + scodeDataFolderName + "/" + transmitDataFileName);
    selfSettingFile.open(QFile::ReadWrite);
    QByteArray selfSetting = selfSettingFile.readAll();
    QJsonObject selfSettingObj = SoapPublicFunc::QStringToJsonObj(QString(selfSetting));
    QJsonValueRef refSnapParams = selfSettingObj.find("SnapParams").value();
    QJsonObject snapParamsObj = refSnapParams.toObject();
    snapParamsObj["snap_w"] = snapParam.snap_w = snapW;
    snapParamsObj["snap_h"] = snapParam.snap_h = snapH;
    snapParamsObj["snap_len"] = snapParam.snap_len = snapL;
    snapParamsObj["snap_delay"] = snapParam.snap_delay = snapDelay;
    snapParamsObj["snap_quality"] = snapParam.snap_quality = snapQua;
    snapParamsObj["snap_freq"] = snapParam.mcast_freq = mcastFre;
    snapParamsObj["mcast_enable"] = snapParam.mcast_enable = mcastEnable;
    snapParamsObj["snap_type"] = snapParam.snap_type = snapType;
    refSnapParams = snapParamsObj;
    selfSetting = SoapPublicFunc::jsonObjToQstring(selfSettingObj).toUtf8();
    selfSettingFile.resize(0);
    selfSettingFile.write(selfSetting);
    selfSettingFile.close();
    //提示
    ui->statusbar->showMessage("抓图设置已保存",statusBarShowTime);
}

//请求转发节点IP列表
bool SoapTransmit::requireNodeInfo()
{
    bool isSend;
    //清空临时的转发节点IP列表
    nodeIpList.clear();
    //广播扫描转发节点IP
    QByteArray scanNode = "FIND_NODE_UNI";
    quint64 len = udpSendSock->writeDatagram(scanNode,scanNode.length(),QHostAddress("239.255.255.250"),3702);
    (len <= 0) ? isSend = false : isSend = true;
    return isSend;
}

//运行信息表 对象单击槽
void SoapTransmit::on_listWidget_runInfo_itemClicked(QListWidgetItem *item)
{
    if(item != nullptr)
    {
        QString itemName = item->text();
        FileType infoType;
        if(itemName == "信号源信息")
        {
            infoType = FileType::IdType;
        }
        else if(itemName == "转发节点信息")
        {
            infoType = FileType::NodeType;
        }
        else if(itemName == "视频墙信息")
        {
            infoType = FileType::WindowsType;
        }
        else if(itemName == "场景轮询信息")
        {
            infoType = FileType::ScenesPollingType;
        }
        else
        {
            infoType = FileType::OthersType;
            return;
        }
        //若窗口不存在则创建一个,若存在则对该窗口重新设置
        fileListDisplayWindow = this->findChild<FileListDisplay *>(QString("FileListDisplay"));
        if(fileListDisplayWindow == nullptr)
        {
            fileListDisplayWindow = new FileListDisplay(this);
            //设置并显示非模态窗口
            fileListDisplayWindow->setObjectName("FileListDisplay");
            ui->layout_runInfo->addWidget(fileListDisplayWindow);
            fileListDisplayWindow->show();
            //绑定信号和槽
            connect(fileListDisplayWindow,&FileListDisplay::sig_infoListSendBack,this,&SoapTransmit::deal_infoListSendBack);
        }
        //根据类型生成表格并显示
        QList<QStringList> infoList;
        switch (infoType)
        {
        case FileType::IdType:
        {
            for(int i=0;i<idListInfoList.count();i++)
            {
                infoList.append(QStringList()<<idListInfoList.at(i)->idListIp<<idListInfoList.at(i)->idListName<<idListInfoList.at(i)->idListMajorStream<<idListInfoList.at(i)->idListMinorStream);
            }
            //设置内容
            fileListDisplayWindow->loadContentIntoList(infoList,QStringList()<<"信号源IP"<<"名称"<<"主码流"<<"辅码流",FileListDisplay::InfoType::IdType,true);
        }
            break;
        case FileType::NodeType:
        {
            for(int i=0;i<nodeListInfoList.count();i++)
            {
                infoList.append(QStringList()<<nodeListInfoList.at(i)->nodeListIp<<QString::number(nodeListInfoList.at(i)->nodeListTransmitSigLim)<<nodeListInfoList.at(i)->transmitedIpList.join("\n"));
            }
            fileListDisplayWindow->loadContentIntoList(infoList,QStringList()<<"转发节点IP"<<"可分配信号数"<<"转发的IP",FileListDisplay::InfoType::NodeType,true);
        }
            break;
        case FileType::WindowsType:
        {
            for(int i=0;i<windowsInfoList.count();i++)
            {
                infoList.append(QStringList()<<QString::number(windowsInfoList.at(i)->windowsNum)<<windowsInfoList.at(i)->transmitedIpList.join("\n"));
            }
            fileListDisplayWindow->loadContentIntoList(infoList,QStringList()<<"开启的视频墙号"<<"转发的IP",FileListDisplay::InfoType::WindowType,false);
        }
            break;
        case FileType::ScenesPollingType:
        {
            for(int i=0;i<scenesPollingInfoList.count();i++)
            {
                infoList.append(QStringList()<<QString::number(scenesPollingInfoList.at(i)->scenesPollingPanel)<<QString::number(scenesPollingInfoList.at(i)->scenesPollingTime));
            }
            fileListDisplayWindow->loadContentIntoList(infoList,QStringList()<<"轮训的视频墙"<<"轮询间隔(S)",FileListDisplay::InfoType::ScenesPollingType,false);
        }
            break;
        default:return;
        }
    }
}

//树形控件对象单击槽
void SoapTransmit::on_treeWidget_opeSet_itemClicked(QTreeWidgetItem *item, int column)
{
    //对象为空直接返回
    if(item == nullptr)
    {
        return;
    }
    //取出选定对象的名称并做出相应的处理
    QString name = item->text(column);
    if(name == "设置" || name == "操作")
    {
        //若该节点存在子项且存在处于收缩状态,将其展开,否则收缩
        if(item->childCount()!=0)
        {
            (item->isExpanded() == false) ? item->setExpanded(true) : item->setExpanded(false);
        }
    }
    else if (name == "扫描转发节点")
    {
        bool isSend = requireNodeInfo();
        if(isSend)
        {
            //启动定时器
            sendMsgTimer->start(1000);
            //重设文本内容
            item->setText(0,"扫描中...");
        }
        else
        {
            ui->statusbar->showMessage("警告!扫描转发节点失败!",statusBarShowTime_error);
        }
    }
    else if(name == "加载本地表")
    {
        item->setText(column,"加载中...");
        setIsIdAndNodeListCorrect(reloadIdAndNodeListData());
        (getIsIdAndNodeListCorrect()) ? ui->statusbar->showMessage("重载本地双表成功",statusBarShowTime)
                                      : ui->statusbar->showMessage("重载双表失败,文件数据为空或不符合格式",statusBarShowTime_error);
        item->setText(column,name);
    }
    else if(name == "日志设置")
    {
        //关闭可能存在的抓图设置子窗口
        SnapShot *snapWindow = this->findChild<SnapShot *>(QString("SnapShot"));
        if(snapWindow != nullptr)
        {
            snapWindow->close();
        }
        //若窗口不存在,创建并设置内容,否则跳出
        LogDialog *logWindow = this->findChild<LogDialog *>(QString("LogDialog"));
        if(logWindow == nullptr)
        {
            //创建并设置名称
            logWindow = new LogDialog(this);
            logWindow->setObjectName("LogDialog");
            ui->layout_opeSet->addWidget(logWindow);
            //显示子窗口
            logWindow->show();
            //绑定信号和槽
            connect(logWindow,&LogDialog::sig_logContentSendBack,this,&SoapTransmit::deal_logContentSendBack);
            //加载内容
            logWindow->loadContent(myRecvLogInfo.recvLogSave,QList<int>()<<myRecvLogInfo.recvLogUseLife<<myRecvLogInfo.recvLogUseLifeUnit,
                                   mySendLogInfo.sendLogSave,QList<int>()<<mySendLogInfo.sendLogUseLife<<mySendLogInfo.sendLogUseLifeUnit);
        }
    }
    else if(name == "抓图设置")
    {
        //关闭可能存在的日志设置子窗口
        LogDialog *logWindow = this->findChild<LogDialog *>(QString("LogDialog"));
        if(logWindow != nullptr)
        {
            logWindow->close();
        }
        //若窗口不存在,创建并设置内容,否则跳出
        SnapShot *snapWindow = this->findChild<SnapShot *>(QString("SnapShot"));
        if(snapWindow == nullptr)
        {
            //创建并设置名称
            snapWindow = new SnapShot(this);
            snapWindow->setObjectName("SnapShot");
            ui->layout_opeSet->addWidget(snapWindow);
            //显示子窗口
            snapWindow->show();
            //绑定信号和槽
            connect(snapWindow,&SnapShot::sig_snapContentSendBack,this,&SoapTransmit::deal_snapContentSendBack);
            //加载内容
            snapWindow->loadContent(snapParam.snap_w,snapParam.snap_h,snapParam.snap_len,snapParam.snap_delay,snapParam.snap_quality,snapParam.mcast_enable,snapParam.mcast_freq,snapParam.snap_type);
        }
    }
    else
    {}
}
