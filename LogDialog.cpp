#include "LogDialog.h"
#include "ui_LogDialog.h"

//构造
LogDialog::LogDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogDialog)
{
    ui->setupUi(this);

    /******************************本窗体的基础设置******************************/
    this->setAttribute(Qt::WA_DeleteOnClose);                                                       //设置在关闭时释放
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::Dialog);           //窗口相关设置
    this->hide();

    //若窗体父对象不为空,根据父对象的大小设置大小,若为空则还需要额外设置样式表
    if(parent != nullptr)
    {
        this->resize(parent->width(),parent->height());
    }
    else
    {
        setMinimumSize(400,200);
        //加载样式表
        QFile styleSheetFile(":/qss/qss/blue.css");
        styleSheetFile.open(QFile::ReadOnly);
        setStyleSheet(styleSheetFile.readAll());
        styleSheetFile.close();
    }

    //控件初始化
    ui->box_recvLogSave->addItems(QStringList()<<"开"<<"关");
    ui->box_recvLogUseLifeUnit->addItems(QStringList()<<"天"<<"月"<<"年"<<"永远");
    ui->box_sendLogSave->addItems(QStringList()<<"开"<<"关");
    ui->box_sendLogUseLifeUnit->addItems(QStringList()<<"天"<<"月"<<"年"<<"永远");
}

//析构
LogDialog::~LogDialog()
{
    delete ui;
}

//加载内容
void LogDialog::loadContent(bool recvLogSave,QList<int> recvLogList,bool sendLogSave,QList<int> sendLogList)
{
    //根据数据设置初始值
    (recvLogSave == true) ? ui->box_recvLogSave->setCurrentIndex(0) : ui->box_recvLogSave->setCurrentIndex(1);
    ui->box_recvLogUseLifeUnit->setCurrentIndex(recvLogList.at(1)-3);
    ui->sli_recvLogUseLife->setValue(recvLogList[0]);
    (sendLogSave == true) ? ui->box_sendLogSave->setCurrentIndex(0) : ui->box_sendLogSave->setCurrentIndex(1);
    ui->box_sendLogUseLifeUnit->setCurrentIndex(sendLogList.at(1)-3);
    ui->sli_sendLogUseLife->setValue(sendLogList.at(0));
}

//接收日志保存下拉框槽
void LogDialog::on_box_recvLogSave_currentIndexChanged(int index)
{
    //索引0开启日志保存,1关闭
    switch (index)
    {
    case 0:
    {
        ui->sli_recvLogUseLife->setEnabled(true);
        ui->box_recvLogUseLifeUnit->setEnabled(true);
    }
        break;
    case 1:
    {
        ui->sli_recvLogUseLife->setDisabled(true);
        ui->box_recvLogUseLifeUnit->setDisabled(true);
    }
    }
}

//发送日志保存下拉框槽
void LogDialog::on_box_sendLogSave_currentIndexChanged(int index)
{
    //索引0开启日志保存,1关闭
    switch (index)
    {
    case 0:
    {
        ui->sli_sendLogUseLife->setEnabled(true);
        ui->box_sendLogUseLifeUnit->setEnabled(true);
    }
        break;
    case 1:
    {
        ui->sli_sendLogUseLife->setDisabled(true);
        ui->box_sendLogUseLifeUnit->setDisabled(true);
    }
    }
}

//接收日志滑块数值改变槽
void LogDialog::on_sli_recvLogUseLife_valueChanged(int value)
{
    ui->lab_recvLogUseLifeValue->setText(QString::number(value));
}

//发送日志滑块数值改变槽
void LogDialog::on_sli_sendLogUseLife_valueChanged(int value)
{
    ui->lab_sendLogUseLifeValue->setText(QString::number(value));
}

//接收日志有效期单位下拉框 索引改变槽
void LogDialog::on_box_recvLogUseLifeUnit_currentIndexChanged(int index)
{
    //根据有效期单位设置滑块范围
    switch (index)
    {
    case 0:ui->sli_recvLogUseLife->setRange(1,30);break;
    case 1:ui->sli_recvLogUseLife->setRange(1,12);break;
    case 2:ui->sli_recvLogUseLife->setRange(1,10);break;
    case 3:ui->sli_recvLogUseLife->setRange(1,1);
    }
    ui->sli_recvLogUseLife->setValue(1);
}

//发送日志有效期单位下拉框 索引改变槽
void LogDialog::on_box_sendLogUseLifeUnit_currentIndexChanged(int index)
{
    //根据有效期单位设置滑块范围
    switch (index)
    {
    case 0:ui->sli_sendLogUseLife->setRange(1,30);break;
    case 1:ui->sli_sendLogUseLife->setRange(1,12);break;
    case 2:ui->sli_sendLogUseLife->setRange(1,10);break;
    case 3:ui->sli_sendLogUseLife->setRange(1,1);
    }
    ui->sli_sendLogUseLife->setValue(1);
}

//确定按钮 点击槽
void LogDialog::on_yesBtn_clicked()
{
    //提取出参数并回传
    bool recvLogSave,sendLogSave;
    QList<int> recvLogList,sendLogList;
    (ui->box_recvLogSave->currentIndex() == 0) ? recvLogSave = true : recvLogSave = false;
    recvLogList.append(ui->sli_recvLogUseLife->value());
    recvLogList.append(ui->box_recvLogUseLifeUnit->currentIndex() + 3);
    (ui->box_sendLogSave->currentIndex() == 0) ? sendLogSave = true : sendLogSave = false;
    sendLogList.append(ui->sli_sendLogUseLife->value());
    sendLogList.append(ui->box_recvLogUseLifeUnit->currentIndex() + 3);
    //发送信号
    emit sig_logContentSendBack(recvLogSave,recvLogList,sendLogSave,sendLogList);
}

//取消按钮 点击槽
void LogDialog::on_cancelBtn_clicked()
{
    this->close();
}
