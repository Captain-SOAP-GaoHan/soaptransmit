#ifndef SCENESPOLLINGTHREAD_H
#define SCENESPOLLINGTHREAD_H

#include <QObject>

#include "SoapPublicFunc.h"

//场景轮询子线程类

class ScenesPollingThread : public QObject
{
    Q_OBJECT
public:
    explicit ScenesPollingThread(QObject *parent = nullptr,int panel_sn = 0);

    //处理场景轮询开始信号
    void deal_scenesPollingStart(int panel_sn,QStringList setWindowsList,int time);

    //处理场景轮询暂停信号
    void deal_scenesPollingPause(int panel_sn);

signals:
    //发送当前场景对应的SetWindows命令
    void sig_scenesPollingCurrScene(QString setWindows);

    //发送已经暂停的信号
    void sig_scenesPollingIsPaused(int panel_sn);


private:
    bool scenesPollingIsPause;                                  //标识场景轮询是否暂停
    int panel_sn;                                               //当前轮询的场景对应的视频墙号
    QStringList setWindowsList;                                 //当前场景轮询的SetWindows列表
    int scenesPollingTime;                                      //场景间的时间间隔(单位:秒)
    int scenesPollingIndex;                                     //暂停时纪录的SetWindows列表下标

};

#endif // SCENESPOLLINGTHREAD_H
