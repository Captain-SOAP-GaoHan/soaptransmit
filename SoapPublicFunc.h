/***************************************************
 * @Title : SoapPublicFunc.h
 * @brief :
 * @author: Captain_SOAP
 * @lastestUpdata: 2021.11.11
 **************************************************/

/*
 * 声明:
 * 该类提供自定义的公用函数,基本上都是静态成员函数
 * STATEMENT:
 * "SoapPublicFunc" Class is desinged and created by Captain_SOAP
 * This class contains popular public functions and so be specified by keyword "static"
 * u can just invoke (or call) functions below without creating a "SoapPublicFunc" object
 * well then , u can also add some functions to this class if u like
 * good luck and have fun
*/

#ifndef SOAPPUBLICFUNC_H
#define SOAPPUBLICFUNC_H

#include <QWidget>
#include <QDateTime>
#include <QTimer>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QTextCodec>
#include <QElapsedTimer>
#include <QtGlobal>
#include <QDir>
#include <QUrl>
#include <QIcon>
#include <QApplication>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QHostInfo>
#include <QtMath>
#include <QDesktopServices>
#include <QCryptographicHash>

class SoapPublicFunc
{
public:

    SoapPublicFunc();

    ~SoapPublicFunc();


    /******************************REFERENCE******************************/
    //以下函数(消息弹窗提示仅供参考,你可通过复制黏贴的方式将代码放入你的程序并做自定义修改)
    //消息弹窗
    static void msgBoxRemind(QString remind = "");

    //消息弹窗(重载1)
    static void msgBoxRemind(QString remind,quint32 ms);

    //消息弹窗(重载2)
    static void msgBoxRemind(QString remind,quint32 ms,QString label);

    //消息弹窗(重载3)
    static void msgBoxRemind(QString remind,quint32 ms,QString label,QFont font);

    //警告弹窗(模态)
    static void warnBoxRemind(QString remind = "");

    //警告弹窗(重载1)
    static void warnBoxRemind(QString remind,QFont font);

    //非模态弹窗提示
    static void modelessBoxRemind(QString remind);

    //非模态弹窗提示(重载1)
    static void modelessBoxRemind(QString remind,quint32 ms);

    //非模态弹窗提示(重载2)
    static void modelessBoxRemind(QString remind,quint32 ms,QWidget *parent);

    //分支弹窗(继续或退出 (确定或取消))
    static int switchBoxRemind(QString remind = "");

    //Soap自定义弹窗
    static void soapBoxRemind(QString remind,quint32 ms,QString label,QFont font = QFont("微软雅黑",11),bool isModel = true);

    //自定义消息弹窗(欢迎)
    static void soapGreeting(QString remind,int ms = 1000,QString label = "提示");
    /******************************REFERENCE******************************/

    //检查是否为Json
    static bool checkIsJson(QByteArray json);

    //检查QString是否为合法的IP地址
    static bool checkIsIp(QString ip);

    //检查QString是否为合法的端口号
    static bool checkIsPort(quint32 port);

    //检查QString是否为合法的资源显示区域(示例：0,0,1920,1080)
    static bool checkIsArea(QString area);

    //检查QString是否为合法的节点实际内容(示例：1.jpg)
    static bool checkIsContent(QString content);

    //检查QString是否是合法的网址(Url)
    static bool checkIsWebUrl(QString url);

    //检查QString是否为合法的windows文件名
    static bool checkIsFileName(QString fileName);

    //检查QString是否为合法的dev(设备)名
    static bool checkIsDevName(QString devName);

    //检查QString的位数是否为奇数位
    static bool checkStrSizeIsOdd(QString str);

    //检查QString的位数是否为偶数位
    static bool checkStrSizeIsEven(QString str);

    //检查QString是否带有中文字符
    static bool checkStrContainCN(QString str);

    //返回一个不带有后缀的文件名
    static QString fileNameWithoutSuff(QString fileName);

    //QString->QjsonObject
    static QJsonObject QStringToJsonObj(const QString jsonString);

    //QJsonObject->QString
    static QString jsonObjToQstring(const QJsonObject &jsonObject);

    //QJsonObject->QString(精简版)
    static QString jsonObjToQstring_lite(const QJsonObject &jsonObject);

    //QByteArray -> QString
    static QString QByteArrayToQString(QByteArray byteArray);

    //返回当前时间
    static QString currentTime();

    //返回当前时间(日期)
    static QString currentTime_date();

    //返回当前时间(时:分)
    static QString currentTime_hour();

    //判断质数
    static bool checkIsPrime(quint64 prime);

    //判断质数2.0
    static bool checkIsPrime2(quint64 prime);

    //计时程序
    static quint64 countTime();

    //返回一个(0,max-1)区间内的随机数(短时间内重复)
    static quint32 generateRamNum(quint32 max);

    //返回一个(0,max-1)区间内的随机数列表
    static QList<quint32> generateRamNumList(quint32 max ,quint32 num);

    //返回一个(0,max-1)区间内的随机数(不重复)
    static quint32 generateRamNumPro(quint32 max);

    //获取(指定路径下的)文件名列表
    static QStringList getFileNames(QString path);

    //获取(指定路径下的)文件名列表(重载1)
    static QStringList getFileNames(QString path,QStringList filter);

    //获取(指定路径下的)文件信息
    static QFileInfoList getFileInfo(const QString &path);

    //获取(指定路径下的)文件信息(重载1)
    static QFileInfoList getFileInfo(const QString &path,QStringList filter);

    //判断一个绝对路径下的文件是否存在
    static bool isFileNameExist(QString absoluteFileName);

    //判断文件夹是否存在
    static bool isFilePathExist(QString absolutePath);

    //根据指定的分隔符将QString->QStringList
    static QStringList stringToStringList(const QString str,QChar separtor);

    //根据指定的分隔符将QStringList->QString(组合)
    static QString stringListToString(const QStringList strList,QChar separator);

    //生成问候语
    static QString soapGreeting();

    //生成离别语
    static QString soapFarewell();

    //将一个数字转为16进制的QbyteArray类型
    static QByteArray toHex(int num,unsigned int place);

    //将一个QByteArray类型转为uint64
    quint64 toUint64(QByteArray data);

    //自定义非阻塞延时函数(毫秒)
    static void soapSleep(quint32 ms);

    //自定义非阻塞延时函数(秒)
    static void soapSleep_S(quint32 s);

    //返回一个合法文件名的后缀
    static QString fileSuff(QString fileName);

    //返回一个合法文件名的后缀长度
    static int fileSuffLen(QString fileName);

    /******************************REFERENCE******************************/
    //交换int变量
    static void swap(int *x,int *y);

    //交换double变量
    static void swap(double *x,double *y);

    //冒泡排序
    static void bubbleSort(int *arra , int len);

    //选择排序
    static void selectionSort(int *arra,int len);

    //插入排序
    static void insertSort(int *arra,int len);

    //希尔排序
    static void shellSort(int *arra,int len);

    //快速排序
    static void quickSort(int *arra,int start ,int end);

    //归并排序
    static void mergeSort(int *arra,int start, int end);

    /******************************REFERENCE******************************/

    //返回一个与resName相同的树节点的QStandardItem 指针
    static QStandardItem *findSameItem(QString resName,QStandardItem *parentItem);

    //抛硬币函数(单次)
    static int coinFlip();

    //抛硬币函数(多次)(重载1)
    static QList<int> coinFlip(quint32 times);

    //获取本地主机名
    static QString getLocalHostName();

    //获取本地主机相关的IP地址
    static QList<QHostAddress> getLocalHostIpList();

    //返回一个x!(x的阶乘)(注意x不可过大！)
    static quint64 factorial(quint8 x);

    //QStringList 去重函数
    static int stringListRemoveDuplicates(QStringList *myStringList);

    //将时间(毫秒)转换为 分钟:秒 的格式QString 返回
    static QString msTo_MinSec(quint64 ms);

    //为QString变量添加{}  (示例: 123 -> {123})
    static void addBraceForString(QString &str);

    //为QByteArray变量添加{}  (示例: 123 -> {123})
    static void addBraceForByteArray(QByteArray &byteArra);

    //为QString变量取出{}  (示例:{123} -> 123)
    static void delBraceForString(QString &str);

    //取出一个绝对路径下的文件数据
    static QByteArray getAbsoluteFileData(QString absoluteFilePath);

    //将数据写入一个绝对路径下的文件
    static bool setAbsoluteFileData(QString absoluteFilePath,QByteArray fileData);

    //将数据加入一个绝对路径下的文件(在文件尾添加)
    static void appendAbsoluteFileData(QString absoluteFilePath,QByteArray fileData);

    //在一个指定路径下新建指定名称的文件夹
    static bool createFolderInPath(QString absolutePath,QString folderName);

    //新建一条路径(若路径已存在,跳出)
    static bool createPath(QString absolutePath);

    //打开给定的路径
    static bool openGivenUrl(QUrl url);

    //更改一个IP的段落值
    static QString alterIpSection(QString ip,quint16 alterSection,quint16 alterContent);

    //取出一个IP的段落值
    static int getIpSection(QString ip,quint16 getSection);

    //返回指定长度的随机字符串
    static QByteArray genRandomString(quint32 strLen = 10 ,bool isContainLowercase = true ,bool isContainCapital = true,bool isContainSymbol = false);

    //QByteArray 转指定编码格式
    static QString fromTextCondec(const QByteArray &array,const char *name);

    //根据加密方式加密明文
    static QByteArray soapEncrypt(QCryptographicHash::Algorithm encryptWay,QByteArray originText);

    //将字符数组逆序
    static void reverseArray(char *s);

    //将字符数组逆序
    static void reverseArray(QByteArray &s);

    //比较2数大小
    static int findBig_2Num(int x,int y);

    //比较2数大小(重载)
    static int findBig_2Num(double x,double y);

    /******************************SOAP LYRICS LIB******************************/
    //Song of the lonely mountain歌词
    static QByteArray lyrics_songOfTheLonelyMountain();

    //The lion's roar 歌词
    static QByteArray lyrics_theLionsRoar();

    //Long road to hell 歌词
    static QByteArray lyrics_longRoadToHell();

    //No time to die 歌词
    static QByteArray lyrics_noTimeToDie();

    //Guns for hire 歌词
    static QByteArray lyrics_gunsForHire();

private:

};

#endif // SOAPPUBLICFUNC_H




