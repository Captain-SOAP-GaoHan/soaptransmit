#include "QSliderClick.h"


//构造
QSliderClick::QSliderClick(QWidget *parent):QSlider(Qt::Horizontal,parent)
{

}

QSliderClick::~QSliderClick()
{

}


//重写函数mousePressEvent
void QSliderClick::mousePressEvent(QMouseEvent *ev)
{
    //注意先调用父类的鼠标点击事件,这样不会影响拖动情况
    QSlider::mousePressEvent(ev);
    //获取鼠标位置,而并不能直接从ev取值(因为若是拖动的事件,鼠标开始点击的位置就无意义)
    double pos = ev->pos().x() / (double)width();
    double value = pos * (maximum() - minimum()) + minimum();
    setValue(value + 0.5);
    //向父窗口发送自定义时间event type,这样可以在父窗口中捕获这个事件进行处理
    QEvent evEvent(static_cast<QEvent::Type>(QEvent::User + 1));
    QCoreApplication::sendEvent(parentWidget(),&evEvent);
}
