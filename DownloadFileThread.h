#ifndef DOWNLOADFILETHREAD_H
#define DOWNLOADFILETHREAD_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>

#include "SoapPublicFunc.h"

class DownloadFileThread : public QObject
{
    Q_OBJECT
public:
    //传输文件枚举类型(传输文件中的各类状态)
    enum DownloadFileState
    {
        DownloadFileInit = 0,                           //传输文件初始化状态
        DownloadFileSendFail,                           //传输文件发送失败
        DownloadFileSendDone,                           //传输文件发送完成(成功)
        DownloadFileRecvFail,                           //传输文件接收失败
        DownloadFileRecvDone,                           //传输文件接收完成(成功)
        DownloadFileAbort = 5,                          //传输文件终止
        DownloadFileConnectFail,                        //传输文件建立(TCP)连接失败
        DownloadFileNotFound,                           //传输文件未找到指定的文件
        DownloadFileOpenFail,                           //传输文件打开失败
        DownloadFileOthersSit                           //其他状态(预留)
    };

    //文件各类参数结构体
    struct downloadFileData
    {
        QString fileName;                               //文件名称
        quint64 fileLen;                                //文件总长(单位:字节)
        quint64 fileSendedLen;                          //文件已发送的大小
        QFile file;                                     //文件对象
        quint32 fileEveryPackLen;                       //传输文件时的每包数据长度
        quint32 fileSendInterval;                       //传输文件时的每包数据时间间隔(单位:微秒)
        quint32 fileSumPack;                            //文件的总包数(文件总长/每包数据的长度)
        quint32 fileCurrPackNum;                        //标识文件传输时的当前包号
        //额外参数
        QList<quint32> filePack_sn;                     //指定的文件的数据包号
    };
    downloadFileData fileData;                          //传输文件各类参数

    //构造
    explicit DownloadFileThread(QObject *parent = nullptr,int panel_sn = 0,QString ip = "192.168.1.1",quint16 port = 3700,QString resPath = "D:/");

    //处理开始传输文件信号
    void dealDownloadFileStart(int panel_sn,QString fileName,QString ip,quint16 port,quint32 len,quint32 interval,QList<quint32> pack_sn);

    //处理传输文件暂停(强行终止)信号
    void dealDownloadFileAbort(int panel_sn);

    //10进制转为16进制QByteArray类型
    QByteArray toHex(int num,unsigned int place);

    //将一个QByteArray类型转为uint64
    quint64 toUint64(QByteArray data);

    //根据发送的文件大小生成大约要延时的时间
    static quint64 waitAfterSendDone(quint64 fileSize);

signals:
    //传输文件过程中各类状态或者情况的返回
    void sig_downloadSituation(int panel_sn,DownloadFileState state);

private:

    //传输文件类私有成员
    QTcpSocket *downloadSocket;                         //传输文件线程通信套接字
    int panel_sn;                                       //传输文件对应的视频墙号
    DownloadFileState state;                            //标识传输文件线程当前的状态
    bool isConnected;                                   //标识是否建立了连接
    QHostAddress clientIp;                              //传输文件的对方IP
    quint16 clientPort;                                 //传输文件的对方端口
    QString resourceFilePath;                           //资源的根路径
    bool isAbort;                                       //标识是否终止文件传输线程
};

#endif // DOWNLOADFILETHREAD_H
