#include "ScenesPollingThread.h"

//构造
ScenesPollingThread::ScenesPollingThread(QObject *parent,int panel_sn) : QObject(parent)
{
    //场景轮询暂停
    scenesPollingIsPause = true;
    //初始化视频墙号,场景轮询下标
    this->panel_sn = panel_sn;
    this->scenesPollingIndex = 0;
    this->scenesPollingTime = 0;
}

//处理场景轮询开始信号
void ScenesPollingThread::deal_scenesPollingStart(int panel_sn,QStringList setWindowsList,int time)
{
    //若视频墙号无法对应 或SetWindowsList为空,退出
    if(this->panel_sn != panel_sn || setWindowsList.isEmpty())
    {
        return;
    }
    //若到来的时间间隔或setWindowsList与之前的不符,覆盖数据
    if(this->scenesPollingTime != time || this->setWindowsList != setWindowsList)
    {
        this->setWindowsList = setWindowsList;
        this->scenesPollingTime = time;
        this->scenesPollingIndex = 0;
    }
    scenesPollingIsPause = false;
    //开始轮询
    while(scenesPollingIsPause == false)
    {
        //回传信号到主线程
        emit sig_scenesPollingCurrScene(this->setWindowsList[scenesPollingIndex]);
        //等待时间(不暂停线程的延时函数)
        SoapPublicFunc::soapSleep(scenesPollingTime*1000);
        //下标自增
        scenesPollingIndex++;
        if(scenesPollingIndex == this->setWindowsList.count())
        {
            scenesPollingIndex = 0;
        }
    }
}

//处理场景轮询暂停信号
void ScenesPollingThread::deal_scenesPollingPause(int panel_sn)
{
    //若视频墙号无法对应,退出
    if(this->panel_sn != panel_sn)
    {
        return;
    }
    //暂停轮询
    scenesPollingIsPause = true;
    //发送已经暂停的信号
    emit sig_scenesPollingIsPaused(this->panel_sn);
}
