#ifndef LOGDIALOG_H
#define LOGDIALOG_H

#include <QDialog>
#include <QFile>
#include <QDebug>

namespace Ui
{
class LogDialog;
}

class LogDialog : public QDialog
{
    Q_OBJECT

public:
    //构造
    explicit LogDialog(QWidget *parent = nullptr);
    //析构
    ~LogDialog();

    //加载内容
    void loadContent(bool recvLogSave,QList<int> recvLogList,bool sendLogSave,QList<int> sendLogList);

signals:
    void sig_logContentSendBack(bool recvLogSave,QList<int> recvLogList,bool sendLogSave,QList<int> sendLogList);

private slots:
    //接收日志保存下拉框槽
    void on_box_recvLogSave_currentIndexChanged(int index);

    //发送日志保存下拉框槽
    void on_box_sendLogSave_currentIndexChanged(int index);

    //接收日志滑块数值改变槽
    void on_sli_recvLogUseLife_valueChanged(int value);

    //发送日志滑块数值改变槽
    void on_sli_sendLogUseLife_valueChanged(int value);

    //接收日志有效期单位下拉框 索引改变槽
    void on_box_recvLogUseLifeUnit_currentIndexChanged(int index);

    //发送日志有效期单位下拉框 索引改变槽
    void on_box_sendLogUseLifeUnit_currentIndexChanged(int index);

    //确定按钮 点击槽
    void on_yesBtn_clicked();

    //取消按钮 点击槽
    void on_cancelBtn_clicked();

private:
    Ui::LogDialog *ui;
};

#endif // LOGDIALOG_H
