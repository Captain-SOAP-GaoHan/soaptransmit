#ifndef QSLIDERCLICK_H
#define QSLIDERCLICK_H

#include <QObject>
#include <QWidget>
#include <QMouseEvent>
#include <QCoreApplication>

#include <QSlider>

//在QSlider类基础上修改的类
class QSliderClick : public QSlider
{
public:

    //构造
    QSliderClick(QWidget *parent = nullptr);

    //析构
    ~QSliderClick();

protected:
    //鼠标按下事件
    void mousePressEvent(QMouseEvent *ev) override;

};

#endif // QSLIDERCLICK_H
