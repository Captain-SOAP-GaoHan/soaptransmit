#include "FileReadThread.h"

//构造
FileReadThread::FileReadThread(QObject *parent) : QObject(parent)
{
    filePath.clear();
    fileSize = 0;
    readFileFlag = true;
}

//开始读取数据
void FileReadThread::deal_StartReadFile(QString AbsFilePath)
{
    //检查文件后缀(当前仅仅支持读取 文本文件 和 .json文件)
    if(AbsFilePath.endsWith(".txt") == false && AbsFilePath.endsWith(".json") == false)
    {
        emit sig_sitState(fileTypeUnsupported,QFileInfo(AbsFilePath).fileName());
        return;
    }
    //检查文件路径,若到来的文件路径不一致则开始加载新到来的文件
    if(AbsFilePath != filePath)
    {
        file.setFileName(AbsFilePath);
        if(file.open(QFile::ReadOnly) == false)
        {
            file.close();
            emit sig_sitState(fileOpenFail,QFileInfo(AbsFilePath).fileName());
            return;
        }
        //若文件过大,不予预览
        if(file.size() > (int)10*1048576)
        {
            emit sig_sitState(fileTooLarge,QFileInfo(AbsFilePath).fileName());
            return;
        }
        fileSize = file.size();
        QByteArray data;
        readFileFlag = true;
        //读取文件并回传到主线程
        while(readFileFlag == true && fileSize != 0)
        {
            data = file.read(7500);
            fileSize = fileSize - data.length();
            emit sig_fileContent(data);
            //阻塞性延时,因为回传的数据主线程依然需要一定时间处理,所以不急于立刻开始下一次循环
            QThread::msleep(10);
        }
        //关闭文件
        file.close();
        //检查文件是否读取完毕
        (fileSize == 0) ? emit sig_sitState(fileReadDone,QFileInfo(AbsFilePath).fileName()) : emit sig_sitState(fileReadAbort,QFileInfo(AbsFilePath).fileName());
    }
}

//暂停文件读取
void FileReadThread::deal_stopReadFile()
{
    readFileFlag = false;
}
