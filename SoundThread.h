#ifndef SOUNDTHREAD_H
#define SOUNDTHREAD_H

#include <QObject>
#include <QSound>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QUrl>
#include <QDir>

#include "SoapPublicFunc.h"

class SoundThread : public QObject
{
    Q_OBJECT
public:
    //报告
    enum MusicReport
    {
        PlayerIsPlay,                               //播放器正在播放
        PlayerIsStop,                               //播放器已停止
        MusicListSetSucc,                           //歌单设置成功
        MusicListSetFail,                           //歌单设置失败
        PlayMode_CurrItemInLoop,                    //播放模式:单曲循环
        PlayMode_Sequential,                        //播放模式:顺序播放
        PlayMode_Loop,                              //播放模式:循环
        PlayMode_Random                             //播放模式:随机
    };

    //音乐列表参数结构体
    struct MusicListInfo
    {
        QStringList currMusicList;                  //当前播放的歌单(音乐文件的路径 列表)
        quint32 currMusicListIndex;                 //当前歌单列表的下标
        QMediaPlaylist::PlaybackMode currMode;      //当前歌单的播放模式
    };

    //构造
    explicit SoundThread(QObject *parent = nullptr);

    //处理音效播放
    void deal_soundEffectPlay(QString soundFilePath);

    //处理设置音乐列表
    void deal_setMusicList(QStringList musicList,quint32 index = 0,QMediaPlaylist::PlaybackMode mode = QMediaPlaylist::PlaybackMode::Loop,bool playNow = false);

    //处理播放器状态
    void deal_musicPlay(bool isPlay);

    //设置播放器状态
    void setPlayerState(bool isPlay);

    //获取播放器状态
    bool getPlayerState();

signals:
    //音乐相关信号
    void sig_musicRep(MusicReport rep);


private:
    QMediaPlayer *player;                           //播放器
    QMediaPlaylist *playList;                       //播放列表
    bool isPlay;                                    //播放器状态(播放 或 停止)
    MusicListInfo musicListInfo;                    //音乐参数

};

#endif // SOUNDTHREAD_H
