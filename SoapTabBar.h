#ifndef SOAPTABBAR_H
#define SOAPTABBAR_H

#include <QApplication>
#include <QStyleOptionTab>
#include <QTabBar>
#include <QTabWidget>
#include <QStylePainter>

class SoapTabBar : public QTabBar
{
public:
    SoapTabBar();

    QSize tabSizeHint(int index) const override;

protected:
    void paintEvent(QPaintEvent *) override;

};

#endif // SOAPTABBAR_H
