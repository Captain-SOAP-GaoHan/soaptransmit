#include "FileListDisplay.h"
#include "ui_FileListDisplay.h"

FileListDisplay::FileListDisplay(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileListDisplay)
{
    ui->setupUi(this);

    /******************************本窗体的基础设置******************************/
    this->setAttribute(Qt::WA_DeleteOnClose);                                                       //设置在关闭时释放
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::Dialog);           //窗口相关设置
    this->hide();

    //若窗体父对象不为空,根据父对象的大小设置大小,若为空则还需要额外设置样式表
    if(parent != nullptr)
    {
        this->resize(parent->width(),parent->height());
    }
    else
    {
        setMinimumSize(400,200);
        //加载样式表
        QFile styleSheetFile(":/qss/qss/blue.css");
        styleSheetFile.open(QFile::ReadOnly);
        setStyleSheet(styleSheetFile.readAll());
        styleSheetFile.close();
    }

    /******************************对UI控件的设置******************************/
    ui->tableWidget_list->setSelectionBehavior(QAbstractItemView::SelectRows);                      //设置列表的选中行为:整行选中
    ui->tableWidget_list->setSelectionMode(QAbstractItemView::ExtendedSelection);                   //设置可以多选
    ui->tableWidget_list->setShowGrid(false);                                                       //设置不显示网格线
    ui->tableWidget_list->setFrameShape(QFrame::NoFrame);                                           //设置无边框
    ui->tableWidget_list->setContextMenuPolicy(Qt::CustomContextMenu);                              //设置该控件允许鼠标右键菜单
    ui->tableWidget_list->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);                 //设置水平表头中央对齐
    ui->tableWidget_list->horizontalHeader()->setStretchLastSection(true);                          //设置自动根据宽度对齐
    ui->tableWidget_list->horizontalHeader()->setFont(menuFont);                                    //水平表头设置字形
    ui->tableWidget_list->horizontalHeader()->setResizeContentsPrecision(20);                       //设置当使用ResizeToContents是计算大小的精确度
    ui->tableWidget_list->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);                   //设置垂直表头中央对其
    ui->tableWidget_list->verticalHeader()->setFont(menuFont);                                      //垂直表头设置字形
    ui->tableWidget_list->horizontalHeader()->setHighlightSections(false);                          //设置水平表头不高亮显示选中部分
    ui->tableWidget_list->verticalHeader()->setHighlightSections(false);                            //设置垂直表头不高亮显示选中部分

    /******************************初始化私有成员******************************/
    setListType(InfoType::OtherType);
}

//析构
FileListDisplay::~FileListDisplay()
{
    delete ui;
}

//清空列表控件
void FileListDisplay::clearTabWidget()
{
    ui->tableWidget_list->clear();
}

//为tabWidget控件添加水平表头内容
void FileListDisplay::setHorizonHeaderContent(QStringList headerContent)
{
    if(headerContent.isEmpty() == false)
    {
        ui->tableWidget_list->setColumnCount(headerContent.count());
        ui->tableWidget_list->setHorizontalHeaderLabels(headerContent);
    }
}

//为tabWidget控件添加垂直表头内容
void FileListDisplay::setVerticalHeaderContent(QStringList headerContent)
{
    if(headerContent.isEmpty() == false)
    {
        ui->tableWidget_list->setVerticalHeaderLabels(headerContent);
    }
}

//为TabWidget控件添加内容
void FileListDisplay::loadContentIntoList(QList<QStringList> content,QStringList headerContent,InfoType type,bool btnEnable)
{
    if(type == InfoType::OtherType)
    {
        return;
    }
    //清空表格内容
    ui->tableWidget_list->clear();
    //设置类型
    setListType(type);
    //检查传入的内容是否为空
    if(content.isEmpty() == false)
    {
        //取出参数并检查是否合法
        int row = content.count(),column = (content.at(0).count());
        //确保传入的内容的列数与表头的列数相同
        if(column != headerContent.count())
        {
            return;
        }

        //根据参数设置控件
        ui->tableWidget_list->setRowCount(row);                                                 //设置行数
        ui->tableWidget_list->setColumnCount(column);                                           //设置列数
        ui->tableWidget_list->setHorizontalHeaderLabels(headerContent);                         //设置水平表头内容

        //内容填入QTableWidget控件
        switch (fileType)
        {
        case IdType:
        {
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<column;j++)
                {
                    switch (j)
                    {
                    case 0:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(QIcon(":/Transmit/signal_blue.png"),content.at(i).at(j)));break;
                    case 1:
                    case 2:
                    case 3:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(content.at(i).at(j)));break;
                    }
                }
            }
            //设置垂直表头
            ui->tableWidget_list->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
            //设置水平表头
            ui->tableWidget_list->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
            ui->tableWidget_list->setColumnWidth(0,150);
            ui->tableWidget_list->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
            //设置最小宽度
            //ui->tableWidget_list->horizontalHeader()->setMinimumSectionSize(100);

            //ui->tableWidget_list->setColumnWidth(0,150);
            //ui->tableWidget_list->setColumnWidth(1,)
        }break;
        case NodeType:
        {
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<column;j++)
                {
                    switch (j)
                    {
                    case 0:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(QIcon(":/Transmit/transmit_Blue.png"),content.at(i).at(j)));break;
                    case 1:
                    case 2:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(content.at(i).at(j)));break;
                    }
                }
            }
        }break;
        case WindowType:
        {
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<column;j++)
                {
                    switch (j)
                    {
                    case 0:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(QIcon(":/Transmit/panel_blue.png"),content.at(i).at(j)));break;
                    case 1:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(content.at(i).at(j)));break;
                    }
                }
            }
        }break;
        case ScenesPollingType:
        {
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<column;j++)
                {
                    switch (j)
                    {
                    case 0:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(QIcon(":/Transmit/scene_blue.png"),content.at(i).at(j)));break;
                    case 1:ui->tableWidget_list->setItem(i,j,new QTableWidgetItem(content.at(i).at(j)));break;
                    }
                }
            }
        }break;
        default:return;
        }
    }
    else
    {
        //内容为空,设置表格行数为0
        ui->tableWidget_list->setRowCount(0);
        //根据类型设定列与水平表头内容
        switch (type)
        {
        case InfoType::IdType:
        {
            ui->tableWidget_list->setColumnCount(4);
            ui->tableWidget_list->setHorizontalHeaderLabels(QStringList()<<"信号源IP"<<"名称"<<"主码流"<<"辅码流");
        }
            break;
        case InfoType::NodeType:
        {
            ui->tableWidget_list->setColumnCount(3);
            ui->tableWidget_list->setHorizontalHeaderLabels(QStringList()<<"转发节点IP"<<"可分配信号数"<<"转发的IP");
        }
            break;
        case InfoType::WindowType:
        {
            ui->tableWidget_list->setColumnCount(2);
            ui->tableWidget_list->setHorizontalHeaderLabels(QStringList()<<"开启的视频墙号"<<"转发的IP");
        }
            break;
        case InfoType::ScenesPollingType:
        {
            ui->tableWidget_list->setColumnCount(2);
            ui->tableWidget_list->setHorizontalHeaderLabels(QStringList()<<"轮询的视频墙"<<"轮询间隔(S)");
        }
            break;
        default:return;
        }
    }

    //按钮使能
    ui->addBtn->setEnabled(btnEnable);
    ui->saveBtn->setEnabled(btnEnable);
}

//设置当前列表的格式
void FileListDisplay::setListType(FileListDisplay::InfoType type)
{
    this->fileType = type;
}

//取出当前列表格式
FileListDisplay::InfoType FileListDisplay::getListType()
{
    return this->fileType;
}

//修改选定(单选)的列表对象
void FileListDisplay::alterCell()
{
    QTableWidgetItem *item = ui->tableWidget_list->currentItem();
    if(item != nullptr)
    {
        ui->tableWidget_list->editItem(item);
    }
}

//删除选定的行
void FileListDisplay::removeSelectedRow()
{
    //取出选定的行并加入待删除的行列表
    QItemSelectionModel *mode = ui->tableWidget_list->selectionModel();
    QModelIndexList rowList = mode->selectedRows();
    QVector<int> deleteRows;
    for(int i=0;i<rowList.count();i++)
    {
        deleteRows.append(rowList.at(i).row());
    }
    //将待删除的行号列表排序
    std::sort(deleteRows.begin(),deleteRows.end());
    //删除行号
    for(int i=deleteRows.count()-1;i>=0;i--)
    {
        ui->tableWidget_list->removeRow(deleteRows.at(i));
    }
}

//保存按钮按下
void FileListDisplay::on_saveBtn_clicked()
{
    //取出当前列表中的所有数据并回传
    switch (getListType())
    {
    case IdType:
    case NodeType:
    {
        QList<QStringList> content;
        for(int row=0;row<ui->tableWidget_list->rowCount();row++)
        {
            QStringList perRowList;
            for(int column=0;column<ui->tableWidget_list->columnCount();column++)
            {
                if(column < 3 && ui->tableWidget_list->item(row,column)->text().isEmpty() == true)
                {
                    SoapPublicFunc::modelessBoxRemind("警告!请确保填入了有效的数据!",3000,this);
                    return;
                }
                perRowList.append(ui->tableWidget_list->item(row,column)->text());
            }
            content.append(perRowList);
        }
        //回传信号
        emit sig_infoListSendBack(content,getListType());
    }
        break;
    default:this->close();
    }
}

//取消按钮按下
void FileListDisplay::on_cancelBtn_clicked()
{
    this->close();
}

//新增按钮按下
void FileListDisplay::on_addBtn_clicked()
{
    //增加一行
    ui->tableWidget_list->setRowCount(ui->tableWidget_list->rowCount()+1);
    int currRow = ui->tableWidget_list->rowCount()-1;
    //根据现在的格式添加对象初始值
    switch (getListType())
    {
    case IdType:
    {
        for(int column=0;column<4;column++)
        {
            switch (column)
            {
            case 0:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QIcon(":/Transmit/signal_blue.png"),QString("192.168.1.1")));break;
            case 1:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QString("DefaultName")));break;
            case 2:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QString("--input major stream--")));break;
            case 3:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QString("--input minor stream--")));break;
            }
        }
    }
        break;
    case NodeType:
    {
        for(int column=0;column<2;column++)
        {
            switch (column)
            {
            case 0:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QIcon(":/Transmit/transmit_Blue.png"),QString("192.168.1.1")));break;
            case 1:ui->tableWidget_list->setItem(currRow,column,new QTableWidgetItem(QString("9")));break;
            }
        }
    }
        break;
    default:return;
    }
}

//tableWidget右键菜单
void FileListDisplay::on_tableWidget_list_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->tableWidget_list->indexAt(pos);
    if(index.row() != -1 && index.column() != -1)
    {
        if(getListType() != InfoType::IdType && getListType() != InfoType::NodeType)
        {
            return;
        }
        QMenu *menu = new QMenu(this);
        menu->setAttribute(Qt::WA_DeleteOnClose);
        menu->setShortcutEnabled(true);
        menu->setMinimumWidth(150);

        //菜单添加动作
        menu->addAction(QIcon(":/Transmit/edit_Blue.png"),QString("修改"),this,&FileListDisplay::alterCell,QKeySequence(Qt::Key_A))->setFont(bodyFont);
        QShortcut *alterCell_Sc = new QShortcut(QKeySequence(Qt::Key_A),menu,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
        connect(alterCell_Sc,&QShortcut::activated,this,&FileListDisplay::alterCell);

        menu->addSeparator();

        menu->addAction(QIcon(":/Transmit/clear_Blue.png"),QString("删除"),this,&FileListDisplay::removeSelectedRow,QKeySequence(Qt::Key_D))->setFont(bodyFont);
        QShortcut *deleteSelectRow = new QShortcut(QKeySequence(Qt::Key_D),menu,nullptr,nullptr,Qt::ShortcutContext::WindowShortcut);
        connect(deleteSelectRow,&QShortcut::activated,this,&FileListDisplay::removeSelectedRow);

        //在鼠标位置显示右键菜单
        menu->move(QCursor::pos());
        menu->show();
    }
}

//tableWidget单元格改变槽
void FileListDisplay::on_tableWidget_list_itemChanged(QTableWidgetItem *item)
{
    //需要用户自行保证输入数据的有效,这里考虑到效率问题不校验数据的有效或合法性
    if(item != nullptr)
    {
        item->setText(item->text().trimmed());
    }
}
